/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use std::{
    collections::{hash_map::Entry, HashMap},
    io::BufRead,
};
use thiserror::Error;

/// represent the key value pair of an entry
type KeyValuePair = (String, String);

/// errors that can occur during parsing
#[derive(Error, Debug, Eq, PartialEq)]
pub enum ParseError {
    /// an entry that should be added to a section already exists
    #[error(
        "The following entry already exists in section {section_title:?}: `{key:?} = {value:?}`"
    )]
    DuplicateEntry {
        section_title: String,
        key: String,
        value: String,
    },

    /// failed to add an entry because no section exists
    #[error("The following entry does not belong to any section: `{key:?} = {value:?}`")]
    MissingSection { key: String, value: String },

    /// something went wrong while parsing a line
    #[error("Could not parse the following line: `{0}`")]
    InvalidLine(String),
}

/// line types that exist in ini file
///
/// This is used prior to conversion into sections
#[derive(Debug, Eq, PartialEq)]
enum Line {
    /// comment without the leading '#' but leading whitespaces
    Comment(String),

    /// entry as a key value pair
    Entry(KeyValuePair),

    /// section title
    Title(String),
}

/// a section in file
///
/// Contains the section title and a hash map for all entries
#[derive(Default, Debug, Eq, PartialEq)]
pub struct Section {
    /// title of the section
    pub title: String,

    /// all entries of the section
    pub entries: HashMap<String, String>,
}

/// a complete ini file
#[derive(Default, Debug, Eq, PartialEq)]
pub struct IniFile {
    /// list of sections that make up the file
    pub sections: Vec<Section>,
}

impl IniFile {
    /// create a new IniFile from a file
    ///
    /// Note: This is a rather simple parser with limited information in case of an error.
    /// It is expected that ini files can be parsed without errors.
    /// This parser is not intended for writing ini files.
    /// It is used to read valid files from a system.
    ///
    /// # Arguments
    ///
    /// * `reader` - reader to access the file data
    pub fn from_file<R: BufRead>(reader: R) -> Result<IniFile, ParseError> {
        // parse Icon Theme
        reader
            .lines()
            .map_while(Result::ok) // ignore lines with error
            .map(|line| parse_line(&line))
            .try_fold(IniFile::default(), |state, maybe_line| match maybe_line {
                Ok(line) => try_add_line_to_file(state, line),
                Err(e) => Err(e),
            })
    }
}

/// try to add a Line to a [`IniFile`]
///
/// Check if line can be added to `ini_file`.
/// If it is possible, the file is updated and returned.
/// Otherwise, a [`ParseError`] is returned.
///
/// # Arguments
///
/// * `ini_file` - [`IniFile `] to update
/// * `line` - [`Line`] that shall be added to `ini_file`
///
/// # Return
///
/// * updated `ini_file` or an error
fn try_add_line_to_file(mut ini_file: IniFile, line: Line) -> Result<IniFile, ParseError> {
    match line {
        Line::Comment(_) => Ok(ini_file),
        Line::Title(title) => {
            ini_file.sections.push(Section {
                title,
                ..Section::default()
            });
            Ok(ini_file)
        }
        Line::Entry((key, value)) => {
            if let Some(section) = ini_file.sections.last_mut() {
                match section.entries.entry(key) {
                    Entry::Vacant(e) => {
                        e.insert(value);
                        Ok(ini_file)
                    }
                    Entry::Occupied(e) => Err(ParseError::DuplicateEntry {
                        section_title: section.title.clone(),
                        key: e.key().clone(),
                        value,
                    }),
                }
            } else {
                Err(ParseError::MissingSection { key, value })
            }
        }
    }
}

/// parse a string into a [`Line`]
///
/// Try to parse a string that represents a single line into one of the different
/// line types of an ini file.
/// If this is not possible, return a [`ParseError`].
///
/// # Arguments
///
/// * `line` - string containing the line to parse (without newline)
///
/// # Return
///
/// * resulting [`Line`] or error
fn parse_line(line: &str) -> Result<Line, ParseError> {
    if line.is_empty() {
        Ok(Line::Comment(line.to_string()))
    } else if let Some(comment) = line.strip_prefix('#') {
        Ok(Line::Comment(comment.to_string()))
    } else if let Some(title) = parse_title(line) {
        Ok(Line::Title(title))
    } else if let Some(kvp) = parse_key_value_pair(line) {
        Ok(Line::Entry(kvp))
    } else {
        Err(ParseError::InvalidLine(line.to_string()))
    }
}

/// parse a string as a key-value pair
///
/// Parse a single line, splitting it at '='.
/// This function implements the rules for key-value pairs in desktop entry files.
/// The rules do not include underscore or brackets.
/// They are supported anyhow, because language specifiers in keys use them.
///
/// # Arguments:
///
/// * `s` - string to be parsed
///
/// # Return:
///
/// * a key-value pair of string on success
///
/// # Note:
/// This parser is to gracious and expects more than it should.
/// It simple accepts all characters than can occur in a key.
/// The rationale is that ini files are assumed to be correct.
/// With i8n this behaviour will be replaced with a correct `parse key` function
fn parse_key_value_pair(s: &str) -> Option<KeyValuePair> {
    let fch = s.chars().next()?;

    if !fch.is_ascii_alphanumeric() {
        return None;
    }

    let mut parts = s.split('=');
    let key = parts.next();
    let value = parts.next();
    if let (Some(key), Some(value)) = (key, value) {
        if key
            .trim_end()
            .chars()
            .all(|ch| matches!(ch,'0'..='9' | 'A'..='Z' | 'a'..='z' | '-' | '_' | '[' | ']' | '@'))
        {
            return Some((key.trim().to_owned(), value.trim().to_owned()));
        }
    }

    None
}

/// parse a title
///
/// Parse a section header and return the section title on success.
///
/// # Arguments:
///
/// * `s` - string to be parsed
///
/// # Return:
///
/// * section title on success
fn parse_title(s: &str) -> Option<String> {
    if s.starts_with('[') && s.ends_with(']') {
        let title = &s[1..s.len() - 1];
        if !title.contains('[') && !title.contains(']') {
            Some(title.to_string())
        } else {
            None
        }
    } else {
        None
    }
}

#[cfg(test)]
#[path = "test_fd_org_ini_parser.rs"]
mod tests;
