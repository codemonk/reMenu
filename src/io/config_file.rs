// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{cell::OnceCell, collections::BTreeMap, fmt::Display, rc::Rc};

use crate::{
    core::{
        config::{
            self, Config, DesktopEntriesConfig, FeedConfig, FeedOptions, GeneralConfig, MenuLayout,
            PathEntriesConfig,
        },
        feeds::{
            EntryFeed, ExecutionFeed, Fallthrough, Feed, FilterType, LayeredFeed, LazyFeed,
            LoadingStrategy,
        },
        Color, Length, Pixels,
    },
    io::entries::{desktop_entries, path_entries},
};
use anyhow::{Context, Error, Result};
use clap::ValueEnum;
use log::{debug, info, warn};
use serde::Deserialize;

/// The default config with comments
const CFG_DEFAULT: &str = r##"# This is the default theme for reMenu.
# It is designed to be astethically pleasing and to show of some features.
# Configure it to your liking.

# The general section is mandatory.
# It contains most of the settings.
[general]
# The fallthrough strategy, i.e. what to do if the current feed has no results on a given input.
# Can be one of these:
# - None: Do nothing
# - Stepwise: Fall to the next feed
# - Last: Fall to the last feed
fallthrough = "Stepwise"
# The loading strategy, i.e. when should feeds be loaded
# Can be one of these:
# - Eager: All feeds will be loaded on start of the application
# - Lazy: Feeds will be loaded on first use [default]
loading = "Lazy"
# The layout to use, i.e. how to layout the menu on screen.
# Setting this to 'Dock', makes the menu appears at the top of the screen and entries are rendered as a horizontal line.
# This is similar to dmenu.
# It is the default.
# Setting this to 'Splash' renders the menu as a splash screen at the center of the screen with entries rendered in a column.
layout = "Dock"
# terminal emulator to use for terminal applications
# This will be prepended by the command to execute.
# For example the command 'cmd param1 param2' will be executed as
# 'alacritty -e cmd param1 param2', if this parameter is set to 'alacritty -e'.
terminal = "alacritty -e"
# The height of the menu. Needs a unit (px, % or pt).
height = "32px"
# Font size in pt (optional, defaults to 10)
font_size = 11
# Font to use (a fontconfig-compatible name)
font = "DejaVu Sans"
# Icon theme to use
icon_theme = "Papirus"
# Icon size (px, % or pt, defaults to 24px)
icon_size = "24px"
# color of text as RGBA
text_color = "#FFFFFFFF"
# background color as RGBA
bg_color = "#2F3D44FF"
# color of text of a selected entry as RGBA
text_color_hl = "#2F3D44FF"
# background color of a selected entry as RGBA
bg_color_hl = "#FFFFFFFF"
# width of the input field (optional). Needs a unit (px, % or pt).
input_width = "600px"

# Each feed is given by a feed entry.
# The order for fallthrough strategy is given by
# the order in which feeds appear in the config.
# Each feed has a type (DesktopEntries, Path or DirectExecution),
# a hotkey with modifier (list of Ctrl, Alt, AltGr, Shift, Meta) and key)
# and feed-specific options:
# - Both desktop entries and path accept a filter (StartsWith, Contains or Fuzzy).
# - Desktop entries also accepts a flag to filter entries without icons (defaults to false).

[[feed]]
type = "DesktopEntries"
filter = "StartsWith"
require_icons = "false"
keybind = { modifier = ["Ctrl"], key = "1"}

[[feed]]
type = "Path"
filter = "StartsWith"
keybind = { modifier = ["Ctrl"], key = "2"}

[[feed]]
type = "DirectExecution"
filter = "Fuzzy" # DirectExecution does not use the filter. Any can be provided.
keybind = { modifier = ["Ctrl"], key = "3"}
"##;

/// simple black & white dark theme
const CFG_SIMPLE_DARK: &str = r##"[general]
fallthrough = "Stepwise"
terminal = "alacritty -e"
height = "32px"
font = "DejaVu Sans"
icon_theme = "hicolor"
text_color = "#FFFFFFFF"
bg_color = "#000000FF"
text_color_hl = "#000000FF"
bg_color_hl = "#FFFFFFFF"
input_width = "600px"

[[feed]]
type = "DesktopEntries"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "1"}

[[feed]]
type = "Path"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "2"}

[[feed]]
type = "DirectExecution"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "3"}
"##;

/// simple black & white light theme
const CFG_SIMPLE_LIGHT: &str = r##"[general]
fallthrough = "Stepwise"
terminal = "alacritty -e"
height = "32px"
font = "DejaVu Sans"
icon_theme = "hicolor"
text_color = "#000000FF"
bg_color = "#FFFFFFFF"
text_color_hl = "#FFFFFFFF"
bg_color_hl = "#000000FF"
input_width = "600px"

[[feed]]
type = "DesktopEntries"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "1"}

[[feed]]
type = "Path"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "2"}

[[feed]]
type = "DirectExecution"
filter = "Fuzzy"
keybind = { modifier = ["Ctrl"], key = "3"}
"##;

/// type of a feed
#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub enum FeedType {
    /// feed with desktop entries according to fd.org spec
    DesktopEntries,

    /// feed that directly executes the input
    DirectExecution,

    /// feed with entries from search path
    Path,
}

impl Display for FeedType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            FeedType::DesktopEntries => f.write_str("Desktop entries"),
            FeedType::DirectExecution => f.write_str("Direct execution"),
            FeedType::Path => f.write_str("Path entries"),
        }
    }
}

/// Enumeration of included themes
/// Used if one does not want to provide a custom themes
/// or needs a starting point for creating one.
#[derive(Debug, Default, Clone, ValueEnum)]
pub enum IncludedTheme {
    /// default themes with comments
    #[default]
    Default,

    /// simple dark theme in black & white
    SimpleDark,

    /// simple light theme in black & white
    SimpleLight,
}

/// Representation of the serialized configuration
/// This is used when parsing the TOML.
/// The actual [`Config`] is build from this structure.
/// It decouples the user-facing toml file from internal details.
/// Specifically, it allows us to make fields optional and fill it with defaults.
#[derive(Debug, Clone, Deserialize)]
struct ConfigRaw {
    /// ordered list of feeds
    feed: Vec<FeedConfigRaw>,

    /// feed-independent configuration options
    general: GeneralConfigRaw,
}

/// Representation of all feed-independent, i.e. general, configuration options
/// As with [`ConfigRaw`] this is used for parsing TOML.
#[derive(Debug, Clone, Deserialize)]
struct GeneralConfigRaw {
    /// fallthrough strategy to use
    fallthrough: Fallthrough,

    /// loading strategy to use (lazy or eager)
    #[serde(default)]
    loading: LoadingStrategy,

    /// terminal emulator to use for terminal applications
    /// e.g. `alacritty -e`.
    /// This will be prepended by the command to execute
    terminal: Option<String>,

    /// layout to use, can be either 'Dock' or 'Splash'.
    /// With 'Dock', the menu appears at the top of the screen
    /// and entries are rendered as a horizontal line.
    /// This is similar to dmenu.
    /// With 'Splash' a splash screen is shown at the center of the screen.
    /// Entries are rendered in a column.
    /// 'Dock' is the default.
    #[serde(default)]
    layout: MenuLayout,

    /// height of the menu bar (in px, pt or % of screen height)
    height: String,

    /// font to use for text (fontconfig name)
    font: String,

    /// font size in pt
    pub font_size: Option<u32>,

    /// icon theme to use for application icons
    icon_theme: String,

    /// size of an icon (in px, pt or % of screen height)
    pub icon_size: Option<String>,

    /// color of text for entries
    text_color: String,

    /// background color for entries
    bg_color: String,

    /// color of text for highlighted entries
    text_color_hl: String,

    /// color of background for highlighted entries
    bg_color_hl: String,

    /// width of the input field (in px, pt or % of screen width)
    input_width: Option<String>,
}

/// configuration of a single feed
#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct FeedConfigRaw {
    /// type of feed
    pub r#type: FeedType,

    /// filter to apply to feed
    #[serde(flatten)]
    pub options: BTreeMap<String, String>,

    /// keybind to activate that feed
    pub keybind: Option<config::Keybind>,
}

/// load the configuration file and parse it to a [`Config`] struct
///
/// If a config name is provided, a file will be searched in `$XDG_CONFIG_DIR/remenu/<name>.toml`
/// and loaded.
/// If nothing is provided, the default config at `$XDG_CONFIG_DIR/remenu/config.toml`
/// will be loaded.
/// In case no config can be found at the given location, the default config will be used.
///
/// # Arguments
///
/// * `config` - name of a config file (without extension)
pub fn load(config: Option<String>) -> Result<Config> {
    let path = match config {
        Some(name) => xdg::BaseDirectories::with_prefix("remenu")?
            .place_config_file(format!("{}.toml", name))?,
        None => xdg::BaseDirectories::with_prefix("remenu")?.place_config_file("config.toml")?,
    };

    let config_data =
        std::fs::read_to_string(&path).or_else(|_| -> Result<String, std::io::Error> {
            info!("No config found at '{:?}'. Using default config.", path);
            Ok(CFG_DEFAULT.to_string())
        })?;

    parse_raw_config(&config_data)
}

/// build a [`LayeredFeed`] from a given [`Config`].
///
/// This is used to create the base feed for the core logic.
/// All subfeeds will be instantiated and added to the returned feed.
///
/// # Arguments
///
/// * `cfg` - configuration to used for creating the feed
pub fn build_feed(cfg: &Config) -> LayeredFeed {
    let desktop_entries = Rc::new(OnceCell::new());
    let path_entries = Rc::new(OnceCell::new());
    let feeds = cfg
        .feeds
        .iter()
        .map(|feed_cfg| {
            let feed: Box<dyn Feed> = match feed_cfg.options.clone() {
                FeedOptions::DesktopEntries(options) => {
                    debug!("Loading desktop entries feed");
                    // @todo move into separate function load_entry_feed(strategy, lambda)
                    match cfg.general.loading {
                        LoadingStrategy::Eager => Box::new(EntryFeed::new(
                            desktop_entries.get_or_init(desktop_entries::load).clone(),
                            options.filter.clone(),
                            options.requires_icons,
                        )),
                        LoadingStrategy::Lazy => {
                            let de = desktop_entries.clone();
                            Box::new(LazyFeed::new(move || {
                                EntryFeed::new(
                                    de.get_or_init(desktop_entries::load).clone(),
                                    options.filter.clone(),
                                    options.requires_icons,
                                )
                            }))
                        }
                    }
                }
                FeedOptions::PathEntries(options) => {
                    debug!("Loading path feed");
                    match cfg.general.loading {
                        LoadingStrategy::Eager => Box::new(EntryFeed::new(
                            path_entries.get_or_init(path_entries::load).clone(),
                            options.filter.clone(),
                            false,
                        )),
                        LoadingStrategy::Lazy => {
                            let pe = path_entries.clone();
                            Box::new(LazyFeed::new(move || {
                                EntryFeed::new(
                                    pe.get_or_init(path_entries::load).clone(),
                                    options.filter.clone(),
                                    false,
                                )
                            }))
                        }
                    }
                }
                FeedOptions::DirectExecution => {
                    debug!("Loading direct execution feed");
                    Box::new(ExecutionFeed::new())
                }
            };
            feed
        })
        .collect();

    LayeredFeed::new(feeds, cfg.general.fallthrough.clone())
}

/// create a new config file from the given included theme
///
/// Take one of the included themes and write it the config file location at
/// `$XDG_CONFIG_DIR/remenu/config.toml`.
///
/// # Arguments
///
/// * `theme` - included theme to write to config file
pub fn touch(theme: IncludedTheme) -> Result<()> {
    let path = xdg::BaseDirectories::with_prefix("remenu")?.place_config_file("config.toml")?;
    match theme {
        IncludedTheme::Default => std::fs::write(path, CFG_DEFAULT)?,
        IncludedTheme::SimpleDark => std::fs::write(path, CFG_SIMPLE_DARK)?,
        IncludedTheme::SimpleLight => std::fs::write(path, CFG_SIMPLE_LIGHT)?,
    };

    Ok(())
}

/// Validate the config file
///
/// Load the config file from `$XDG_CONFIG_DIR/remenu/config.toml` and check if
/// it can be loaded without errors.
/// Either return a success string or the parsing error.
///
/// # Arguments
///
/// * `config` - name of a config file
pub fn validate(config: Option<String>) -> Result<String> {
    load(config).map(|_| "Config parsed successfully".to_string())
}

/// parse toml data of a config file to a [`Config`]
///
/// Arguments
///
/// * `data` - string slice containing a complete config.toml
fn parse_raw_config(data: &str) -> Result<Config> {
    let cfg: ConfigRaw = toml::from_str(data)?;

    // warnings for potentially troublesome keybinds
    let _keybinds = cfg
        .feed
        .iter()
        .filter_map(|f| f.keybind.clone())
        .inspect(|kb| {
            if kb.modifier.is_empty() {
                warn!("Keybinds without modifier can cause unexpected behaviour.");
            }
        })
        .fold(Vec::new(), |mut kbs, kb| {
            if kbs.contains(&kb) {
                warn!("Using the same keybind multiple times can cause unexpected behaviour.");
            } else {
                kbs.push(kb.clone());
            }
            kbs
        });

    let feed_cfg = cfg
        .feed
        .into_iter()
        .map(|cfg_raw| match cfg_raw.r#type {
            FeedType::DesktopEntries => Ok(FeedConfig {
                options: parse_desktop_entries_options(cfg_raw.options)?,
                keybind: cfg_raw.keybind,
            }),
            FeedType::DirectExecution => Ok(FeedConfig {
                options: parse_direct_execution_options(cfg_raw.options)?,
                keybind: cfg_raw.keybind,
            }),
            FeedType::Path => Ok(FeedConfig {
                options: parse_path_entries_options(cfg_raw.options)?,
                keybind: cfg_raw.keybind,
            }),
        })
        .collect::<Result<Vec<FeedConfig>>>()?;

    Ok(Config {
        feeds: feed_cfg,
        general: GeneralConfig {
            fallthrough: cfg.general.fallthrough,
            loading: cfg.general.loading,
            // @todo try to read env var
            terminal: cfg.general.terminal.unwrap_or("alacritty -e".to_string()),
            layout: cfg.general.layout,
            height: parse_length(&cfg.general.height)
                .with_context(|| "Failed to parse 'height' in '[General]'.")?,
            font: cfg.general.font,
            font_size: cfg.general.font_size.unwrap_or(10),
            icon_theme: cfg.general.icon_theme,
            icon_size: parse_length(&cfg.general.icon_size.unwrap_or("24px".to_string()))
                .with_context(|| "Failed to parse 'icon_size' in '[General]'.")?,
            text_color: parse_color(&cfg.general.text_color)
                .with_context(|| "Failed to parse 'text_color' in '[General]'.")?,
            bg_color: parse_color(&cfg.general.bg_color)
                .with_context(|| "Failed to parse 'bg_color' in '[General]'.")?,
            text_color_hl: parse_color(&cfg.general.text_color_hl)
                .with_context(|| "Failed to parse 'text_color_hl' in '[General]'.")?,
            bg_color_hl: parse_color(&cfg.general.bg_color_hl)
                .with_context(|| "Failed to parse 'bg_color_hl' in '[General]'.")?,
            input_width: cfg
                .general
                .input_width
                .map(|l| parse_length(&l))
                .transpose()?,
        },
    })
}

fn parse_desktop_entries_options(kvp: BTreeMap<String, String>) -> Result<FeedOptions> {
    let valid_options = ["filter".to_string(), "require_icons".to_string()];
    for key in kvp.keys() {
        if !valid_options.contains(key) {
            return Err(Error::msg(format!(
                "Option '{}' is not supported for feed of type DesktopEntries.",
                key
            )));
        }
    }

    match kvp.get("filter") {
        Some(filter) => Ok(FeedOptions::DesktopEntries(DesktopEntriesConfig {
            filter: parse_filter(filter)?,
            requires_icons: parse_bool(kvp.get("require_icons").unwrap_or(&"false".to_string()))?,
        })),
        None => Err(Error::msg(
            "Missing requirement option 'filter' for feed of type DesktopEntries.",
        )),
    }
}

fn parse_direct_execution_options(_kvp: BTreeMap<String, String>) -> Result<FeedOptions> {
    Ok(FeedOptions::DirectExecution)
}

fn parse_path_entries_options(kvp: BTreeMap<String, String>) -> Result<FeedOptions> {
    let valid_options = ["filter".to_string()];
    for key in kvp.keys() {
        if !valid_options.contains(key) {
            return Err(Error::msg(format!(
                "Option '{}' is not supported for feed of type Path.",
                key
            )));
        }
    }

    match kvp.get("filter") {
        Some(filter) => Ok(FeedOptions::PathEntries(PathEntriesConfig {
            filter: parse_filter(filter)?,
        })),
        None => Err(Error::msg(
            "Missing requirement option 'filter' for feed of type Path.",
        )),
    }
}

/// parse a string as a color value
///
/// Parse a string and interpret it as either a hexadecimal color value,
/// a RGB color value or a RGBA color value.
///
/// Arguments
///
/// * `value` - text containing the color value
fn parse_color(value: &str) -> Result<Color> {
    parse_hex_color(value)
        .transpose()
        .or_else(|| parse_rgb_color(value).transpose())
        .or_else(|| parse_rgba_color(value).transpose())
        .unwrap_or(Err(Error::msg(format!(
            "Value '{}' is not a valid color value",
            value
        ))))
}

/// parse a string as a hexadecimal color value
///
/// String has to start with '#' and be followed hexadecimal values.
/// Both RGB and RGBA are supported.
/// Accepted string are thus `#RRGGBB` and `#RRGGBBAA`.
/// The functions returns an error if it could not successfully parse the color value as hex
/// even though it has the correct prefix.
/// It returns successful with a value if it could parse the value and nothing
/// if the string is not a hexadecimal value.
/// Note that this nested return type is a hack considered acceptable due to its limited scope.
///
/// Arguments
///
/// * `value` - text containing the color value
fn parse_hex_color(value: &str) -> Result<Option<Color>> {
    if let Some(color_value) = value.strip_prefix('#') {
        if color_value.len() == 6 {
            Ok(Some(Color {
                red: u8::from_str_radix(&color_value[0..2], 16)?,
                green: u8::from_str_radix(&color_value[2..4], 16)?,
                blue: u8::from_str_radix(&color_value[4..6], 16)?,
                alpha: 0xFF,
            }))
        } else if color_value.len() == 8 {
            Ok(Some(Color {
                red: u8::from_str_radix(&color_value[0..2], 16)?,
                green: u8::from_str_radix(&color_value[2..4], 16)?,
                blue: u8::from_str_radix(&color_value[4..6], 16)?,
                alpha: u8::from_str_radix(&color_value[6..8], 16)?,
            }))
        } else {
            Err(Error::msg(
                "Value for color values starting with '#' must be 6 digit or 8 digit hex values",
            ))
        }
    } else {
        Ok(None)
    }
}

/// parse a string as a RGB color value
///
/// String has to start with `Rgb(` and end with `)`.
/// In between follows a comma-separated list of three integers ranging from 0 to 255.
/// For example: `Rgb(0, 128, 160)`.
/// The functions returns an error if it could not successfully parse the color value as Rgb()
/// even though it has the correct prefix.
/// It returns successful with a value if it could parse the value and nothing
/// if the string is not a Rgb() value.
/// Note that this nested return type is a hack considered acceptable due to its limited scope.
///
/// Arguments
///
/// * `value` - text containing the color value
fn parse_rgb_color(value: &str) -> Result<Option<Color>> {
    if let Some(color_value) = value
        .strip_prefix("Rgb(")
        .and_then(|val| val.strip_suffix(')'))
    {
        let values: Vec<&str> = color_value.split(',').collect();
        if values.len() == 3 {
            Ok(Some(Color {
                red: values[0].trim().parse()?,
                green: values[1].trim().parse()?,
                blue: values[2].trim()[..values[2].len() - 1].parse()?,
                alpha: 0xFF,
            }))
        } else {
            Err(Error::msg(format!(
                "Value '{}' is not a valid Rgb() color value",
                color_value
            )))
        }
    } else {
        Ok(None)
    }
}

/// parse a string as a RGBA color value
///
/// String has to start with `Rgba(` and end with `)`.
/// In between follows a comma-separated list of four integers ranging from 0 to 255.
/// For example: `Rgb(0, 128, 160, 255)`.
/// The functions returns an error if it could not successfully parse the color value as Rgba()
/// even though it has the correct prefix.
/// It returns successful with a value if it could parse the value and nothing
/// if the string is not a Rgba() value.
/// Note that this nested return type is a hack considered acceptable due to its limited scope.
///
/// Arguments
///
/// * `value` - text containing the color value
fn parse_rgba_color(value: &str) -> Result<Option<Color>> {
    if let Some(color_value) = value
        .strip_prefix("Rgba(")
        .and_then(|val| val.strip_suffix(')'))
    {
        let values: Vec<&str> = color_value.split(',').collect();
        if values.len() == 4 {
            Ok(Some(Color {
                red: values[0].trim().parse()?,
                green: values[1].trim().parse()?,
                blue: values[2].trim().parse()?,
                alpha: values[3].trim()[..values[3].len() - 1].parse()?,
            }))
        } else {
            Err(Error::msg(format!(
                "Value '{}' is not a valid Rgba() color value",
                color_value
            )))
        }
    } else {
        Ok(None)
    }
}

/// parse a string to a length value
///
/// Take a string and parse it as a length value.
/// The string is assumed to start with a digits followed by a unit.
/// Valid units are 'px', 'pt', '%' and no unit.
/// In the latter case, 'px' is assumed as default.
///
/// # Arguments
///
/// * `value` - text to parse
fn parse_length(value: &str) -> Result<Length> {
    let (number, unit) = match value.find(|c: char| !c.is_ascii_digit()) {
        Some(pos) => value.split_at(pos),
        None => (value, ""),
    };
    let length = number.parse::<Pixels>()?;

    match unit {
        "px" => Ok(Length::Pixel(length)),
        "%" => Ok(Length::Percentage(length)),
        "pt" => Ok(Length::Point(length)),
        "" => Ok(Length::Pixel(length)),
        _ => Err(Error::msg("Invalid unit for length value")),
    }
}

/// parse a string to a filter type
///
/// Take a string and parse it as a filter type.
/// Valid strings are 'Contains', 'Fuzzy' and 'StartsWith'.
///
/// # Arguments
///
/// * `value` - text to parse
fn parse_filter(value: &str) -> Result<FilterType> {
    match value {
        "Contains" => Ok(FilterType::Contains),
        "Fuzzy" => Ok(FilterType::Fuzzy),
        "StartsWith" => Ok(FilterType::StartsWith),
        _ => Err(Error::msg(format!(
            "Not supported value '{}' found. Should either be a valid filter type",
            value,
        ))),
    }
}

/// parse a string to a bool
///
/// Take a string and parse it as a bool.
/// Valid strings are 'false' and 'true'.
///
/// # Arguments
///
/// * `value` - text to parse
fn parse_bool(value: &str) -> Result<bool> {
    match value {
        "false" => Ok(false),
        "true" => Ok(true),
        _ => Err(Error::msg(format!(
            "Not supported value '{}' found. Should either be 'true' or 'false'",
            value,
        ))),
    }
}

#[cfg(test)]
#[path = "test_config_file.rs"]
mod tests;
