/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use anyhow::{Context, Result};
use fontconfig::Fontconfig;
use freetype::{face::LoadFlag, Library};

pub struct Glyph {
    pub bitmap: Vec<u8>,
    pub width: u32,
    pub height: u32,
    pub advance_x: i32,
    pub ascender: i32,
    pub left_offset: i32,
}

#[derive(Debug, Eq, PartialEq)]
pub struct Font {
    face: freetype::Face,
    pub ascender: i32,
    pub descender: i32,
    pub x_scale: i64,
    pub y_scale: i64,
}

impl Font {
    pub fn new(font_file: &str, size_pt: isize, dpi: u32) -> Result<Font> {
        let fc = Fontconfig::new()
            .ok_or(freetype::Error::Unknown)
            .with_context(|| "Error while loading font")?;
        // `Fontconfig::find()` returns `Option` (will rarely be `None` but still could be)
        let font = fc
            .find(font_file, None)
            .ok_or(freetype::Error::Unknown)
            .with_context(|| "Error while loading font")?;
        // println!("Name: {}\nPath: {}", font.name, font.path.display());

        // +++ get glyph data
        // Init the library
        // @question who owns the library? Do I need to keep it?
        let lib = Library::init().with_context(|| "Error while loading font")?;
        let face = lib
            .new_face(font.path, 0)
            .with_context(|| "Error while loading font")?;
        face.set_char_size(0, size_pt * 64, dpi, dpi)
            .with_context(|| "Error while loading font")?;
        let ascender = (face
            .size_metrics()
            .with_context(|| "Error while loading font")?
            .ascender
            / 64) as i32;
        let descender = (face
            .size_metrics()
            .with_context(|| "Error while loading font")?
            .descender
            / 64) as i32;
        let x_scale = face
            .size_metrics()
            .with_context(|| "Error while loading font")?
            .x_scale;
        let y_scale = face
            .size_metrics()
            .with_context(|| "Error while loading font")?
            .y_scale;

        Ok(Font {
            face,
            ascender,
            descender,
            x_scale,
            y_scale,
        })
    }

    pub fn get_char(&self, ch: char) -> Result<Glyph> {
        // @todo in case of an error, check if error is related missing fonts and substitude
        self.face
            .load_char(ch as usize, LoadFlag::RENDER)
            .with_context(|| "Error while getting character")?; // expects UTF-32, check that cast is okay

        // Get the glyph instance
        let glyph = self.face.glyph();
        let bitmap = glyph.bitmap();
        let buffer = bitmap.buffer().to_vec();

        Ok(Glyph {
            bitmap: buffer,
            width: bitmap.width() as u32,
            height: bitmap.rows() as u32,
            advance_x: (glyph.linear_hori_advance() / 65536) as i32,
            ascender: (glyph.metrics().horiBearingY / 64) as i32,
            left_offset: (glyph.metrics().horiBearingX / 64) as i32,
        })
    }
}
