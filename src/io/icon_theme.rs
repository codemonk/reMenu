// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::{
    core::Pixmap,
    io::fd_org_ini_parser::{IniFile, Section},
};
use image::{imageops::resize, ImageReader};
use log::{debug, warn};
use resvg::{
    tiny_skia::Pixmap as SPixmap,
    usvg::{self, Transform},
};
use std::{
    collections::{hash_map::Entry, HashMap, VecDeque},
    ffi::OsStr,
    fs,
    io::{BufRead, BufReader},
    path::Path,
    path::PathBuf,
    str::FromStr,
};
use thiserror::Error;

/// Errors that can occur when loading an icon theme
#[derive(Error, Debug, PartialEq, Eq)]
pub enum IconThemeError {
    /// loading of an image file for an icon failed
    #[error("Could not find or load icon: `{0}`")]
    LoadImageError(String),

    /// loading of the theme index failed
    #[error("Could not find or load theme index")]
    LoadIndexError,

    /// parsing of an theme index failed
    #[error("Could not parse theme index: `{0}`")]
    ParseIndexError(String),

    /// could not find home directory
    #[error("Home directory not found")]
    NoHome,
}

/// directory types as specified by icon theme specification
#[derive(Clone, Debug, Eq, PartialEq)]
enum DirectoryType {
    Threshold,
    Fixed,
    Scalable,
}

impl FromStr for DirectoryType {
    type Err = IconThemeError;

    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "Threshold" => Ok(Self::Threshold),
            "Fixed" => Ok(Self::Fixed),
            "Scalable" => Ok(Self::Scalable),
            _ => Err(IconThemeError::ParseIndexError(
                "Could not parse string to Directory type.".to_string(),
            )),
        }
    }
}

/// per directory settings for a theme
#[derive(Clone, Debug, Eq, PartialEq)]
struct IconDirectory {
    /// size of icons
    size: i32,

    /// scaling factor for icons (defaults to zero)
    scale: i32,

    /// context information
    context: String,

    /// the type of directory
    r#type: DirectoryType,

    /// maximum size of a (scaled) icon (defaults to size)
    max_size: i32,

    /// minimum size of a (scaled) icon (defaults to size)
    min_size: i32,

    /// threshold (defaults to 2)
    threshold: i32,
}

/// global settings for a theme
#[derive(Clone, Debug, Eq, PartialEq)]
struct ThemeSettings {
    // name of the theme
    name: String,

    /// comment for the theme
    comment: String,

    /// name of the themes, this theme inherits
    inherits: Vec<String>,

    /// flag if this theme is hidden from the user (defaults to false)
    hidden: bool,

    /// examples
    example: Option<String>,
}

/// an icone theme
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct IconTheme {
    /// settings for the theme
    settings: ThemeSettings,

    /// base directories for this theme
    theme_directories: Vec<PathBuf>,

    /// subdirectories of the theme (includes standard a scaled directories)
    directories: HashMap<String, IconDirectory>,

    /// themes, this theme inherits (only set for the root theme)
    inherited_themes: Vec<IconTheme>,

    /// desired size for icons
    size: i32,
}

impl IconTheme {
    /// load an icon theme
    ///
    /// Load an icon theme and all of its inherited themes.
    /// Fails if either the selected or default theme could not be loaded.
    /// If an inherited seem could not be found, it is ignored with a warning.
    /// The rationale is that some seems provide optional inherited themes that
    /// can be used if the user installed them but do not have to.
    ///
    /// # Arguments:
    ///
    /// * `name` - name of the theme to load
    /// * `size` - desired icon size
    pub fn open(name: &str, size: i32) -> Result<IconTheme, IconThemeError> {
        let default_name = "hicolor";
        let mut theme = Self::read(name, size)?;

        // only themes other than default have inherited themes
        if name != default_name {
            let mut backlog: VecDeque<String> = VecDeque::new();
            for inherited_theme in &theme.settings.inherits {
                if inherited_theme != default_name {
                    backlog.push_back(inherited_theme.to_owned());
                }
            }

            while !backlog.is_empty() {
                let current_theme_name = backlog.pop_front().unwrap();
                match Self::read(&current_theme_name, size) {
                    Ok(current_theme) => {
                        for inherited_theme in &current_theme.settings.inherits {
                            if inherited_theme != default_name {
                                backlog.push_back(inherited_theme.to_owned());
                            }
                        }
                        theme.inherited_themes.push(current_theme);
                    }
                    Err(e) => {
                        warn!("Failed to read inherited theme '{}' with error. Ignoring it. Error: {}.", current_theme_name, e)
                    }
                }
            }

            // default theme always comes last
            let current_theme = Self::read(default_name, size)?;
            theme.inherited_themes.push(current_theme);
        }

        Ok(theme)
    }

    /// locate a given icon in theme
    ///
    /// Look for the given icon name in theme.
    /// Return its path if found.
    ///
    /// # Arguments:
    ///
    /// * `icon_name` - name of the icon the find
    pub fn locate(&self, icon_name: &str) -> Result<PathBuf, IconThemeError> {
        lookup_icon(icon_name, self.size, 1, self)
            .or_else(|| {
                self.inherited_themes
                    .iter()
                    .map(|theme| lookup_icon(icon_name, self.size, 1, theme))
                    .find(|result| result.is_some())
                    .flatten()
            })
            .ok_or(IconThemeError::LoadImageError(
                "No suitable icon in theme".to_string(),
            ))
    }

    /// load an icon from theme
    ///
    /// Look for the given icon name in theme.
    /// Load its contents into a pixmap if found.
    ///
    /// # Arguments:
    ///
    /// * `icon` - name of the icon the find
    pub fn load_scaled(&self, icon_name: &str) -> Result<Pixmap, IconThemeError> {
        let icon_path = self.locate(icon_name)?;

        match icon_path.extension().and_then(OsStr::to_str) {
            Some("png") => self.load_png_scaled(&icon_path),
            Some("svg") => self.load_svg_scaled(&icon_path),
            _ => Err(IconThemeError::LoadImageError(format!(
                "Unsupported or unknown format for icon `{}`",
                icon_path.to_string_lossy()
            ))),
        }
    }

    /// get configured icon size
    pub fn size(&self) -> i32 {
        self.size
    }

    /// find and parse the theme index for an theme
    ///
    /// Search for the theme index of a theme.
    /// Parse it and return an IconTheme.
    ///
    /// # Arguments:
    ///
    /// * `name` - name of the theme to load
    /// * `size` - desired icon size
    fn read(name: &str, size: i32) -> Result<IconTheme, IconThemeError> {
        let base_directories = get_base_directories()?;

        let theme_directories: Vec<PathBuf> = base_directories
            .into_iter()
            .filter(|dir| dir.exists())
            .filter_map(|dir| {
                let subdir = dir.join(name);
                match subdir.exists() {
                    true => Some(subdir),
                    false => None,
                }
            })
            .collect();

        let reader = theme_directories
            .clone()
            .into_iter()
            .filter_map(|dir| {
                let idx_file = dir.join("index.theme");
                match idx_file.exists() {
                    true => Some(idx_file),
                    false => None,
                }
            })
            .take(1)
            .filter_map(|idx_file| {
                let file_hdl = fs::File::open(idx_file).ok()?;
                let rdr = BufReader::new(file_hdl);
                Some(rdr)
            })
            .nth(0)
            .ok_or(IconThemeError::LoadIndexError)?;

        let (settings, icon_dirs) = parse_theme_index(reader)?;

        Ok(IconTheme {
            settings,
            theme_directories,
            directories: icon_dirs,
            size,
            inherited_themes: Vec::new(),
        })
    }

    /// load contents of PNG file into a pixmap
    ///
    /// Load contents of PNG file.
    /// Scale it to icon size and store it in a pixmap.
    ///
    /// # Arguments:
    ///
    /// * `path` - path of the file to load
    fn load_png_scaled(&self, path: &Path) -> Result<Pixmap, IconThemeError> {
        let mut reader = ImageReader::open(path)
            .map_err(|err| IconThemeError::LoadImageError(err.to_string()))?;
        reader.set_format(image::ImageFormat::Png);

        let icon = reader
            .decode()
            .map_err(|err| IconThemeError::LoadImageError(err.to_string()))?
            .into_rgba8();

        let icon = resize(
            &icon,
            self.size as u32,
            self.size as u32,
            image::imageops::FilterType::Nearest,
        );
        let width = icon.width();
        let height = icon.height();

        Pixmap::new(icon.into_vec(), width, height).ok_or(IconThemeError::LoadImageError(
            "Failed to create pixmap from loaded image".to_string(),
        ))
    }

    /// load contents of SVG file into a pixmap
    ///
    /// Load contents of SVG file.
    /// Scale it to icon size and store it in a pixmap.
    ///
    /// # Arguments:
    ///
    /// * `path` - path of the file to load
    fn load_svg_scaled(&self, path: &Path) -> Result<Pixmap, IconThemeError> {
        debug!("Load icon `{}.", path.to_string_lossy());
        let data = fs::read(path).map_err(|_e| {
            IconThemeError::LoadImageError(format!(
                "Could not load image `{}`",
                path.to_string_lossy()
            ))
        })?;

        let tree =
            usvg::Tree::from_data(data.as_slice(), &usvg::Options::default()).map_err(|_e| {
                IconThemeError::LoadImageError(format!(
                    "Could not parse SVG for icon `{}`.",
                    path.to_string_lossy()
                ))
            })?;

        debug!("Render icon `{}.", path.to_string_lossy());
        let mut pixmap =
            SPixmap::new(self.size as u32, self.size as u32).expect("Creation of pixmap failed");
        let mut mpm = pixmap.as_mut();

        // scale to target size. This might deform, if image is to square.
        // Icons are expected to be square, though.
        let transform = Transform::from_scale(
            self.size as f32 / tree.size().width(),
            self.size as f32 / tree.size().height(),
        );
        resvg::render(&tree, transform, &mut mpm);

        Pixmap::new(pixmap.data().to_owned(), self.size as u32, self.size as u32).ok_or(
            IconThemeError::LoadImageError("Failed to create pixmap from loaded image".to_string()),
        )
    }
}

/// get base directories for searching theme
fn get_base_directories() -> Result<Vec<PathBuf>, IconThemeError> {
    let home_icons_dir = home::home_dir()
        .ok_or(IconThemeError::NoHome)?
        .join(".icons");
    let mut data_icon_dirs = xdg::BaseDirectories::with_prefix("icons")
        .unwrap()
        .get_data_dirs();
    let pixmaps_dir = "/usr/share/pixmaps";
    let mut base_dirs = vec![home_icons_dir];
    base_dirs.append(&mut data_icon_dirs);
    base_dirs.push(pixmaps_dir.into());

    Ok(base_dirs)
}

/// parse a theme index
///
/// # Arguments
///
/// * `reader` - raw data for parsing
fn parse_theme_index<R: BufRead>(
    reader: R,
) -> Result<(ThemeSettings, HashMap<String, IconDirectory>), IconThemeError> {
    let theme_raw =
        IniFile::from_file(reader).map_err(|e| IconThemeError::ParseIndexError(e.to_string()))?;
    let (header, tail) =
        theme_raw
            .sections
            .split_first()
            .ok_or(IconThemeError::ParseIndexError(
                "No sections in theme index file.".to_string(),
            ))?;
    let (settings, dirs, scaled_dirs) = parse_header_theme_index_settings(header)?;

    let icon_directories = tail
        .iter()
        .filter_map(|section| parse_directory(section).inspect_err(|e| warn!("Skipping directory {} because of the following error. Results might be unexpected. Error: {}", section.title, e)).ok())
        .fold(
            HashMap::new(),
            |mut dirs, (name, dir)| {
                match dirs.entry(name) {
                    Entry::Occupied(e) => warn!("Duplicate directory entry {} in theme index. Ignoring one entry.", e.key()),
                    Entry::Vacant(e) => { e.insert(dir); },
                };
                dirs
            }
        );

    let mut merged_dirs = dirs;
    merged_dirs.extend(scaled_dirs);
    merged_dirs.sort();
    merged_dirs.dedup();

    let final_icon_dirs = merged_dirs
        .iter()
        .filter_map(|dir| match icon_directories.get(dir.as_str()) {
            Some(icon_dir) => Some((dir, icon_dir)),
            None => {
                warn!("No matching directory entry for directory `{}`", dir);
                None
            }
        })
        .fold(HashMap::new(), |mut icon_dirs, (dir, icon_dir)| {
            // @todo avoid duplicates
            icon_dirs.insert(dir.to_string(), icon_dir.to_owned());
            icon_dirs
        });

    Ok((settings, final_icon_dirs))
}

/// parse header section of theme index
///
/// Parse the header section of a theme index.
/// Read of theme settings.
/// Nonexisting settings are set to defaults.
/// Also read theme directories.
///
/// # Arguments
///
/// * `section` - section from theme index to parse
fn parse_header_theme_index_settings(
    section: &Section,
) -> Result<(ThemeSettings, Vec<String>, Vec<String>), IconThemeError> {
    if section.title == "Icon Theme" {
        let settings = ThemeSettings {
            name: section
                .entries
                .get("Name")
                .ok_or(IconThemeError::ParseIndexError(
                    "Could not find setting `Name`.".to_string(),
                ))?
                .clone(),
            comment: section
                .entries
                .get("Comment")
                .ok_or(IconThemeError::ParseIndexError(
                    "Could not find setting `Comment`".to_string(),
                ))?
                .clone(),
            hidden: section
                .entries
                .get("Hidden")
                .map(|v| v == "true")
                .unwrap_or(false),
            inherits: section
                .entries
                .get("Inherits")
                .map(|v| v.split(',').map(|name| name.trim().to_string()).collect())
                .unwrap_or_default(),
            example: section.entries.get("Example").cloned(),
        };

        let directories: Vec<String> = section
            .entries
            .get("Directories")
            .ok_or(IconThemeError::ParseIndexError(
                "Could not find settings `Directories`".to_string(),
            ))?
            .split(',')
            .map(|dir| dir.trim())
            .map(|dir| dir.to_string())
            .filter(|dir| !dir.is_empty())
            .collect();

        let scaled_directories: Vec<String> = section
            .entries
            .get("ScaledDirectories")
            .map(|val| val.to_owned())
            .unwrap_or_default()
            .split(',')
            .map(|dir| dir.trim())
            .map(|dir| dir.to_string())
            .filter(|dir| !dir.is_empty())
            .collect();

        Ok((settings, directories, scaled_directories))
    } else {
        Err(IconThemeError::ParseIndexError(
            "Could not find section `Icon Theme`".to_string(),
        ))
    }
}

/// parse directory in theme
///
/// Parse directory section of theme index.
/// Return settings for this directory.
///
/// # Arguments
///
/// * `section` - section in theme index to parse as directory
fn parse_directory(section: &Section) -> Result<(&str, IconDirectory), IconThemeError> {
    let size: i32 = section
        .entries
        .get("Size")
        .ok_or(IconThemeError::ParseIndexError(
            "Could not find entry `Size` for directory section.".to_string(),
        ))?
        .parse()
        .map_err(|_| {
            IconThemeError::ParseIndexError("Could not convert `Size` to int.".to_string())
        })?;

    let scale = section
        .entries
        .get("Scale")
        .map(|v| v.parse())
        .transpose()
        .map_err(|_| {
            IconThemeError::ParseIndexError("`Scale` cannot be parsed as an integer".to_string())
        })?
        .unwrap_or(1);

    let context = section
        .entries
        .get("Context")
        .ok_or(IconThemeError::ParseIndexError(
            "Could not find entry `Context` for directory section.".to_string(),
        ))?
        .to_owned();

    let entry_type = section
        .entries
        .get("Type")
        .ok_or(IconThemeError::ParseIndexError(
            "Could not find entry `Type` for directory section.".to_string(),
        ))?
        .parse()?;

    let max_size = section
        .entries
        .get("MaxSize")
        .map(|v| v.parse())
        .transpose()
        .map_err(|_| {
            IconThemeError::ParseIndexError("`MaxSize` cannot be parsed as an integer".to_string())
        })?
        .unwrap_or(size);

    let min_size = section
        .entries
        .get("MinSize")
        .map(|v| v.parse())
        .transpose()
        .map_err(|_| {
            IconThemeError::ParseIndexError("`MinSize` cannot be parsed as an integer".to_string())
        })?
        .unwrap_or(size);

    let threshold = section
        .entries
        .get("Threshold")
        .map(|v| v.parse())
        .transpose()
        .map_err(|_| {
            IconThemeError::ParseIndexError(
                "`Threshold` cannot be parsed as an integer".to_string(),
            )
        })?
        .unwrap_or(2);

    Ok((
        section.title.as_str(),
        IconDirectory {
            size,
            scale,
            context,
            r#type: entry_type,
            max_size,
            min_size,
            threshold,
        },
    ))
}

/// lookup icon in theme
///
/// Lookup icon in theme according to spec.
/// Search in theme and check if file exists.
///
/// # Arguments
///
/// * `name` - name of the icon
/// * `size` - desired icon size
/// * `scale` - desired scale
/// * `theme` - theme to search in
fn lookup_icon(name: &str, size: i32, scale: i32, theme: &IconTheme) -> Option<PathBuf> {
    // note: intendionally no support for xpm
    let extensions = ["png", "svg"];
    for subdir in theme.directories.iter() {
        for directory in &theme.theme_directories {
            for extension in extensions {
                if directory_matches_size(subdir.1, size, scale) {
                    let file_name = build_icon_path(directory, subdir.0, name, extension);
                    if file_name.is_file() {
                        return Some(file_name);
                    }
                }
            }
        }
    }
    let mut closest_filename = PathBuf::new();
    let mut minimal_size = i32::MAX;
    for subdir in theme.directories.iter() {
        for directory in &theme.theme_directories {
            for extension in extensions {
                let file_name = build_icon_path(directory, subdir.0, name, extension);
                if file_name.is_file()
                    && directory_size_distance(subdir.1, size, scale) < minimal_size
                {
                    closest_filename = file_name;
                    minimal_size = directory_size_distance(subdir.1, size, scale);
                }
            }
        }
    }
    if closest_filename.is_file() {
        return Some(closest_filename);
    }

    None
}

/// directory_matches_size according to spec
fn directory_matches_size(dir: &IconDirectory, icon_size: i32, icon_scale: i32) -> bool {
    if dir.scale != icon_scale {
        false
    } else {
        match dir.r#type {
            DirectoryType::Fixed => dir.size == icon_size,
            DirectoryType::Scalable => dir.min_size <= icon_size && icon_size <= dir.max_size,
            DirectoryType::Threshold => {
                dir.size - dir.threshold <= icon_size && icon_size <= dir.size + dir.threshold
            }
        }
    }
}

/// directory_size_distance according to spec
/// slightly modified for readability (use match statement)
fn directory_size_distance(dir: &IconDirectory, icon_size: i32, icon_scale: i32) -> i32 {
    match dir.r#type {
        DirectoryType::Fixed => num_traits::abs(dir.size * dir.scale - icon_size * icon_scale),
        DirectoryType::Scalable => {
            if icon_size * icon_scale < dir.min_size * dir.scale {
                dir.min_size * dir.scale - icon_size * icon_scale
            } else if icon_size * icon_scale > dir.max_size * dir.scale {
                icon_size * icon_scale - dir.max_size * dir.scale
            } else {
                0
            }
        }
        DirectoryType::Threshold => {
            if icon_size * icon_scale < (dir.size - dir.threshold) * dir.scale {
                dir.min_size * dir.scale - icon_size * icon_scale
            } else if icon_size * icon_size > (dir.size + dir.threshold) * dir.scale {
                icon_size * icon_size - dir.max_size * dir.scale
            } else {
                0
            }
        }
    }
}

/// build_icon_path according to spec
/// slight deviation as theme name is already part of directory
fn build_icon_path(directory: &Path, subdir: &str, icon_name: &str, extension: &str) -> PathBuf {
    let mut pb = PathBuf::with_capacity(
        directory.as_os_str().len() + subdir.len() + icon_name.len() + extension.len() + 3, // number of separators ('/' and '.')
    );

    pb.push(directory);
    pb.push(subdir);
    pb.push(icon_name);

    let final_extension = match pb.extension() {
        Some(name) => {
            let mut buf = name.to_owned();
            buf.push(".");
            buf.push(extension);
            buf
        }
        None => extension.into(),
    };

    pb.set_extension(final_extension);

    pb
}

#[cfg(test)]
#[path = "test_icon_theme.rs"]
mod tests;
