use super::*;
use std::io::Cursor;

// tests include common valid and invalid line pattern and check they correct handling
// Every supported type of ini file is covered with a single test of a valid file.
// These also cover many types of lines.
// Therefore, line test mostly cover special and error cases.

#[test]
fn test_parse_example_theme_index() {
    let entry = r"[Icon Theme]
Name=Birch
Name[sv]=Björk
Comment=Icon theme with a wooden look
Comment[sv]=Träinspirerat ikontema
Inherits=wood,default
Directories=48x48/apps,48x48@2/apps,48x48/mimetypes,32x32/apps,32x32@2/apps,scalable/apps,scalable/mimetypes

[scalable/apps]
Size=48
Type=Scalable
MinSize=1
MaxSize=256
Context=Applications

[scalable/mimetypes]
Size=48
Type=Scalable
MinSize=1
MaxSize=256
Context=MimeTypes

[32x32/apps]
Size=32
Type=Fixed
Context=Applications

[32x32@2/apps]
Size=32
Scale=2
Type=Fixed
Context=Applications

[48x48/apps]
Size=48
Type=Fixed
Context=Applications

[48x48@2/apps]
Size=48
Scale=2
Type=Fixed
Context=Applications

[48x48/mimetypes]
Size=48
Type=Fixed
Context=MimeTypes";

    // @note: The use of "default" in this example is unclear.
    // Since nothing is said in the spec, consider it to be a normal theme name
    let result = parse_theme_index(Cursor::new(entry));
    let settings = ThemeSettings {
        name: "Birch".to_string(),
        comment: "Icon theme with a wooden look".to_string(),
        inherits: vec!["wood".to_string(), "default".to_string()],
        hidden: false,
        example: None,
    };

    let directories = HashMap::from([
        (
            "scalable/apps".to_string(),
            IconDirectory {
                size: 48,
                context: "Applications".to_string(),
                r#type: DirectoryType::Scalable,
                scale: 1,
                max_size: 256,
                min_size: 1,
                threshold: 2,
            },
        ),
        (
            "scalable/mimetypes".to_string(),
            IconDirectory {
                size: 48,
                context: "MimeTypes".to_string(),
                r#type: DirectoryType::Scalable,
                scale: 1,
                max_size: 256,
                min_size: 1,
                threshold: 2,
            },
        ),
        (
            "32x32/apps".to_string(),
            IconDirectory {
                size: 32,
                context: "Applications".to_string(),
                r#type: DirectoryType::Fixed,
                scale: 1,
                max_size: 32,
                min_size: 32,
                threshold: 2,
            },
        ),
        (
            "32x32@2/apps".to_string(),
            IconDirectory {
                size: 32,
                context: "Applications".to_string(),
                r#type: DirectoryType::Fixed,
                scale: 2,
                max_size: 32,
                min_size: 32,
                threshold: 2,
            },
        ),
        (
            "48x48/apps".to_string(),
            IconDirectory {
                size: 48,
                context: "Applications".to_string(),
                r#type: DirectoryType::Fixed,
                scale: 1,
                max_size: 48,
                min_size: 48,
                threshold: 2,
            },
        ),
        (
            "48x48@2/apps".to_string(),
            IconDirectory {
                size: 48,
                context: "Applications".to_string(),
                r#type: DirectoryType::Fixed,
                scale: 2,
                max_size: 48,
                min_size: 48,
                threshold: 2,
            },
        ),
        (
            "48x48/mimetypes".to_string(),
            IconDirectory {
                size: 48,
                context: "MimeTypes".to_string(),
                r#type: DirectoryType::Fixed,
                scale: 1,
                max_size: 48,
                min_size: 48,
                threshold: 2,
            },
        ),
    ]);
    assert_eq!(result, Ok((settings, directories)));
}

#[test]
fn test_parse_theme_index_missing_section() {
    let entry = r"[Icon Theme]
Name=Birch
Name[sv]=Björk
Comment=Icon theme with a wooden look
Comment[sv]=Träinspirerat ikontema
Inherits=wood,default
Directories=scalable/apps,scalable/mimetypes

[scalable/apps]
Size=48
Type=Scalable
MinSize=1
MaxSize=256
Context=Applications

[scalable/mimetypes]";

    let result = parse_theme_index(Cursor::new(entry));
    let settings = ThemeSettings {
        name: "Birch".to_string(),
        comment: "Icon theme with a wooden look".to_string(),
        inherits: vec!["wood".to_string(), "default".to_string()],
        hidden: false,
        example: None,
    };

    let directories = HashMap::from([(
        "scalable/apps".to_string(),
        IconDirectory {
            size: 48,
            context: "Applications".to_string(),
            r#type: DirectoryType::Scalable,
            scale: 1,
            max_size: 256,
            min_size: 1,
            threshold: 2,
        },
    )]);
    assert_eq!(result, Ok((settings, directories)));
}

#[test]
fn test_path_for_names_with_dots() {
    let dir = "/usr/share/icons".as_ref();
    let subdir = "32x32@2/apps";
    let icon_name = "xdg.app.terminal";
    let extension = "png";
    let icon_path = build_icon_path(dir, subdir, icon_name, extension);
    assert_eq!(
        icon_path,
        PathBuf::from_str("/usr/share/icons/32x32@2/apps/xdg.app.terminal.png").unwrap()
    );
}

#[test]
fn test_path_for_names_with_hyphens() {
    let dir = "/usr/share/icons/".as_ref();
    let subdir = "48x48/apps/";
    let icon_name = "xfce-settings";
    let extension = "png";
    let icon_path = build_icon_path(dir, subdir, icon_name, extension);
    assert_eq!(
        icon_path,
        PathBuf::from_str("/usr/share/icons/48x48/apps/xfce-settings.png").unwrap()
    );
}

// @todo test missing inherited theme. Requires mocked disk or abstraction
