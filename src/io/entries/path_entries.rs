/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
use crate::core::Entry;
use std::{path::Path, path::PathBuf};

/// load and return entries from a path
///
///
/// # Return:
///
/// * vector of entries
pub fn load() -> Vec<Entry> {
    let mut open_pathes = search_pathes();
    let mut entries = Vec::new();

    while let Some(current_path) = open_pathes.pop() {
        if current_path.is_dir() {
            let (mut binaries, mut subdirs) = traverse_dir(&current_path);
            open_pathes.append(&mut subdirs);
            entries.append(&mut binaries);
        }
    }

    entries.sort();
    entries.dedup();

    entries
}

fn search_pathes() -> Vec<PathBuf> {
    // @todo
    match std::env::var_os("PATH") {
        Some(paths) => std::env::split_paths(&paths).collect(),
        None => Vec::new(),
    }
}

fn traverse_dir(dir_path: &Path) -> (Vec<Entry>, Vec<PathBuf>) {
    let mut entries = Vec::new();
    let mut subdirs = Vec::new();
    if let Ok(path_entries) = dir_path.read_dir() {
        (entries, subdirs) = path_entries
            .into_iter()
            .filter_map(|e| e.ok())
            .map(|e| e.path())
            .fold((Vec::new(), Vec::new()), |mut result, p| {
                add_entry(p, &mut result.0, &mut result.1);
                result
            });
    }

    (entries, subdirs)
}

fn add_entry(path: PathBuf, entries: &mut Vec<Entry>, subdirs: &mut Vec<PathBuf>) {
    if path.is_dir() {
        subdirs.push(path);
    } else if path.is_file() {
        entries.push(Entry {
            name: path
                .file_name()
                .expect("Expected a file name")
                .to_string_lossy()
                .to_string(),
            cmd: path.to_string_lossy().to_string(),
            comment: Some(path.to_string_lossy().to_string()),
            terminal: None,
            icon: None,
        });
    }
}
