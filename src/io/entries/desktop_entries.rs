// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use crate::core::Entry;
use anyhow::Result;
use log::{debug, info, warn};
use std::{
    fs,
    io::{BufRead, BufReader},
};

/// Representation of a desktop entry read from file
///
/// Structure used to temporarily store a desktop entry.
/// It will be converted to an [`Entry`], if necessary fields are present.
/// Different to an [`Entry`], it represents the fact, that fields in a desktop
/// entry file can be left out.
/// @todo Should be replaced by a struct based on ini parser
#[derive(Clone, Debug, Default)]
struct MaybeEntry {
    cmd: Option<String>,
    name: Option<String>,
    comment: Option<String>,
    icon: Option<String>,
    terminal: Option<bool>,
    entry_type: Option<String>,
}

/// load and return desktop entries
///
/// Load desktop entries from standard pathes and return them
/// Currently ignores subfolders as no use case in know where they contain apps.
///
/// # Arguments:
///
/// * `with_icons_only` - only return entries that have an icon
///
/// # Return:
///
/// * vector of entries
pub fn load() -> Vec<Entry> {
    // @todo simplify logging by introducing a warn_if! macro
    let mut entries: Vec<Entry> = get_search_pathes()
        .iter()
        .filter_map(|dir| {
            let rd = std::fs::read_dir(dir);
            if rd.is_err() {
                warn!("Could not read directory {}", dir);
            }
            rd.ok()
        })
        .flatten()
        .filter_map(|candidate| candidate.map(|entry| entry.path()).ok())
        .inspect(|path| {
            if path.is_dir() {
                warn!(
                    "Found entry that is a directory: {}. Its contents will be skipped",
                    path.to_str().unwrap_or("")
                );
            }
        })
        .filter(|path| !path.is_dir())
        .filter_map(|path| {
            if let Ok(file) = fs::File::open(&path) {
                Some(BufReader::new(file))
            } else {
                warn!(
                    "File could not be opened and will be skipped: {}",
                    &path.to_str().unwrap_or("")
                );
                None
            }
        })
        .filter_map(|reader| {
            let entry = read_entry(reader);
            if entry.is_none() {
                warn!("Could not read entry.");
            }
            entry
        })
        .collect();

    entries.sort();
    entries.dedup();

    entries
}

/// Get list of directories that shall be searched for desktop entries
///
/// Based on XDG directories, create a list of directories that will be searched for desktop entries.
///
/// # Return:
///
/// * vector of search pathes
fn get_search_pathes() -> Vec<String> {
    let mut pathes = xdg::BaseDirectories::with_prefix("applications")
        .unwrap()
        .get_data_dirs();

    let data_home = xdg::BaseDirectories::with_prefix("applications")
        .unwrap()
        .get_data_home();
    pathes.push(data_home);

    let pathes = pathes
        .iter()
        .map(|path| path.to_str())
        .filter_map(|path| path.map(|p| p.to_string()))
        .collect();
    info!("Collected all pathes for desktop entries.");

    pathes
}

/// read a single desktop entry
///
/// Parse the data provided by `reader` into [`Entry`].
///
/// # Arguments:
///
/// * `reader` - buffered reader providing the data for a desktop entry
///
/// # Return:
///
/// * a [`Entry`] on success
fn read_entry<R: BufRead>(reader: R) -> Option<Entry> {
    let mut entry: Option<Entry> = None;
    let lines = reader.lines();
    let maybe_entry = lines
        .map_while(Result::ok) // ignore lines with error
        .skip_while(|line| line != "[Desktop Entry]")
        .skip(1)
        .take_while(|line| !line.starts_with('['))
        .filter_map(|line| parse_key_value_pair(&line))
        .fold(MaybeEntry::default(), update_with_key_value_pair);

    debug!("Read potential entry {:?}.", maybe_entry);
    if let (Some(entry_type), Some(cmd), Some(name)) =
        (maybe_entry.entry_type, maybe_entry.cmd, maybe_entry.name)
    {
        if entry_type == "Application" {
            entry = Some(Entry {
                cmd,
                name,
                comment: maybe_entry.comment,
                terminal: maybe_entry.terminal,
                // @todo call path transformer
                icon: maybe_entry.icon,
            })
        }
    }
    debug!("Read entry {:?}.", entry);

    entry
}

/// parse a string as a key-value pair
///
/// Parse a single line, splitting it at '='.
/// This follows the rules for key-value pairs in desktop entry files.
///
/// # Arguments:
///
/// * `s` - string to be parsed
///
/// # Return:
///
/// * a key-value pair of string on success
fn parse_key_value_pair(s: &str) -> Option<(String, String)> {
    let fch = s.chars().next();

    if !fch?.is_ascii_alphanumeric() {
        return None;
    }

    if let Some((key, value)) = s.split_once('=') {
        if key.chars().all(|ch| ch.is_ascii_alphanumeric()) {
            // we ignore keys with '-' for now
            return Some((key.trim().to_owned(), value.trim().to_owned()));
        }
    }

    None
}

/// insert a key-value pair into given [`MaybeEntry`]
///
/// Match a given key-value against the fields of [`MaybeEntry`].
/// If the key matches any of the fields we are interested in, extend the provided `entry`.
/// Otherwise skip it.
///
/// # Arguments:
///
/// * `entry` - [`MaybeEntry`] to be extend
/// * `(key, value)` - key-value pair to add to `entry`
///
/// # Return:
///
/// * (updated) `entry`
fn update_with_key_value_pair(entry: MaybeEntry, (key, value): (String, String)) -> MaybeEntry {
    match key.as_str() {
        "Type" => MaybeEntry {
            entry_type: Some(value),
            ..entry
        },
        "Name" => MaybeEntry {
            name: Some(value),
            ..entry
        },
        "Exec" => MaybeEntry {
            cmd: Some(handle_exec_parameters(value)),
            ..entry
        },
        "Terminal" => MaybeEntry {
            terminal: match value.as_str() {
                "true" => Some(true),
                "false" => Some(false),
                _ => None,
            },
            ..entry
        },
        "Comment" => MaybeEntry {
            comment: Some(value),
            ..entry
        },
        "Icon" => MaybeEntry {
            icon: if value.trim().is_empty() {
                None
            } else {
                Some(value)
            },
            ..entry
        },
        _ => entry,
    }
}

/// Handle parameters in command
///
/// Parse `value` for parameters according to desktop entry spec.
/// Handle parameters found and return updated string.
/// As of now, simply remove them.
///
/// # Arguments:
///
/// * `value` - command string to parse
// @todo Replace with a more sophisticated version.
// We should support at least replacing name and path.
// We must also check if file forwarding in flatpak requires a special treatment
fn handle_exec_parameters(value: String) -> String {
    value
        .replace("%c", "") // name
        .replace("%f", "")
        .replace("%F", "")
        .replace("%i", "")
        .replace("%k", "") // path
        .replace("%u", "")
        .replace("%U", "")
        .trim()
        .to_string()
}

#[cfg(test)]
#[path = "test_desktop_entries.rs"]
mod tests;
