use super::*;

// tests include common valid and invalid line pattern and check they correct handling
// Every supported type of ini file is covered with a single test of a valid file.
// These also cover many types of lines.
// Therefore, line test mostly cover special and error cases.

#[test]
fn test_parse_section_title_simple() {
    assert_eq!(
        parse_line("[test title]"),
        Ok(Line::Title("test title".to_string()))
    );
}

#[test]
fn test_parse_section_title_with_at() {
    assert_eq!(
        parse_line("[test@title]"),
        Ok(Line::Title("test@title".to_string()))
    );
}

#[test]
fn test_parse_section_title_with_slash() {
    assert_eq!(
        parse_line("[test/title]"),
        Ok(Line::Title("test/title".to_string()))
    );
}

#[test]
fn test_parse_section_title_without_closing_bracket() {
    assert_eq!(
        parse_line("[test title"),
        Err(ParseError::InvalidLine("[test title".to_string()))
    );
}

#[test]
fn test_parse_section_title_with_early_closing_bracket() {
    assert_eq!(
        parse_line("[test ]title["),
        Err(ParseError::InvalidLine("[test ]title[".to_string()))
    );
}

#[test]
fn test_parse_entry_simple() {
    assert_eq!(
        parse_line("somekey=with value"),
        Ok(Line::Entry((
            "somekey".to_string(),
            "with value".to_string()
        )))
    );
}

#[test]
fn test_parse_entry_with_space() {
    assert_eq!(
        parse_line("somekey = with value"),
        Ok(Line::Entry((
            "somekey".to_string(),
            "with value".to_string()
        )))
    );
}

#[test]
fn test_parse_entry_space_in_key() {
    assert_eq!(
        parse_line("some key=with value"),
        Err(ParseError::InvalidLine("some key=with value".to_string()))
    );
}

/*
// This test is needed when we get more precise with parsing key-value pairs.
// This will happen with i8n
#[test]
fn test_parse_entry_key_with_at() {
    assert_eq!(
        parse_line("some@key=with value"),
        Err(ParseError::InvalidLine("some@key=with value".to_string()))
    );
}*/

#[test]
fn test_parse_entry_key_with_chinese() {
    assert_eq!(
        parse_line("some本=with value"),
        Err(ParseError::InvalidLine("some本=with value".to_string()))
    );
}

#[test]
fn test_parse_entry_key_with_language() {
    assert_eq!(
        parse_line("somekey[de]=with value"),
        Ok(Line::Entry((
            "somekey[de]".to_string(),
            "with value".to_string()
        )))
    );
}

#[test]
fn test_parse_entry_key_with_language_and_modifier() {
    assert_eq!(
        parse_line("Name[ca@valencia]=Brisa"),
        Ok(Line::Entry((
            "Name[ca@valencia]".to_string(),
            "Brisa".to_string()
        )))
    );
}

#[test]
fn test_parse_comment_with_title() {
    assert_eq!(
        parse_line("#[test title]"),
        Ok(Line::Comment("[test title]".to_string()))
    );
}

#[test]
fn test_parse_comment_with_entry() {
    assert_eq!(
        parse_line("# key = value"),
        Ok(Line::Comment(" key = value".to_string()))
    );
}

#[test]
fn test_parse_comment_with_partial_title() {
    assert_eq!(
        parse_line("# [test title"),
        Ok(Line::Comment(" [test title".to_string()))
    );
}

#[test]
fn test_add_duplicate_entry_line() {
    let test_file = IniFile {
        sections: vec![Section {
            title: "test section".to_string(),
            entries: HashMap::from([("mykey".to_string(), "test value".to_string())]),
        }],
    };

    assert_eq!(
        try_add_line_to_file(
            test_file,
            Line::Entry(("mykey".to_string(), "test value".to_string())),
        ),
        Err(ParseError::DuplicateEntry {
            section_title: "test section".to_string(),
            key: "mykey".to_string(),
            value: "test value".to_string()
        })
    );
}

#[test]
fn test_add_entry_without_section() {
    assert_eq!(
        try_add_line_to_file(
            IniFile::default(),
            Line::Entry(("mykey".to_string(), "test value".to_string())),
        ),
        Err(ParseError::MissingSection {
            key: "mykey".to_string(),
            value: "test value".to_string()
        })
    );
}

#[test]
fn test_parse_theme_index() {
    let entry = r"[Icon Theme]
Name=Hicolor
Comment=Fallback icon theme
Hidden=true
Directories=16x16/actions,192x192@2/apps,512x512@2/apps,scalable/stock/code

[16x16/actions]
Size=16
Context=Actions
Type=Threshold

[192x192@2/apps]
Size=192
Scale=2
Context=Applications
Type=Threshold

[512x512@2/apps]
MinSize=64
Size=512
Scale=2
MaxSize=512
Context=Applications
Type=Scalable

[scalable/stock/code]
MinSize=1
Size=128
MaxSize=256
Context=Stock
Type=Scalable";

    assert_eq!(
        IniFile::from_file(entry.as_bytes()),
        Ok(IniFile {
            sections: vec![
                (Section {
                    title: "Icon Theme".to_string(),
                    entries: HashMap::from([
                        ("Name".to_string(), "Hicolor".to_string()),
                        ("Comment".to_string(), "Fallback icon theme".to_string()),
                        ("Hidden".to_string(), "true".to_string()),
                        (
                            "Directories".to_string(),
                            "16x16/actions,192x192@2/apps,512x512@2/apps,scalable/stock/code"
                                .to_string()
                        ),
                    ])
                }),
                (Section {
                    title: "16x16/actions".to_string(),
                    entries: HashMap::from([
                        ("Size".to_string(), "16".to_string()),
                        ("Context".to_string(), "Actions".to_string()),
                        ("Type".to_string(), "Threshold".to_string()),
                    ])
                }),
                (Section {
                    title: "192x192@2/apps".to_string(),
                    entries: HashMap::from([
                        ("Size".to_string(), "192".to_string()),
                        ("Scale".to_string(), "2".to_string()),
                        ("Context".to_string(), "Applications".to_string()),
                        ("Type".to_string(), "Threshold".to_string()),
                    ])
                }),
                (Section {
                    title: "512x512@2/apps".to_string(),
                    entries: HashMap::from([
                        ("MinSize".to_string(), "64".to_string()),
                        ("Size".to_string(), "512".to_string()),
                        ("Scale".to_string(), "2".to_string()),
                        ("MaxSize".to_string(), "512".to_string()),
                        ("Context".to_string(), "Applications".to_string()),
                        ("Type".to_string(), "Scalable".to_string()),
                    ])
                }),
                (Section {
                    title: "scalable/stock/code".to_string(),
                    entries: HashMap::from([
                        ("MinSize".to_string(), "1".to_string()),
                        ("Size".to_string(), "128".to_string()),
                        ("MaxSize".to_string(), "256".to_string()),
                        ("Context".to_string(), "Stock".to_string()),
                        ("Type".to_string(), "Scalable".to_string()),
                    ])
                })
            ]
        })
    );
}

#[test]
fn test_parse_desktop_entry() {
    let entry = r"# Automatically generated file, do not edit!
[Desktop Entry]
Name=Helix
GenericName=Text Editor
GenericName[de]=Texteditor
GenericName[fr]=Éditeur de texte
GenericName[ru]=Текстовый редактор
Comment=Edit text files
Comment[ar]=مُحَرِّرُ مِلَفَّاتٍ نَصِّيَّة
Comment[de]=Textdateien bearbeiten
Comment[el]=Επεξεργασία αρχείων κειμένου
Comment[ja]=テキストファイルを編集します
Comment[kn]=ಪಠ್ಯ ಕಡತಗಳನ್ನು ಸಂಪಾದಿಸು
Comment[zh_CN]=编辑文本文件
TryExec=helix
Exec=helix %F
Terminal=true
Type=Application
Keywords=Text;editor;
Keywords[fr]=Texte;éditeur;
Icon=helix
Categories=Utility;TextEditor;
StartupNotify=false
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;";

    assert_eq!(
        IniFile::from_file(entry.as_bytes()),
        Ok(IniFile {
            sections: vec![Section {
                title: "Desktop Entry".to_string(),
                entries: HashMap::from([
                    ("Name".to_string(),"Helix".to_string()),
                    ("GenericName".to_string(),"Text Editor".to_string()),
                    ("GenericName[de]".to_string(),"Texteditor".to_string()),
                    ("GenericName[fr]".to_string(),"Éditeur de texte".to_string()),
                    ("GenericName[ru]".to_string(),"Текстовый редактор".to_string()),
                    ("Comment".to_string(),"Edit text files".to_string()),
                    ("Comment[ar]".to_string(),"مُحَرِّرُ مِلَفَّاتٍ نَصِّيَّة".to_string()),
                    ("Comment[de]".to_string(),"Textdateien bearbeiten".to_string()),
                    ("Comment[el]".to_string(),"Επεξεργασία αρχείων κειμένου".to_string()),
                    ("Comment[ja]".to_string(),"テキストファイルを編集します".to_string()),
                    ("Comment[kn]".to_string(),"ಪಠ್ಯ ಕಡತಗಳನ್ನು ಸಂಪಾದಿಸು".to_string()),
                    ("Comment[zh_CN]".to_string(),"编辑文本文件".to_string()),
                    ("TryExec".to_string(),"helix".to_string()),
                    ("Exec".to_string(),"helix %F".to_string()),
                    ("Terminal".to_string(),"true".to_string()),
                    ("Type".to_string(),"Application".to_string()),
                    ("Keywords".to_string(),"Text;editor;".to_string()),
                    ("Keywords[fr]".to_string(),"Texte;éditeur;".to_string()),
                    ("Icon".to_string(),"helix".to_string()),
                    ("Categories".to_string(),"Utility;TextEditor;".to_string()),
                    ("StartupNotify".to_string(),"false".to_string()),
                    ("MimeType".to_string(),"text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;".to_string())
                ])
            }]
        })
    );
}
