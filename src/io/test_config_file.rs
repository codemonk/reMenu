/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::{
    core::{
        config::{
            Config, DesktopEntriesConfig, FeedConfig, FeedOptions, GeneralConfig, Keybind,
            MenuLayout, Modifier, PathEntriesConfig,
        },
        feeds::{Fallthrough, FilterType, LoadingStrategy},
        Color, Length,
    },
    io::config_file::{
        parse_color, parse_length, parse_raw_config, CFG_DEFAULT, CFG_SIMPLE_DARK, CFG_SIMPLE_LIGHT,
    },
};

#[test]
fn test_parse_default_config() {
    let parsed_config = parse_raw_config(CFG_DEFAULT);
    let config = Config {
        feeds: vec![
            FeedConfig {
                options: FeedOptions::DesktopEntries(DesktopEntriesConfig {
                    filter: FilterType::StartsWith,
                    requires_icons: false,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '1',
                }),
            },
            FeedConfig {
                options: FeedOptions::PathEntries(PathEntriesConfig {
                    filter: FilterType::StartsWith,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '2',
                }),
            },
            FeedConfig {
                options: FeedOptions::DirectExecution,
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '3',
                }),
            },
        ],
        general: GeneralConfig {
            fallthrough: Fallthrough::Stepwise,
            loading: LoadingStrategy::Lazy,
            layout: MenuLayout::Dock,
            terminal: "alacritty -e".to_string(),
            height: Length::Pixel(32),
            font: "DejaVu Sans".to_string(),
            font_size: 11,
            icon_theme: "Papirus".to_string(),
            icon_size: Length::Pixel(24),
            text_color: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            bg_color: Color {
                red: 47,
                green: 61,
                blue: 68,
                alpha: 255,
            },
            text_color_hl: Color {
                red: 47,
                green: 61,
                blue: 68,
                alpha: 255,
            },
            bg_color_hl: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            input_width: Some(Length::Pixel(600)),
        },
    };
    assert_eq!(parsed_config.ok(), Some(config));
}

#[test]
fn test_parse_dark_config() {
    let parsed_config = parse_raw_config(CFG_SIMPLE_DARK);
    let config = Config {
        feeds: vec![
            FeedConfig {
                options: FeedOptions::DesktopEntries(DesktopEntriesConfig {
                    filter: FilterType::Fuzzy,
                    requires_icons: false,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '1',
                }),
            },
            FeedConfig {
                options: FeedOptions::PathEntries(PathEntriesConfig {
                    filter: FilterType::Fuzzy,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '2',
                }),
            },
            FeedConfig {
                options: FeedOptions::DirectExecution,
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '3',
                }),
            },
        ],
        general: GeneralConfig {
            fallthrough: Fallthrough::Stepwise,
            loading: LoadingStrategy::Lazy,
            layout: MenuLayout::Dock,
            terminal: "alacritty -e".to_string(),
            height: Length::Pixel(32),
            font: "DejaVu Sans".to_string(),
            font_size: 10,
            icon_theme: "hicolor".to_string(),
            icon_size: Length::Pixel(24),
            text_color: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            bg_color: Color {
                red: 0,
                green: 0,
                blue: 0,
                alpha: 255,
            },
            text_color_hl: Color {
                red: 0,
                green: 0,
                blue: 0,
                alpha: 255,
            },
            bg_color_hl: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            input_width: Some(Length::Pixel(600)),
        },
    };
    assert_eq!(parsed_config.ok(), Some(config));
}

#[test]
fn test_parse_light_config() {
    let parsed_config = parse_raw_config(CFG_SIMPLE_LIGHT);
    let config = Config {
        feeds: vec![
            FeedConfig {
                options: FeedOptions::DesktopEntries(DesktopEntriesConfig {
                    filter: FilterType::Fuzzy,
                    requires_icons: false,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '1',
                }),
            },
            FeedConfig {
                options: FeedOptions::PathEntries(PathEntriesConfig {
                    filter: FilterType::Fuzzy,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '2',
                }),
            },
            FeedConfig {
                options: FeedOptions::DirectExecution,
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '3',
                }),
            },
        ],
        general: GeneralConfig {
            fallthrough: Fallthrough::Stepwise,
            loading: LoadingStrategy::Lazy,
            layout: MenuLayout::Dock,
            terminal: "alacritty -e".to_string(),
            height: Length::Pixel(32),
            font: "DejaVu Sans".to_string(),
            font_size: 10,
            icon_theme: "hicolor".to_string(),
            icon_size: Length::Pixel(24),
            text_color: Color {
                red: 0,
                green: 0,
                blue: 0,
                alpha: 255,
            },
            bg_color: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            text_color_hl: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            bg_color_hl: Color {
                red: 0,
                green: 0,
                blue: 0,
                alpha: 255,
            },
            input_width: Some(Length::Pixel(600)),
        },
    };
    assert_eq!(parsed_config.ok(), Some(config));
}

#[test]
fn test_parse_valid_hex_color_all_caps() {
    let res = parse_color("#ABCDEFF0");
    assert_eq!(
        res.ok(),
        Some(Color {
            red: 171,
            green: 205,
            blue: 239,
            alpha: 240
        })
    );
}

#[test]
fn test_parse_valid_hex_color_all_small() {
    let res = parse_color("#abcdeff0");
    assert_eq!(
        res.ok(),
        Some(Color {
            red: 171,
            green: 205,
            blue: 239,
            alpha: 240
        })
    );
}

#[test]
fn test_parse_valid_hex_color_mixed() {
    let res = parse_color("#aBCdEfF0");
    assert_eq!(
        res.ok(),
        Some(Color {
            red: 171,
            green: 205,
            blue: 239,
            alpha: 240
        })
    );
}

// invalid hex color
#[test]
fn test_parse_invalid_hex_color_no_hex_digit() {
    let res = parse_color("#gBCDEFF0");
    assert!(res.is_err());
}

#[test]
fn test_parse_valid_rgb_color() {
    let res = parse_color("Rgb(171, 205, 239)");
    assert_eq!(
        res.ok(),
        Some(Color {
            red: 171,
            green: 205,
            blue: 239,
            alpha: 255
        })
    );
}

#[test]
fn test_parse_invalid_rgb_color_not_closed() {
    let res = parse_color("Rgb(171, 205, 239");
    assert!(res.is_err());
}

#[test]
fn test_parse_invalid_rgb_color_missing_value() {
    let res = parse_color("Rgb(171, 239)");
    assert!(res.is_err());
}

#[test]
fn test_parse_valid_rgba_color() {
    let res = parse_color("Rgba(171, 205, 239, 182)");
    assert_eq!(
        res.ok(),
        Some(Color {
            red: 171,
            green: 205,
            blue: 239,
            alpha: 182
        })
    );
}

#[test]
fn test_parse_invalid_rgba_color_not_closed() {
    let res = parse_color("Rgba(171, 205, 239, 182");
    assert!(res.is_err());
}

#[test]
fn test_parse_invalid_rgba_color_missing_value() {
    let res = parse_color("Rgba(171, 239, 182)");
    assert!(res.is_err());
}

#[test]
fn test_parse_length_implicit_px() {
    let res = parse_length("2054");
    assert_eq!(res.ok(), Some(Length::Pixel(2054)));
}

#[test]
fn test_parse_length_explicit_px() {
    let res = parse_length("102px");
    assert_eq!(res.ok(), Some(Length::Pixel(102)));
}

#[test]
fn test_parse_length_explicit_perc() {
    let res = parse_length("532%");
    assert_eq!(res.ok(), Some(Length::Percentage(532)));
}

#[test]
fn test_parse_length_explicit_pt() {
    let res = parse_length("12pt");
    assert_eq!(res.ok(), Some(Length::Point(12)));
}

#[test]
fn test_parse_length_explicit_pt_whitespace() {
    let res = parse_length("12 pt");
    assert!(res.is_err());
}
