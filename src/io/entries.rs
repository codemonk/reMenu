/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/**
Entry loader for desktop entry files

Loads desktop entry files according to XDG spec.
Currently only supports a very limited subset of all features.
Loads no translations and does no resolve placeholders.
**/
pub mod desktop_entries;

/**
Entry loader for binaries at a given path
**/
pub mod path_entries;
