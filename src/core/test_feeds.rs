/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use super::{EntryFeed, Feed, FilterOperator, FilterType};
use crate::core::{
    feeds::{ExactFilter, FuzzyFilter},
    Entry,
};

fn generate_entries() -> Vec<Entry> {
    vec![
        Entry {
            name: "App Executor".to_string(),
            cmd: "/usr/bin/exec".to_string(),
            comment: Some("Fear the executor".to_string()),
            terminal: Some(true),
            icon: None,
        },
        Entry {
            name: "App runner".to_string(),
            cmd: "/usr/bin/run".to_string(),
            comment: None,
            terminal: Some(false),
            icon: Some("".to_string()),
        },
        Entry {
            name: "Go go go".to_string(),
            cmd: "/usr/bin/go".to_string(),
            comment: Some("Weird app".to_string()),
            terminal: None,
            icon: Some("".to_string()),
        },
        Entry {
            name: "MyApp".to_string(),
            cmd: "/usr/bin/cmd1".to_string(),
            comment: None,
            terminal: None,
            icon: Some("".to_string()),
        },
    ]
}

fn generate_entry_feed(filter_type: FilterType) -> EntryFeed {
    EntryFeed::new(generate_entries(), filter_type, false)
}

#[test]
fn test_startswith_filter_no_match() {
    let mut filter = ExactFilter::new_prefix_filter(generate_entries());
    assert!(filter.filter("bla").is_empty());
}

#[test]
fn test_startswith_filter_exact_match() {
    let mut filter = ExactFilter::new_prefix_filter(generate_entries());
    let filtered = filter.filter("MyApp");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_startswith_filter_partial_match_single_match() {
    let mut filter = ExactFilter::new_prefix_filter(generate_entries());
    let filtered = filter.filter("My");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_startswith_filter_partial_match_multiple_match() {
    let mut filter = ExactFilter::new_prefix_filter(generate_entries());
    let filtered = filter.filter("App");
    assert_eq!(filtered.len(), 2);

    assert_eq!(filtered[0].entry.cmd, "/usr/bin/exec");
    assert_eq!(filtered[0].entry.name, "App Executor");
    assert_eq!(
        filtered[0].entry.comment,
        Some("Fear the executor".to_string())
    );
    assert_eq!(filtered[0].entry.terminal, Some(true));
    assert_eq!(filtered[1].entry.cmd, "/usr/bin/run");
    assert_eq!(filtered[1].entry.name, "App runner");
    assert_eq!(filtered[1].entry.comment, None);
    assert_eq!(filtered[1].entry.terminal, Some(false));
}

#[test]
fn test_contains_filter_no_match() {
    let mut filter = ExactFilter::new_infix_filter(generate_entries());
    assert!(filter.filter("bla").is_empty());
}

#[test]
fn test_contains_filter_exact_match() {
    let mut filter = ExactFilter::new_infix_filter(generate_entries());
    let filtered = filter.filter("MyApp");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_contains_filter_partial_match_single_match() {
    let mut filter = ExactFilter::new_infix_filter(generate_entries());
    let filtered = filter.filter("My");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_contains_filter_partial_match_multiple_match() {
    let mut filter = ExactFilter::new_infix_filter(generate_entries());
    let filtered = filter.filter("App");
    assert_eq!(filtered.len(), 3);

    assert_eq!(filtered[0].entry.cmd, "/usr/bin/exec");
    assert_eq!(filtered[0].entry.name, "App Executor");
    assert_eq!(
        filtered[0].entry.comment,
        Some("Fear the executor".to_string())
    );
    assert_eq!(filtered[0].entry.terminal, Some(true));
    assert_eq!(filtered[1].entry.cmd, "/usr/bin/run");
    assert_eq!(filtered[1].entry.name, "App runner");
    assert_eq!(filtered[1].entry.comment, None);
    assert_eq!(filtered[1].entry.terminal, Some(false));
}

#[test]
fn test_fuzzy_filter_no_match() {
    let mut filter = FuzzyFilter::new(&generate_entries());
    assert!(filter.filter("bla").is_empty());
}

#[test]
fn test_fuzzy_filter_exact_match() {
    let mut filter = FuzzyFilter::new(&generate_entries());
    let filtered = filter.filter("MyApp");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_fuzzy_filter_partial_match_single_match() {
    let mut filter = FuzzyFilter::new(&generate_entries());
    let filtered = filter.filter("My");
    assert_eq!(filtered.len(), 1);
    assert!(filtered
        .first()
        .is_some_and(|x| x.entry.cmd == "/usr/bin/cmd1"
            && x.entry.name == "MyApp"
            && x.entry.comment.is_none()
            && x.entry.terminal.is_none()));
}

#[test]
fn test_fuzzy_filter_partial_match_multiple_match() {
    let mut filter = FuzzyFilter::new(&generate_entries());
    let filtered = filter.filter("App");
    assert_eq!(filtered.len(), 3);
    // we do not check for content since nucleo does not guarantee order of results
}

// we do not test fuzzy matches as this depends on the fuzzy algorithm implemented in nucleo.
// This is assumed to be correct

#[test]
fn create_new_filled_feed() {
    let feed = generate_entry_feed(FilterType::StartsWith);
    // applying no filter should result in all entries
    assert!(feed.get_filtered().len() == feed.filtered_entries.len());
    assert!(feed.get_selected().is_some_and(|x| x.cmd == "/usr/bin/exec"
        && x.name == "App Executor"
        && x.comment.is_some_and(|c| c == "Fear the executor")
        && x.terminal.is_some_and(|t| t)));
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}

#[test]
fn move_selection_unfiltered() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    assert!(feed.filtered_entries.len() >= 3);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 2));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}

#[test]
fn move_selection_unfiltered_beyond_first() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    assert!(feed.filtered_entries.len() >= 3);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}

#[test]
fn move_selection_unfiltered_beyond_last() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    assert!(feed.filtered_entries.len() == 4);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 2));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 3));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 3));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 2));
}

#[test]
fn move_selection_filtered() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    feed.set_filter_text("App");
    assert!(feed.filtered_entries.len() == 2);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}

#[test]
fn move_selection_filtered_beyond_first() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    feed.set_filter_text("App");
    assert!(!feed.filtered_entries.is_empty());
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
}

#[test]
fn move_selection_filtered_beyond_last() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    feed.set_filter_text("App");
    assert!(feed.filtered_entries.len() == 2);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.move_selection_left();
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}

#[test]
fn reset_selection_changed_filter() {
    let mut feed = generate_entry_feed(FilterType::StartsWith);
    assert!(feed.filtered_entries.len() > 2);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
    feed.move_selection_right();
    assert!(feed.get_selection_index().is_some_and(|x| x == 1));
    feed.set_filter_text("App");
    assert!(feed.filtered_entries.len() == 2);
    assert!(feed.get_selection_index().is_some_and(|x| x == 0));
}
