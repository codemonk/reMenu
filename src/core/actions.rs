// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::core::{
    feeds::{Feed, LayeredFeed},
    StatefulEntry,
};
use anyhow::{Error, Result};
use log::{debug, error, warn};
use serde::Deserialize;
use std::{fmt::Debug, process::Command};

#[derive(Debug, Default, Clone, Deserialize)]
pub struct Keybind {
    pub shift: bool,
    pub ctrl: bool,
    pub alt: bool,
    pub altgr: bool,
    pub meta: bool,
    pub key: char,
}

/**
Available actions in response to user input

This enum isolates user input from resulting actions.
Input events are handled by converting them into variants of this enum.
The resulting Action is then processed separately.
**/
#[derive(PartialEq, Eq)]
pub enum Action<O: UiOutputHandler> {
    /// append a character to filter text
    AddChar(char),

    /// choose and execute the currently selected item
    ChooseSelection,

    /// exit app without executing anything
    Exit,

    /// move selection one item to the left
    MoveSelLeft,

    /// move selection one item to the right
    MoveSelRight,

    /// input does not match any Action
    None,

    /// remove last character from filter text
    RemoveChar,

    /// set the currently active layer
    SetLayer(usize),

    /// set the output handler, in case it changed
    SetOutputHandler(O),

    /// Redraw on system request
    Redraw,
}

// custom implementation to avoid requiring impl for O
impl<O: UiOutputHandler> Debug for Action<O> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Action::AddChar(c) => write!(f, "Action::AddChar({})", c),
            Action::ChooseSelection => write!(f, "Action::ChooseSelection"),
            Action::Exit => write!(f, "Action::Exit"),
            Action::MoveSelLeft => write!(f, "Action::MoveSelLeft"),
            Action::MoveSelRight => write!(f, "Action::MoveSelRight"),
            Action::None => write!(f, "Action::None"),
            Action::RemoveChar => write!(f, "Action::RemoveChar"),
            Action::SetLayer(l) => write!(f, "Action::SetLayer({})", l),
            Action::SetOutputHandler(_) => write!(f, "Action::SetOutputHandler"),
            Action::Redraw => write!(f, "Action::Redraw"),
        }
    }
}

/// implemented by the output part of a UI to visualize changes to the current state
/// caused by the logic.
pub trait UiOutputHandler {
    /// update the input text that is used for filtering
    ///
    /// Update the text that the user provided and that is used to filter results.
    /// Might return an error if updating went wrong.
    ///
    /// # Arguments
    /// * `txt` - the new input text
    fn update_input(&mut self, txt: &str) -> Result<()>;

    /// update the list of results to show to the user
    ///
    /// # Arguments
    /// * `results` - the new list of (filtered) results
    fn update_results(&mut self, results: &[StatefulEntry]) -> Result<()>;

    /// decrease the position of the selection within the list of results
    fn decrease_selection_position(&mut self) -> Result<()>;

    /// increase the position of the selection within the list of results
    fn increase_selection_position(&mut self) -> Result<()>;

    /// refresh (e.g. redraw) the UI after some changes were made
    ///
    /// Allows a series of updates to be done before an potentionally expensive
    /// refresh occurs.
    fn refresh(&mut self) -> Result<()>;
}

/// The core logic structure for reMenu
/// Holds the logic state (entries, filter and input text) and the UI.
pub struct ActionHandler<O: UiOutputHandler> {
    /// user interface output
    ui_output: Option<O>,

    /// filter for entries (holding all available entries)
    feeds: LayeredFeed,

    /// current filter text
    filter_text: String,

    /// executor for executing commands
    exec: Executor,
}

impl<O: UiOutputHandler> ActionHandler<O> {
    /// create a new action handler
    ///
    /// # Arguments
    /// * `ui` - the UI to use
    /// * `entries` - list of all available entries
    /// * `terminal` - terminal emulator command for terminal applications
    pub fn new(feeds: LayeredFeed, terminal: &str) -> ActionHandler<O> {
        ActionHandler {
            ui_output: None,
            feeds,
            filter_text: "".to_string(),
            exec: Executor::new(terminal),
        }
    }

    /// evaluate a single action
    ///
    /// This is the core decision logic for the logic loop (or eval loop).
    ///
    /// # Arguments
    ///
    /// * action - the action to process
    ///
    /// # Return
    /// * `Ok(true)` - if the program shall wait for other events
    /// * `Ok(false)` - if the program shall exit
    /// * `Err(e)` - if an error occured
    pub fn evaluate(&mut self, action: Action<O>) -> Result<bool> {
        let mut refresh = false;
        let mut cont = true;

        match action {
            Action::AddChar(ch) => {
                debug!("Add input character {}", ch);
                self.filter_text.push(ch);
                self.feeds.set_filter_text(&self.filter_text);

                if let Some(out) = &mut self.ui_output {
                    out.update_input(&self.filter_text).map_err(|err| {
                        error!("Failed to set input text to '{}'", self.filter_text);
                        err
                    })?;
                    out.update_results(&self.feeds.get_filtered().to_vec())
                        .map_err(|err| {
                            error!("Failed to set entries: {}", err);
                            err
                        })?;
                }
                refresh = true;
            }
            Action::ChooseSelection => {
                debug!("Choosing selection");
                if let Some(entry) = self.feeds.get_selected() {
                    self.exec.exec(entry.cmd, entry.terminal.unwrap_or(true))?;
                };
                cont = false;
            }
            Action::Exit => {
                debug!("Exiting by user request");
                cont = false;
            }
            Action::None => {}
            Action::MoveSelLeft => {
                debug!("Move selection left");
                self.feeds.move_selection_left();
                if let Some(out) = &mut self.ui_output {
                    out.decrease_selection_position()?;
                }
                refresh = true;
            }
            Action::MoveSelRight => {
                debug!("Move selection right");
                self.feeds.move_selection_right();
                if let Some(out) = &mut self.ui_output {
                    out.increase_selection_position().map_err(|err| {
                        error!("Failed to increase selection position");
                        err
                    })?;
                }
                refresh = true;
            }
            Action::RemoveChar => {
                debug!("Remove a character");
                self.filter_text.pop();
                self.feeds.set_filter_text(&self.filter_text);

                if let Some(out) = &mut self.ui_output {
                    out.update_input(&self.filter_text).map_err(|err| {
                        error!("Failed to set input text to '{}'", self.filter_text);
                        err
                    })?;
                    out.update_results(&self.feeds.get_filtered().to_vec())
                        .map_err(|err| {
                            error!("Failed to set entries");
                            err
                        })?;
                }
                refresh = true;
            }
            Action::SetLayer(layer) => {
                debug!("Switching to layer '{}'", layer);
                self.feeds.set_layer(layer).map_err(|err| {
                    error!("Failed to set layer to '{}'", layer);
                    err
                })?;
                if let Some(out) = &mut self.ui_output {
                    out.update_results(&self.feeds.get_filtered().to_vec())
                        .map_err(|err| {
                            error!("Failed to set entries");
                            err
                        })?;
                }
                refresh = true;
            }
            Action::Redraw => refresh = true,
            Action::SetOutputHandler(oh) => {
                debug!("Setting output handler.");
                self.ui_output = Some(oh);
                if let Some(out) = &mut self.ui_output {
                    out.update_results(self.feeds.get_filtered())?;
                }
                refresh = true;
            }
        }

        if let Some(out) = &mut self.ui_output {
            if refresh {
                debug!("Refreshing UI.");
                out.refresh().map_err(|err| {
                    error!("Failed to refresh UI.");
                    err.context("Failed to refresh UI")
                })?;
                Ok(cont)
            } else {
                Ok(cont)
            }
        } else {
            if refresh {
                warn!("UI refresh issued while no UI is available.");
            }
            Ok(cont)
        }
    }

    /// Force refresh an update of the UI
    ///
    /// A force refresh is used to update the UI without an event.
    /// This is especially usedful for the initial update prior to any event.
    pub fn refresh(&mut self) -> Result<()> {
        if let Some(out) = &mut self.ui_output {
            debug!("Refreshing UI.");
            out.update_results(self.feeds.get_filtered())?;
            out.refresh()
        } else {
            warn!("UI refresh issued while no UI is available.");
            Ok(())
        }
    }
}

/// Execute an app either as a GUI app or a terminal app.
// An executor holds the information needed to execute an app in a terminal emulator.
struct Executor {
    /// command to open a terminal emulator
    term_cmd: String,

    /// parameters to pass to the terminal emulator
    term_params: Vec<String>,
}

impl Executor {
    /// create a new executor
    ///
    /// # Arguments
    ///
    /// * `term` - string with the command incl. parameters to invoke the terminal emulator
    fn new(term: &str) -> Self {
        let mut elems = term.split(' ');
        let cmd = elems.next().unwrap_or(term);
        let params: Vec<String> = elems.map(|s| s.to_string()).collect();

        Executor {
            term_cmd: cmd.to_string(),
            term_params: params,
        }
    }

    /// execute a command in a terminal emulator
    ///
    /// # Arguments
    ///
    /// * `cmd` - command that opens the app
    /// * `in_term` - true if the app is to be opened in a terminal, false otherwise
    fn exec(&self, cmd: String, in_term: bool) -> Result<()> {
        let mut cmd_elements = cmd.split(' ');

        match in_term {
            true => {
                let mut command = Command::new(&self.term_cmd);
                let mut params = self.term_params.clone();
                params.extend(cmd_elements.map(|s| s.to_string()));
                command.args(params);
                debug!(
                    "Executing command '{}' in terminal via '{:?}'.",
                    cmd, command
                );
                command.spawn().map_err(|err| {
                    error!("Failed to execute command with error: {}.", err);
                    err
                })?;
            }
            false => {
                debug!("Executing command '{}'.", cmd);
                let cmd = cmd_elements
                    .next()
                    .ok_or(Error::msg("Failed to get command"))
                    .map_err(|err| {
                        error!("Failed to split command with error: {}.", err);
                        err
                    })?;
                Command::new(cmd)
                    .args(cmd_elements.collect::<Vec<_>>())
                    .spawn()
                    .map_err(|err| {
                        error!("Failed to execute command with error: {}.", err);
                        err
                    })?;
            }
        };

        Ok(())
    }
}

#[cfg(test)]
#[path = "test_actions.rs"]
mod tests;
