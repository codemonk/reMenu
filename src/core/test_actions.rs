/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
use super::{Action, ActionHandler, UiOutputHandler};
use crate::core::{
    feeds::{EntryFeed, FilterType, LayeredFeed},
    Entry, StatefulEntry,
};
use anyhow::Result;

// These tests test the correct functionality of ActionHandler only.
// Basically this means, that input and results are set and the sequence of calls is in expected order.
// The correct functionality of integrated modules is tested elsewhere.

/// enum representing UiActions calls
/// used to test sequence of calls
#[derive(Debug, PartialEq, Eq)]
enum UiCalls {
    DecreaseSelPos,
    IncreaseSelPos,
    Refresh,
    UpdateInput,
    UpdateResults,
}

#[derive(Debug, Default)]
struct TestUi {
    /// the input text set by action handler
    input: Option<String>,

    /// the list of entries set by action handler
    results: Option<Vec<StatefulEntry>>,

    /// number of refresh calls
    call_sequence: Vec<UiCalls>,
}

impl UiOutputHandler for TestUi {
    fn update_input(&mut self, txt: &str) -> Result<()> {
        // @todo support failure to test reaction to that
        self.input = Some(txt.to_string());
        self.call_sequence.push(UiCalls::UpdateInput);
        Ok(())
    }

    fn update_results(&mut self, results: &[StatefulEntry]) -> Result<()> {
        // @todo support failure to test reaction to that
        self.results = Some(results.to_vec());
        self.call_sequence.push(UiCalls::UpdateResults);
        Ok(())
    }

    fn decrease_selection_position(&mut self) -> Result<()> {
        // @todo support failure to test reaction to that
        self.call_sequence.push(UiCalls::DecreaseSelPos);
        Ok(())
    }

    fn increase_selection_position(&mut self) -> Result<()> {
        // @todo support failure to test reaction to that
        self.call_sequence.push(UiCalls::IncreaseSelPos);
        Ok(())
    }

    fn refresh(&mut self) -> Result<()> {
        // @todo support failure to test reaction to that
        self.call_sequence.push(UiCalls::Refresh);
        Ok(())
    }
}

fn test_layers() -> LayeredFeed {
    LayeredFeed::new(
        vec![Box::new(EntryFeed::new(
            vec![
                Entry {
                    name: "KiCad PCB Editor (Standalone)".to_string(),
                    cmd: "pcbnew %f".to_string(),
                    comment: Some("Standalone circuit board editor for KiCad boards".to_string()),
                    terminal: Some(false),
                    icon: None,
                },
                Entry {
                    name: "Helix".to_string(),
                    cmd: "helix %F".to_string(),
                    comment: Some("Edit text files".to_string()),
                    terminal: Some(true),
                    icon: None,
                },
                Entry {
                    name: "Yakuake".to_string(),
                    cmd: "yakuake".to_string(),
                    comment: Some(
                        "A drop-down terminal emulator based on KDE Konsole technology."
                            .to_string(),
                    ),
                    terminal: Some(false),
                    icon: None,
                },
                Entry {
                    name: "Vim".to_string(),
                    cmd: "vim %F".to_string(),
                    comment: Some("Edit text files".to_string()),
                    terminal: Some(true),
                    icon: None,
                },
            ],
            FilterType::StartsWith,
            false,
        ))],
        crate::core::feeds::Fallthrough::None,
    )
}

#[test]
fn test_set_output_handler() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler.ui_output.as_ref().unwrap().input.is_none());
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![UiCalls::UpdateResults, UiCalls::Refresh]
    );
}

#[test]
fn test_none() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let eval_result = action_handler.evaluate(Action::None);
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler.ui_output.as_ref().unwrap().input.is_none());
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![UiCalls::UpdateResults, UiCalls::Refresh]
    );
}

#[test]
fn test_exit() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let eval_result = action_handler.evaluate(Action::Exit);
    assert!(eval_result.is_ok_and(|cont| !cont));
    assert!(action_handler.ui_output.as_ref().unwrap().input.is_none());
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![UiCalls::UpdateResults, UiCalls::Refresh]
    );
}

#[test]
fn test_addchar() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let eval_result = action_handler.evaluate(Action::AddChar('a'));
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .input
        .as_ref()
        .is_some_and(|txt| txt == "a"));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .results
        .as_ref()
        .is_some_and(|res| res.is_empty()));
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh
        ]
    );
}

#[test]
fn test_removechar() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let _eval_result = action_handler.evaluate(Action::AddChar('a'));
    let _eval_result = action_handler.evaluate(Action::AddChar('l'));
    let _eval_result = action_handler.evaluate(Action::AddChar('b'));
    let eval_result = action_handler.evaluate(Action::RemoveChar);
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .input
        .as_ref()
        .is_some_and(|txt| txt == "al"));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .results
        .as_ref()
        .is_some_and(|res| res.is_empty()));
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh
        ]
    );
}

#[test]
fn test_chooseselection() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let _eval_result = action_handler.evaluate(Action::AddChar('a'));
    let _eval_result = action_handler.evaluate(Action::AddChar('l'));
    let _eval_result = action_handler.evaluate(Action::AddChar('b'));
    let eval_result = action_handler.evaluate(Action::ChooseSelection);
    assert!(eval_result.is_ok_and(|cont| !cont));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .input
        .as_ref()
        .is_some_and(|txt| txt == "alb"));
    assert!(action_handler
        .ui_output
        .as_ref()
        .unwrap()
        .results
        .as_ref()
        .is_some_and(|res| res.is_empty()));
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::UpdateInput,
            UiCalls::UpdateResults,
            UiCalls::Refresh
        ]
    );
}

#[test]
fn test_moveselleft() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");

    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let _eval_result = action_handler.evaluate(Action::MoveSelRight);
    let eval_result = action_handler.evaluate(Action::MoveSelLeft);
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler.ui_output.as_ref().unwrap().input.is_none());
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::IncreaseSelPos,
            UiCalls::Refresh,
            UiCalls::DecreaseSelPos,
            UiCalls::Refresh
        ]
    );
}

#[test]
fn test_moveselright() {
    let test_ui = TestUi::default();
    let mut action_handler = ActionHandler::new(test_layers(), "alacritty -e");
    let eval_result = action_handler.evaluate(Action::SetOutputHandler(test_ui));
    assert!(eval_result.is_ok_and(|cont| cont));

    let eval_result = action_handler.evaluate(Action::MoveSelRight);
    assert!(eval_result.is_ok_and(|cont| cont));
    assert!(action_handler.ui_output.as_ref().unwrap().input.is_none());
    assert_eq!(
        action_handler.ui_output.as_ref().unwrap().call_sequence,
        vec![
            UiCalls::UpdateResults,
            UiCalls::Refresh,
            UiCalls::IncreaseSelPos,
            UiCalls::Refresh
        ]
    );
}
