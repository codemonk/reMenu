/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::core::{CoreError, Entry, StatefulEntry};
use nucleo::{
    pattern::{CaseMatching, Normalization},
    Nucleo,
};
use serde::Deserialize;
use std::{cell::OnceCell, fmt::Display, sync::Arc};

/// Fallthrough strategy for feeds
/// This strategy decides what should happen if a feed returns no results
#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub enum Fallthrough {
    /// No fallthrough -> an empty list of results is shown
    None,

    /// fall through to the next feed and display its results
    Stepwise,

    /// fall through to the last feed and display its results
    Last,
}

/// Loading strategy for feeds
/// This strategy decides when a feed shall be loaded
#[derive(Debug, Clone, Default, Deserialize, PartialEq, Eq)]
pub enum LoadingStrategy {
    /// eager loading, i.e. load immediately
    Eager,

    /// lazy loading, i.e. load when needed for the first time
    #[default]
    Lazy,
}

/// A feed is a data structure that provides a list of entries based on a given filter text.
/// Most commonly, a feed contains a list of entries and returns the filtered entries.
/// This is not always the case.
pub trait Feed {
    /// set the text of the filter
    ///
    /// This will modify the filter text and the list of filtered entries.
    /// It will also reset the selection index.
    ///
    /// # Arguments
    /// * `txt` - filter text
    fn set_filter_text(&mut self, txt: &str);

    /// Get the list of currently filtered entries (i.e. all entries the filter applies to)
    fn get_filtered(&self) -> &Vec<StatefulEntry>;

    /// Get the currently selected entry (if any).
    /// If the list of filtered entries is not empty, one entry must be selected.
    fn get_selected(&self) -> Option<Entry>;

    /// Move selection one entry to the left
    fn move_selection_left(&mut self);

    /// Move selection one entry to the right
    fn move_selection_right(&mut self);

    /// Get the index of the selection in the list of filtered entries (if any)
    fn get_selection_index(&self) -> Option<usize>;
}

/// A feed of feeds with different feed switching strategies.
/// Each feed in this feed is considered to be a 'layer'.
/// The layer index is given by the position in the list of feeds.
/// Feeds can be selected by the application (and by this the user).
/// Fallthrough strategies might change the currently active layer.
/// This is the root feed used by reMenu.
pub struct LayeredFeed {
    /// list of available feeds
    feeds: Vec<Box<dyn Feed>>,

    /// fallthrough strategy if a feed does not return any filtered entries
    fallthrough: Fallthrough,

    /// the currently selected layer
    selected_level: usize,

    /// the currently active layer activated by a fallthrough strategy
    current_level: usize,

    /// the filter text to apply the the active feed
    filter_text: String,
}

impl Feed for LayeredFeed {
    fn set_filter_text(&mut self, txt: &str) {
        self.filter_text = txt.to_string();
        self.apply_filter_text();
    }

    fn get_filtered(&self) -> &Vec<StatefulEntry> {
        self.feeds[self.current_level].get_filtered()
    }

    fn get_selected(&self) -> Option<Entry> {
        self.feeds[self.current_level].get_selected()
    }

    fn move_selection_left(&mut self) {
        self.feeds[self.current_level].move_selection_left();
    }

    fn move_selection_right(&mut self) {
        self.feeds[self.current_level].move_selection_right();
    }

    fn get_selection_index(&self) -> Option<usize> {
        self.feeds[self.current_level].get_selection_index()
    }
}

impl LayeredFeed {
    /// create a new layered feed
    ///
    /// Creates a new layered feed where each provided feed is a layer.
    /// The layers are created in the order defined by `feeds`.
    ///
    /// # Arguments
    ///
    /// * `feeds` - vector of feeds used as layers
    /// * `fallthrough` - fallthrough strategy if a layer does not return results
    pub fn new(feeds: Vec<Box<dyn Feed>>, fallthrough: Fallthrough) -> Self {
        LayeredFeed {
            feeds,
            fallthrough,
            selected_level: 0,
            current_level: 0,
            filter_text: "".to_string(),
        }
    }

    /// set the current layer
    ///
    /// Make `layer` the new current layer.
    /// The current layer is the initial layer use before applying a fallthrough strategy.
    ///
    /// # Arguments
    ///
    /// * `layer` - index of the desired new current layer
    pub fn set_layer(&mut self, layer: usize) -> Result<(), CoreError> {
        if layer < self.feeds.len() {
            self.selected_level = layer;
            self.apply_filter_text();
            Ok(())
        } else {
            Err(CoreError::OutOfBounds)
        }
    }

    /// apply current filter text to layers
    ///
    /// Apply the currently set filter text to the active layer.
    /// If the current layer does not return any entries, apply filter text
    /// to other layers, following the selected fallthrough strategy.
    fn apply_filter_text(&mut self) {
        self.current_level = self.selected_level;
        self.feeds[self.current_level].set_filter_text(&self.filter_text);

        match self.fallthrough {
            Fallthrough::None => {}
            Fallthrough::Stepwise => {
                while self.current_level < self.feeds.len() - 1
                    && self.feeds[self.current_level].get_filtered().is_empty()
                {
                    self.current_level += 1;
                    self.feeds[self.current_level].set_filter_text(&self.filter_text);
                }
            }
            Fallthrough::Last => {
                if self.current_level < self.feeds.len() - 1
                    && self.feeds[self.current_level].get_filtered().is_empty()
                {
                    self.current_level = self.feeds.len() - 1;
                    self.feeds[self.current_level].set_filter_text(&self.filter_text);
                }
            }
        }
    }
}

/// a filter for a list of entries
pub struct EntryFeed {
    /// resulting entries after filtering
    filtered_entries: Vec<StatefulEntry>,

    /// text used for filtering
    text: String,

    /// filter operation (see [`FilterOperator`])
    operation: Box<dyn FilterOperator>,
    // symbol
    // icon
}

impl Feed for EntryFeed {
    fn set_filter_text(&mut self, txt: &str) {
        self.text = txt.to_string();

        // update filtered entries
        self.filtered_entries = self.operation.filter(&self.text);

        // update selection
        if !self.filtered_entries.is_empty() {
            self.filtered_entries[0].selected = true;
        }
    }

    fn get_filtered(&self) -> &Vec<StatefulEntry> {
        &self.filtered_entries
    }

    fn get_selected(&self) -> Option<Entry> {
        self.get_selection_index()
            .map(|sel_idx| self.filtered_entries[sel_idx].entry.clone())
    }

    fn move_selection_left(&mut self) {
        if let Some(sel_idx) = self.get_selection_index() {
            if sel_idx > 0 {
                self.filtered_entries[sel_idx].selected = false;
                self.filtered_entries[sel_idx - 1].selected = true;
            }
        }
    }

    fn move_selection_right(&mut self) {
        if let Some(sel_idx) = self.get_selection_index() {
            if sel_idx < self.filtered_entries.len() - 1 {
                self.filtered_entries[sel_idx].selected = false;
                self.filtered_entries[sel_idx + 1].selected = true;
            }
        }
    }

    fn get_selection_index(&self) -> Option<usize> {
        (0..self.filtered_entries.len()).find(|&idx| self.filtered_entries[idx].selected)
    }
}

impl EntryFeed {
    /// create a new filter for entries
    ///
    /// # Arguments:
    ///
    /// * `entries` - the list of entries to be filtered
    /// * `with_icons_only` - allow entries with icons only
    pub fn new(entries: Vec<Entry>, filter: FilterType, with_icons: bool) -> EntryFeed {
        let mut filter = EntryFeed {
            filtered_entries: entries
                .iter()
                .filter(|entry| !with_icons || entry.icon.is_some())
                .map(|entry| StatefulEntry {
                    entry: entry.clone(),
                    selected: false,
                })
                .collect(),
            text: "".to_string(),
            operation: match filter {
                FilterType::Fuzzy => Box::new(FuzzyFilter::new(&entries)),
                FilterType::Contains => Box::new(ExactFilter::new_infix_filter(entries)),
                FilterType::StartsWith => Box::new(ExactFilter::new_prefix_filter(entries)),
            },
        };

        // hack to set selection
        filter.set_filter_text("");

        filter
    }
}

/// a feed for direct exection
/// This feed simply takes the filter text as the only entry.
pub struct ExecutionFeed {
    /// resulting filtered entries
    filtered_entries: Vec<StatefulEntry>,

    /// text used for filtering
    text: String,
    // symbol
    // icon
}

impl Feed for ExecutionFeed {
    fn set_filter_text(&mut self, txt: &str) {
        self.text = txt.to_string();
        self.filtered_entries.clear();

        if !txt.is_empty() {
            self.filtered_entries.push(StatefulEntry {
                entry: Entry {
                    name: txt.to_string(),
                    cmd: txt.to_string(),
                    comment: None,
                    terminal: None,
                    icon: None,
                },
                selected: true,
            })
        }
    }

    fn get_filtered(&self) -> &Vec<StatefulEntry> {
        &self.filtered_entries
    }

    fn get_selected(&self) -> Option<Entry> {
        self.filtered_entries.first().map(|e| e.entry.clone())
    }

    fn move_selection_left(&mut self) {}

    fn move_selection_right(&mut self) {}

    fn get_selection_index(&self) -> Option<usize> {
        Some(0)
    }
}

impl ExecutionFeed {
    /// create a new execution feed
    pub fn new() -> Self {
        ExecutionFeed {
            filtered_entries: Vec::new(),
            text: "".to_string(),
        }
    }
}

/// This feed encapsules another feed
/// The encapsuled feed is created on first access.
pub struct LazyFeed<F: Feed, P: Fn() -> F> {
    /// feed that is wrapped by the lazy loading feed
    feed: OnceCell<F>,

    /// function that produces a feed when needed
    producer: P,
}

impl<F: Feed, P: Fn() -> F + Clone> Feed for LazyFeed<F, P> {
    fn set_filter_text(&mut self, txt: &str) {
        match self.feed.get_mut() {
            Some(f) => f.set_filter_text(txt),
            None => {
                let _ = self.feed.set((self.producer)());
                self.feed
                    .get_mut()
                    .expect("OnceCell is empty, even though it was set before")
                    .set_filter_text(txt)
            }
        }
    }

    fn get_filtered(&self) -> &Vec<StatefulEntry> {
        self.feed.get_or_init(|| (self.producer)()).get_filtered()
    }

    fn get_selected(&self) -> Option<Entry> {
        self.feed.get_or_init(|| (self.producer)()).get_selected()
    }

    fn move_selection_left(&mut self) {
        match self.feed.get_mut() {
            Some(f) => f.move_selection_left(),
            None => {
                let _ = self.feed.set((self.producer)());
                self.feed
                    .get_mut()
                    .expect("OnceCell is empty, even though it was set before")
                    .move_selection_left()
            }
        }
    }

    fn move_selection_right(&mut self) {
        match self.feed.get_mut() {
            Some(f) => f.move_selection_right(),
            None => {
                let _ = self.feed.set((self.producer)());
                self.feed
                    .get_mut()
                    .expect("OnceCell is empty, even though it was set before")
                    .move_selection_right()
            }
        }
    }

    fn get_selection_index(&self) -> Option<usize> {
        self.feed
            .get_or_init(|| (self.producer)())
            .get_selection_index()
    }
}

impl<F: Feed, P: Fn() -> F> LazyFeed<F, P> {
    /// create a new lazy loading feed
    ///
    /// # Arguments:
    ///
    /// * `producer` - function that produces a feed when called
    pub fn new(producer: P) -> Self {
        LazyFeed {
            feed: OnceCell::new(),
            producer,
        }
    }
}

/// the filter type to use for filtering entries
#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Debug, Default, Deserialize)]
pub enum FilterType {
    /// filter by start sequence
    #[default]
    StartsWith,

    /// filter by contained sequence
    Contains,

    /// use a fuzzy filter
    Fuzzy,
}

impl Display for FilterType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            FilterType::StartsWith => f.write_str("Starts with"),
            FilterType::Contains => f.write_str("Contains"),
            FilterType::Fuzzy => f.write_str("Fuzzy"),
        }
    }
}

/// implements a filter operator on a list of entries
/// The implementing structure is expected to contain the entries to filter.
trait FilterOperator {
    /// Filter entries based on the given text
    ///
    /// Apply the implemented filter to the contained entries.
    /// Return a list of entries matching the filter.
    ///
    /// # Arguments
    ///
    /// * `txt` - text to use for filtering
    fn filter(&mut self, txt: &str) -> Vec<StatefulEntry>;
}

/// Exact filter, i.e. the filter does must match (parts of) the entry title exactly
struct ExactFilter {
    /// entries to filter
    entries: Vec<Entry>,

    /// predicate to use for filtering
    /// first parameter is entry name, second is filter text
    /// Returns true if the string match according to the predicate and false otherwise.
    predicate: fn(&str, &str) -> bool,
}

impl FilterOperator for ExactFilter {
    fn filter(&mut self, txt: &str) -> Vec<StatefulEntry> {
        self.entries
            .iter()
            .filter(|entry| (self.predicate)(&entry.name, txt))
            .map(|entry| StatefulEntry {
                entry: entry.clone(),
                selected: false,
            })
            .collect()
    }
}

impl ExactFilter {
    fn new_infix_filter(entries: Vec<Entry>) -> Self {
        ExactFilter {
            entries,
            predicate: |entry, text| entry.to_lowercase().contains(&text.to_lowercase()),
        }
    }

    fn new_prefix_filter(entries: Vec<Entry>) -> Self {
        ExactFilter {
            entries,
            predicate: |entry, text| entry.to_lowercase().starts_with(&text.to_lowercase()),
        }
    }
}

/// implements a fuzzy filter for entries
struct FuzzyFilter {
    /// entries to filter
    entries: Vec<Entry>,

    /// the fuzzy matcher to use
    matcher: Nucleo<usize>,
}

impl FilterOperator for FuzzyFilter {
    fn filter(&mut self, txt: &str) -> Vec<StatefulEntry> {
        self.matcher
            .pattern
            .reparse(0, txt, CaseMatching::Smart, Normalization::Smart, true);
        // simply wait for up to 10ms. Hope this is enough ;)
        self.matcher.tick(10);
        let snapshot = self.matcher.snapshot();

        snapshot
            .matched_items(0..snapshot.matched_item_count())
            .map(|item| {
                self.entries
                    .get(*item.data)
                    .expect("Filtered item must be in part of unfiltered items.")
            })
            .map(|entry| StatefulEntry {
                entry: entry.to_owned(),
                selected: false,
            })
            .collect()
    }
}

impl FuzzyFilter {
    /// create a new fuzzy filter
    ///
    /// # Arguments
    ///
    /// * `entries` - list of entries to filter
    fn new(entries: &[Entry]) -> Self {
        // @todo properly use callback
        // Most users of nucleo ignore it and just wait but this might fail
        let matcher = Nucleo::new(nucleo::Config::DEFAULT, Arc::new(|| {}), None, 1);
        let injector = matcher.injector();

        // push the idx as the value to return and name as the text to fuzzy search
        for (idx, entry) in entries.iter().enumerate() {
            injector.push(idx, move |_, cols| cols[0] = entry.name.clone().into());
        }

        FuzzyFilter {
            entries: entries.to_vec(),
            matcher,
        }
    }
}

#[cfg(test)]
#[path = "test_feeds.rs"]
mod tests;
