/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::core::{
    feeds::{Fallthrough, FilterType, LoadingStrategy},
    Color, Length,
};
use serde::Deserialize;

/// This is the central config struct used to configure all parts of the application.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Config {
    /// ordered list of feeds
    pub feeds: Vec<FeedConfig>,

    /// feed-independent configuration options
    pub general: GeneralConfig,
}

/// Representation of all feed-independent, i.e. general, configuration options
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GeneralConfig {
    /// fallthrough strategy to use
    pub fallthrough: Fallthrough,

    /// loading strategy to use (lazy or eager)
    pub loading: LoadingStrategy,

    /// terminal emulator to use for terminal applications
    /// e.g. `alacritty -e`.
    /// This will be prepended by the command to execute
    pub terminal: String,

    /// layout to use, can be either 'Dock' or 'Splash'.
    /// With 'Dock', the menu appears at the top of the screen
    /// and entries are rendered as a horizontal line.
    /// This is similar to dmenu.
    /// With 'Splash' a splash screen is shown at the center of the screen.
    /// Entries are rendered in a column.
    pub layout: MenuLayout,

    /// height of the menu bar (in px, pt or % of screen height)
    pub height: Length,

    /// font to use for text (fontconfig name)
    pub font: String,

    /// font size in pt
    pub font_size: u32,

    /// icon theme to use for application icons
    pub icon_theme: String,

    /// size of an icon (in px, pt or % of screen height)
    pub icon_size: Length,

    /// color of text for entries
    pub text_color: Color,

    /// background color for entries
    pub bg_color: Color,

    /// color of text for highlighted entries
    pub text_color_hl: Color,

    /// color of background for highlighted entries
    pub bg_color_hl: Color,

    /// width of the input field (in px, pt or % of screen width)
    pub input_width: Option<Length>,
}

/// Enumeration of included themes
/// Used if one does not want to provide a custom themes
/// or needs a starting point for creating one.
#[derive(Debug, Default, Clone, Copy, Deserialize, PartialEq, Eq)]
pub enum MenuLayout {
    /// default themes with comments
    #[default]
    Dock,

    /// simple dark theme in black & white
    Splash,
    // simple light theme in black & white
    // Grid,
}

/// Modifier keys
#[derive(Debug, Clone, Deserialize, Eq, PartialEq)]
pub enum Modifier {
    Alt,
    AltGr,
    Ctrl,
    Meta,
    Shift,
}

/// Combination of keys that are used as a keybind
#[derive(Debug, Clone, Deserialize, PartialEq, Eq)]
pub struct Keybind {
    /// list of modifier keys to be pressed
    pub modifier: Vec<Modifier>,

    /// single character to be pressed
    pub key: char,
}

/// configuration of a single feed
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FeedConfig {
    /// filter to apply to feed
    pub options: FeedOptions,

    /// keybind to activate that feed
    pub keybind: Option<Keybind>,
}

/// options specific to a feed type
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum FeedOptions {
    DesktopEntries(DesktopEntriesConfig),
    DirectExecution,
    PathEntries(PathEntriesConfig),
}

/// options for desktop entry feed
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DesktopEntriesConfig {
    /// filter to apply to feed
    pub filter: FilterType,

    /// only show entries with icons
    pub requires_icons: bool,
}

/// options for path entry feed
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PathEntriesConfig {
    /// filter to apply to feed
    pub filter: FilterType,
}
