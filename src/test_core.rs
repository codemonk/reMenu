/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
use super::{
    feeds::{EntryFeed, Feed, FilterType},
    Entry,
};

#[test]
fn equality_for_entry() {
    let entry1 = Entry {
        cmd: "/usr/bin/cmd1".to_string(),
        name: "MyApp".to_string(),
        comment: None,
        terminal: None,
        icon: None,
    };

    // the same entry
    assert_eq!(entry1, entry1);

    // the same entry (cloned, referential transparency)
    assert_eq!(entry1, entry1.clone());
}

#[test]
fn inequality_for_entry() {
    let entry1 = Entry {
        name: "MyApp".to_string(),
        cmd: "/usr/bin/cmd1".to_string(),
        comment: None,
        terminal: None,
        icon: None,
    };

    let entry2 = Entry {
        name: "MyApp".to_string(),
        cmd: "/usr/bin/cmd2".to_string(),
        comment: None,
        terminal: Some(false),
        icon: Some("".to_string()),
    };

    // entries with same name
    assert_ne!(entry1, entry2);

    // entries with different name
    assert_ne!(
        entry1,
        Entry {
            name: "YourApp".to_string(),
            ..entry1.clone()
        }
    );

    // entries with different cmd
    assert_ne!(
        entry1,
        Entry {
            cmd: "happy".to_string(),
            ..entry1.clone()
        }
    );

    // entries with different comment
    assert_ne!(
        entry1,
        Entry {
            comment: Some("This is my comment".to_string()),
            ..entry1.clone()
        }
    );

    // entries with different terminal
    assert_ne!(
        entry1,
        Entry {
            terminal: Some(true),
            ..entry1.clone()
        }
    );
}

#[test]
fn order_by_name_first() {
    // ensure precendence of elements: if everything is different, name should take precendence
    let first = Entry {
        name: "MyApp".to_string(),
        cmd: "/usr/bin/cmd1".to_string(),
        comment: None,
        terminal: None,
        icon: None,
    };
    let second = Entry {
        name: "Go go go".to_string(),
        cmd: "/usr/bin/go".to_string(),
        comment: Some("Weird app".to_string()),
        terminal: None,
        icon: None,
    };
    let third = Entry {
        name: "App runner".to_string(),
        cmd: "/usr/bin/run".to_string(),
        comment: None,
        terminal: Some(false),
        icon: None,
    };
    let fourth = Entry {
        name: "App Executor".to_string(),
        cmd: "/usr/bin/exec".to_string(),
        comment: Some("Fear the executor".to_string()),
        terminal: Some(true),
        icon: None,
    };

    assert!(first.ne(&second));
    assert!(first.gt(&second));
    assert!(second.lt(&first));

    assert!(second.ne(&third));
    assert!(second.gt(&third));
    assert!(third.lt(&second));

    assert!(third.ne(&fourth));
    assert!(third.gt(&fourth));
    assert!(fourth.lt(&third));
}

#[test]
fn order_by_cmd_second() {
    // if everthing is different except name, cmd is decisive
    let first = Entry {
        name: "App".to_string(),
        cmd: "/usr/bin/cmd1".to_string(),
        comment: None,
        terminal: None,
        icon: None,
    };
    let second = Entry {
        name: "App".to_string(),
        cmd: "/usr/bin/go".to_string(),
        comment: Some("Weird app".to_string()),
        terminal: None,
        icon: None,
    };
    let third = Entry {
        name: "App".to_string(),
        cmd: "/usr/bin/run".to_string(),
        comment: None,
        terminal: Some(false),
        icon: None,
    };
    let fourth = Entry {
        name: "App".to_string(),
        cmd: "/usr/bin/exec".to_string(),
        comment: Some("Fear the executor".to_string()),
        terminal: Some(true),
        icon: None,
    };

    assert!(first.ne(&second));
    assert!(first.lt(&second));
    assert!(second.gt(&first));

    assert!(second.ne(&third));
    assert!(second.lt(&third));
    assert!(third.gt(&second));

    assert!(third.ne(&fourth));
    assert!(third.gt(&fourth));
    assert!(fourth.lt(&third));
}

#[test]
fn create_new_empty_filter() {
    let filter = EntryFeed::new(Vec::new(), FilterType::StartsWith, false);
    assert!(filter.get_filtered().is_empty());
    assert!(filter.get_selected().is_none());
    assert!(filter.get_selection_index().is_none());
}
