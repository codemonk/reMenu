/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
use thiserror::Error;

pub mod actions;

pub mod feeds;

/**
User-facing config
**/
pub mod config;

/// representation of an icon by its name
pub type Icon = String;

/// common error types
#[derive(Error, Debug, Eq, PartialEq)]
pub enum CoreError {
    /// raised if an access is or would be out of bounds
    #[error("Access would be out of bounds")]
    OutOfBounds,
}

/// represents a number of pixels
/// signed to represent a negative different
pub type Pixels = i32;

/// represent a onedimensional distance (a length)
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Length {
    // length in pixels
    Pixel(Pixels),

    // length in points (equivalent to logical pixels in this case)
    Point(Pixels),

    // length in percent
    Percentage(Pixels),
}

/// represent a color as RGBA
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
pub struct Color {
    /// red component
    pub red: u8,
    /// green component
    pub green: u8,
    /// blue component
    pub blue: u8,
    ///alpha component
    pub alpha: u8,
}

/// representation of image data in RGBA, 8 bit per channel format
// @todo use Pixels?
pub struct Pixmap {
    buffer: Vec<u8>,
    width: u32,
    height: u32,
}

impl Pixmap {
    pub fn new(buffer: Vec<u8>, width: u32, height: u32) -> Option<Pixmap> {
        if Ok(width * height * 4) == buffer.len().try_into() {
            Some(Pixmap {
                buffer,
                width,
                height,
            })
        } else {
            None
        }
    }

    pub fn pixels(&self) -> &Vec<u8> {
        &self.buffer
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn width(&self) -> u32 {
        self.width
    }
}

/// an executable entry for the menu
#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Debug)]
pub struct Entry {
    /// name to display
    pub name: String,

    /// command to execute
    pub cmd: String,

    /// an additional comment (commonly used for popups)
    pub comment: Option<String>,

    /// flag indicating if an entry is to be executed in terminal or not
    pub terminal: Option<bool>,

    /// icon
    pub icon: Option<Icon>,
}

/// helper struct for managing entries displayed in menu
#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord)]
pub struct StatefulEntry {
    ///entry to display
    pub entry: Entry,

    /// indicate if the entry is currently selected or not
    pub selected: bool,
}

#[cfg(test)]
#[path = "test_core.rs"]
mod tests;
