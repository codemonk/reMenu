/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::io::GraphicsStore;
use crate::{
    core::{Color, Length},
    ui::{Direction, ScreenInfo, Surface, SurfaceProducer},
};
use anyhow::{Context, Result};
use log::warn;

/// width calculation mode for rendering
#[derive(Clone, Debug, Default)]
pub enum RenderWidth {
    /// use the configured width
    #[default]
    Fixed,

    /// calculate width based on content
    Flexible,
}

/// Configuration options for TextField
#[derive(Debug, Clone)]
pub struct TextFieldConfig {
    /// color of text
    pub text_color: Color,

    /// background color
    pub bg_color: Color,

    /// color of text when highlighted
    pub text_color_hl: Color,

    /// background color when highlighted
    pub bg_color_hl: Color,

    /// height of textfield
    pub height: Length,

    /// width of textfield
    pub width: Length,

    /// padding (space between text and border) of textfield
    /// note: height & width take precedence over calculated sizes
    /// It is expected that necessary calculations are done when filling this struct
    pub padding: Length,

    /// select width calcuation mode for rendering
    pub render_width: RenderWidth,

    /// width of the caret; 0 if no caret is used
    pub caret_width: Length,
}

impl Default for TextFieldConfig {
    fn default() -> TextFieldConfig {
        TextFieldConfig {
            text_color: Color {
                red: 0xFF,
                green: 0xFF,
                blue: 0xFF,
                alpha: 0xFF,
            },
            bg_color: Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00,
                alpha: 0xFF,
            },
            text_color_hl: Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00,
                alpha: 0xFF,
            },
            bg_color_hl: Color {
                red: 0xFF,
                green: 0xFF,
                blue: 0xFF,
                alpha: 0xFF,
            },
            height: Length::Pixel(0),
            width: Length::Pixel(0),
            padding: Length::Pixel(0),
            render_width: RenderWidth::Fixed,
            caret_width: Length::Pixel(0),
        }
    }
}

/// Textfield is a widget for displaying text
pub struct TextField<R: Surface> {
    /// configuration applied to that textfield
    config: TextFieldConfig,

    /// text to display
    text: String,

    /// name of icon to display
    icon: Option<String>,

    /// target surface for rendering of text & background
    surface: R,

    /// hightlight state of textfield (true: highlighted, false: not highlighted)
    highlight: bool,

    /// indicate if text, icon or background needs to be redrawn
    tainted: bool,

    /// width of the last rendered content
    content_width: i32,
}

impl<R> TextField<R>
where
    R: Surface,
{
    /// create a new TextField
    ///
    /// # Arguments
    ///
    /// * `producer` - The rendering system for which target surfaces are produced
    /// * `config` - configuration options to apply to this textfield
    pub fn new<P: SurfaceProducer<Output = R>, S: ScreenInfo>(
        producer: &P,
        config: TextFieldConfig,
        screen: &S,
    ) -> Result<TextField<R>> {
        Ok(TextField {
            config: config.clone(),
            text: "".to_owned(),
            icon: None,
            surface: producer
                .create_surface(
                    screen.to_pixels(config.width, Direction::Horizontal),
                    screen.to_pixels(config.height, Direction::Vertical),
                )
                .with_context(|| "Failed to create renderable in textfiled")?,
            highlight: false,
            tainted: true,
            content_width: 0,
        })
    }

    /// render content of text field to configured target surface
    ///
    /// # Arguments
    ///
    /// * `x` - horizontal position of the text field (places the left of the text field)
    /// * `y` -  vertical position of the text field (places the top of the text field)
    /// * `store` - graphics store for gyphs and icons. Must be the same used for set_text()
    /// * `screen` - information about the target screen. Must be the same used for set_text()
    pub fn render<S: ScreenInfo>(
        &mut self,
        x: i16,
        y: i16,
        store: &GraphicsStore,
        screen: &S,
    ) -> Result<()> {
        let configured_width = screen.to_pixels(self.config.width, Direction::Horizontal);

        if self.tainted {
            let clr = match self.highlight {
                true => self.config.text_color_hl,
                false => self.config.text_color,
            };

            match self.highlight {
                true => self.surface.fill(self.config.bg_color_hl)?,
                false => self.surface.fill(self.config.bg_color)?,
            }

            let tf_padding = screen.to_pixels(self.config.padding, Direction::Horizontal);
            let mut current_x = tf_padding;
            if let (Some(icon), Some(theme)) = (&self.icon, &store.theme) {
                match theme.load_scaled(icon) {
                    Ok(pixmap) => {
                        if pixmap.width() as i32 > theme.size() {
                            warn!("Pixmap is larger than icon size.");
                        } else {
                            self.surface.put(
                                pixmap.pixels(),
                                current_x,
                                tf_padding,
                                pixmap.width() as i32,
                                pixmap.height() as i32,
                            )?
                        }
                    }
                    Err(e) => warn!("Failed to load icon: {} with error {:?}", icon, e),
                }
                current_x += theme.size() + tf_padding;
            }
            for ch in self.text.chars() {
                // @todo maybe try with fallback character or no char if error is missing char
                let glyph = store.font.get_char(ch)?;
                debug_assert_eq!(glyph.bitmap.len(), (glyph.width * glyph.height) as usize);
                let current_y: i32 = tf_padding + store.font.ascender - glyph.ascender;
                self.surface
                    .put_colorized(
                        &glyph.bitmap,
                        current_x + glyph.left_offset,
                        current_y,
                        glyph.width as i32,
                        glyph.height as i32,
                        clr,
                    )
                    .with_context(|| "Error while putting characters on canvas")?;
                current_x += glyph.advance_x;
            }
            current_x += tf_padding;

            self.content_width = if current_x > configured_width {
                configured_width
            } else {
                current_x
            };

            // render caret
            let cw = screen.to_pixels(self.config.caret_width, Direction::Horizontal);
            if cw != 0 {
                // substract descender because it is relative to baseline and thus negative
                let caret =
                    vec![0xFF; (cw * (store.font.ascender - store.font.descender)) as usize];
                self.surface
                    .put_colorized(
                        caret.as_slice(),
                        current_x,
                        tf_padding,
                        cw,
                        store.font.ascender - store.font.descender,
                        clr,
                    )
                    .with_context(|| "Error while putting caret on canvas")?;
            }
            self.tainted = false;
        }

        self.surface.render(x, y)?;

        Ok(())
    }

    /// Set icon of text field
    ///
    /// Assign new icon name to text field and trigger recalculation of its visible width.
    /// # Arguments
    ///
    /// * `txt' - The new text to display
    pub fn set_icon(&mut self, icon: &Option<String>) -> Result<()> {
        self.icon.clone_from(icon);
        self.tainted = true;

        Ok(())
    }

    /// Set text of text field
    ///
    /// Assign new text string to text field and recalculates its visible width.
    /// # Arguments
    ///
    /// * `txt' - The new text to display
    pub fn set_text(&mut self, txt: &str) -> Result<()> {
        txt.clone_into(&mut self.text);
        self.tainted = true;

        Ok(())
    }

    /// set text color to use when not highlighted
    ///
    /// # Arguments
    ///
    /// * `clr` - new color for text
    pub fn set_text_color(&mut self, clr: Color) {
        self.config.text_color = clr;
        self.tainted = true;
    }

    /// set background color to use when not highlighted
    ///
    /// # Arguments
    ///
    /// * `clr` - new color for background
    pub fn set_background_color(&mut self, clr: Color) {
        self.config.bg_color = clr;
        self.tainted = true;
    }

    /// set text color to use when highlighted
    ///
    /// # Arguments
    ///
    /// * `clr` - new color for text
    pub fn set_text_color_hl(&mut self, clr: Color) {
        self.config.text_color_hl = clr;
        self.tainted = true;
    }

    /// set background color to use when highlighted
    ///
    /// # Arguments
    ///
    /// * `clr` - new color for background
    pub fn set_background_color_hl(&mut self, clr: Color) {
        self.config.bg_color_hl = clr;
        self.tainted = true;
    }

    /// set highlight mode
    ///
    /// Set textfield to highlighted or not highlighted.
    /// Decides which colors to use for rendering
    ///
    /// # Arguments
    ///
    /// * `hl` - highlight enabled true/false
    pub fn set_highlight(&mut self, hl: bool) {
        self.highlight = hl;
        self.tainted = true;
    }

    /// get length
    ///
    /// Returns the length of icon and text clipped at the textfield width.
    /// This returns the length of the last rendered text and icon.
    pub fn length(&self) -> i32 {
        self.content_width
    }
}

/* for testing
//let txt_pict = conn.generate_id();
const B: u8 = 0x00; // background
const C: u8 = 0xCC; // corner
const E: u8 = 0xEE; // edge
const F: u8 = 0xFF; // foreground
const CROSSIMG: [u8; 480] = [
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 0
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 1
    B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, // 2
    B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, // 3
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 4
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 5
    B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, // 6
    B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, // 7
    B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, // 8
    B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, // 9
    B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, // 10
    B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, // 11
    B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, // 12
    B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, // 13
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 14
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 15
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 16
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 17
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 18
    B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, // 19
    B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, // 20
    B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, // 21
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 22
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 23
];

const CROSSIMG_SQUARE: [u8; 576] = [
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 0
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 1
    B, B, B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, B, B, // 2
    B, B, B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, B, B, // 3
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 4
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 5
    B, B, B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, B, B, // 6
    B, B, B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, B, B, // 7
    B, B, B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, B, B, // 8
    B, B, B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, B, B, // 9
    B, B, B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, B, B, // 10
    B, B, B, B, E, E, F, F, F, F, F, F, F, F, F, F, F, F, E, E, B, B, B, B, // 11
    B, B, B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, B, B, // 12
    B, B, B, B, C, C, E, E, E, E, F, F, F, F, E, E, E, E, C, C, B, B, B, B, // 13
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 14
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 15
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 16
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 17
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 18
    B, B, B, B, B, B, B, B, E, E, F, F, F, F, E, E, B, B, B, B, B, B, B, B, // 19
    B, B, B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, B, B, // 20
    B, B, B, B, B, B, B, B, C, C, E, E, E, E, C, C, B, B, B, B, B, B, B, B, // 21
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 22
    B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, B, // 23
];
*/

// @todo add test for set_text() that check if length are correctly calculated on
// characters that take more than one byte (e.g. heart emoji, chinese)
// also check for surrogate pairs (e.g. flag emoji). Do we really need them?
