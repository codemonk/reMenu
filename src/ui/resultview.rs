// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::core::StatefulEntry;
use crate::io::GraphicsStore;
use crate::ui::{
    textfield::TextFieldConfig, Color, Direction, Length, RenderWidth, ScreenInfo, Surface,
    SurfaceProducer, TextField,
};
use anyhow::{Context, Result};
use log::debug;

type Position = usize;
const LOWER: usize = 0;

/// Configuration for a ResultView
/// Contains all parameters for changing the behaviour and look of a result view
#[derive(Debug)]
pub struct ResultViewConfig {
    /// space between to text and the border of and entry (in pixels)
    pub padding: Length,

    /// space between to entries
    pub separator: Length,

    /// color of text in an entry
    pub text_color: Color,

    /// background color of an entry
    pub bg_color: Color,

    /// color of text in an entry when highlighted
    pub text_color_hl: Color,

    /// background color of an entry when highlighted
    pub bg_color_hl: Color,

    /// total width of a result view
    pub width: Length,

    /// total height of a result view
    pub height: Length,

    /// maximum number of results
    pub max_results: usize,

    /// maximum width of an entry
    pub max_result_width: Length,
}

impl Default for ResultViewConfig {
    fn default() -> Self {
        ResultViewConfig {
            padding: Length::Pixel(4),
            separator: Length::Pixel(20),
            text_color: Color {
                red: 0xFF,
                green: 0xFF,
                blue: 0xFF,
                alpha: 0xFF,
            },
            bg_color: Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00,
                alpha: 0xFF,
            },
            text_color_hl: Color {
                red: 0x00,
                green: 0x00,
                blue: 0x00,
                alpha: 0xFF,
            },
            bg_color_hl: Color {
                red: 0xFF,
                green: 0xFF,
                blue: 0xFF,
                alpha: 0xFF,
            },
            width: Length::Pixel(0),
            height: Length::Pixel(0),
            max_results: 20,
            max_result_width: Length::Pixel(240),
        }
    }
}

pub struct ResultView<R: Surface> {
    config: ResultViewConfig,
    cursor_position: Position, // @todo should this be optional?

    // entries to display
    entries: Vec<StatefulEntry>,

    /// list of start positions of pages
    /// This list is generated lazily, since paging is seldomly used.
    pages: Vec<Option<usize>>,

    // the currently visible page
    current_page: usize,

    /// current visible page
    outputs: Vec<TextField<R>>,
}

impl<R> ResultView<R>
where
    R: Surface,
{
    /// create a new ResultView
    pub fn new<P: SurfaceProducer<Output = R>, S: ScreenInfo>(
        producer: &P,
        config: ResultViewConfig,
        screen: &S,
    ) -> Result<ResultView<R>> {
        let mut outputs = Vec::with_capacity(config.max_results);
        for _idx in 0..config.max_results {
            let mut tf = TextField::new(
                producer,
                TextFieldConfig {
                    width: config.max_result_width,
                    height: config.height,
                    padding: config.padding,
                    render_width: RenderWidth::Flexible,
                    ..TextFieldConfig::default()
                },
                screen,
            )?;
            tf.set_text_color(config.text_color);
            tf.set_text_color_hl(config.text_color_hl);
            tf.set_background_color(config.bg_color);
            tf.set_background_color_hl(config.bg_color_hl);
            outputs.push(tf);
        }

        Ok(ResultView {
            config,
            cursor_position: LOWER,
            entries: Vec::new(),
            pages: vec![Some(0), None],
            current_page: 0,
            outputs,
        })
    }

    pub fn decrease_selection_positon(&mut self) {
        if self.cursor_position > 0 {
            self.cursor_position -= 1;
            debug!("Changed cursor position to {}", self.cursor_position);
        } else if self.current_page > 0 {
            debug_assert!(self.pages[self.current_page].is_some());
            debug_assert!(self.pages[self.current_page - 1].is_some());

            let page_end = self.pages[self.current_page].unwrap();
            self.current_page -= 1;
            let page_start = self.pages[self.current_page].unwrap();

            debug_assert!(page_end - page_start > 0);
            self.cursor_position = page_end - 1 - page_start;
            debug!("Change page to {}", self.current_page);
        }
    }

    /// increases the selection position and switches pages when necessary
    pub fn increase_selection_positon(&mut self) -> Result<()> {
        debug_assert!(self.current_page + 1 < self.pages.len());
        debug_assert!(self.pages[self.current_page].is_some());
        let page_start = self.pages[self.current_page].unwrap();
        let page_end = match self.pages[self.current_page + 1] {
            Some(idx) => idx,
            None => self.entries.len(),
        };

        if self.cursor_position + 1 < page_end - page_start {
            self.cursor_position += 1;
            debug!("Changed cursor position to {}", self.cursor_position);
        } else if self.pages[self.current_page + 1].is_some() {
            self.current_page += 1;
            self.cursor_position = 0;
            debug!("Change page to {}", self.current_page);
        }

        Ok(())
    }

    /// render the current state of this view to the given position
    pub fn render<S: ScreenInfo>(
        &mut self,
        x: i16,
        y: i16,
        store: &GraphicsStore,
        screen: &S,
    ) -> Result<()> {
        debug_assert!(self.current_page + 1 < self.pages.len());
        debug_assert!(self.pages[self.current_page].is_some());
        let page_start = self.pages[self.current_page].unwrap();
        let relevant_entries = &self.entries[page_start..];

        let mut current_x = x;
        let mut num_rendered = 0;
        for idx in 0..self.entries.len() {
            if let Some(entry) = relevant_entries.get(idx) {
                debug!("Render entry `{}` of result view.", entry.entry.name);
                self.outputs[idx].set_text(&entry.entry.name)?;
                self.outputs[idx].set_icon(&entry.entry.icon)?;
                if idx == self.cursor_position {
                    self.outputs[idx].set_highlight(true);
                } else {
                    self.outputs[idx].set_highlight(false);
                };

                self.outputs[idx]
                    .render(current_x, y, store, screen)
                    .with_context(|| "Error when rendering fields of result view")?;

                current_x += (screen.to_pixels(self.config.separator, Direction::Horizontal)
                    + self.outputs[idx].length()) as i16;
                num_rendered += 1;

                if current_x >= screen.width() as i16 {
                    break;
                }
            }
        }

        if self.pages[self.current_page + 1].is_none()
            && relevant_entries.len() > num_rendered
            && num_rendered >= 1
        {
            debug!(
                "Add new page beginning at {}.",
                page_start + num_rendered - 1
            );
            self.pages
                .insert(self.pages.len() - 1, Some(page_start + num_rendered - 1));
        }

        Ok(())
    }

    /// change the entries to display
    /// this resets cursor position and visible page
    pub fn set_entries(&mut self, entries: Vec<StatefulEntry>) -> Result<()> {
        debug!("Set entries for result view: {}", entries.len());
        self.entries = entries;

        self.cursor_position = 0;
        self.pages = vec![Some(0), None];
        self.current_page = 0;

        Ok(())
    }
}
