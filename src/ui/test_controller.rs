/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::core::actions::{Action, ActionHandler, UiOutputHandler};
use crate::core::config::{
    Config, DesktopEntriesConfig, FeedConfig, FeedOptions, GeneralConfig, Keybind, MenuLayout,
    Modifier, PathEntriesConfig,
};
use crate::core::feeds::{Fallthrough, FilterType, LoadingStrategy};
use crate::core::{Color, Length};
use crate::io::config_file;
use crate::ui::controller::Controller;
use crate::ui::{Event, Key};
use anyhow::Result;

// @todo: Create a macro that automates the error text currently added manually to each assert
// This also compares actions as done by this function: assert_same_action
fn is_same_action<O: UiOutputHandler>(left: &Action<O>, right: &Action<O>) -> bool {
    match (left, right) {
        (Action::AddChar(cl), Action::AddChar(cr)) => cl == cr,
        (Action::ChooseSelection, Action::ChooseSelection) => true,
        (Action::Exit, Action::Exit) => true,
        (Action::MoveSelLeft, Action::MoveSelLeft) => true,
        (Action::MoveSelRight, Action::MoveSelRight) => true,
        (Action::None, Action::None) => true,
        (Action::RemoveChar, Action::RemoveChar) => true,
        (Action::SetLayer(ll), Action::SetLayer(lr)) => ll == lr,
        (Action::SetOutputHandler(_), Action::SetOutputHandler(_)) => true, // there is no good way to compare output handlers, so we take this simplifcation
        (Action::Redraw, Action::Redraw) => true,
        (_, _) => false,
    }
}

fn assert_same_action<O: UiOutputHandler>(actual: Result<Action<O>>, expected: Result<Action<O>>) {
    let actual_action = actual
        .expect("Expected an action to test, received an error instead for value under test.");
    let expected_action = expected
        .expect("Expected an action to test, received an error instead for value to test against.");

    assert!(
        is_same_action(&actual_action, &expected_action),
        "Expected action {:?} but found action is {:?} instead",
        expected_action,
        actual_action
    );
}

fn create_controller() -> Controller {
    let config = Config {
        feeds: vec![
            FeedConfig {
                options: FeedOptions::DesktopEntries(DesktopEntriesConfig {
                    filter: FilterType::StartsWith,
                    requires_icons: false,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '1',
                }),
            },
            FeedConfig {
                options: FeedOptions::PathEntries(PathEntriesConfig {
                    filter: FilterType::StartsWith,
                }),
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '2',
                }),
            },
            FeedConfig {
                options: FeedOptions::DirectExecution,
                keybind: Some(Keybind {
                    modifier: vec![Modifier::Ctrl],
                    key: '3',
                }),
            },
        ],
        general: GeneralConfig {
            fallthrough: Fallthrough::Stepwise,
            loading: LoadingStrategy::Lazy,
            layout: MenuLayout::Dock,
            terminal: "alacritty -e".to_string(),
            height: Length::Pixel(32),
            font: "DejaVu Sans".to_string(),
            font_size: 11,
            icon_theme: "Papirus".to_string(),
            icon_size: Length::Pixel(24),
            text_color: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            bg_color: Color {
                red: 47,
                green: 61,
                blue: 68,
                alpha: 255,
            },
            text_color_hl: Color {
                red: 47,
                green: 61,
                blue: 68,
                alpha: 255,
            },
            bg_color_hl: Color {
                red: 255,
                green: 255,
                blue: 255,
                alpha: 255,
            },
            input_width: Some(Length::Pixel(600)),
        },
    };

    let feeds = config_file::build_feed(&config);

    let ah = ActionHandler::new(feeds, &config.general.terminal);

    Controller::new(config, ah)
}

#[test]
fn test_select_action_other() {
    let mut ctrl = create_controller();
    assert_same_action(ctrl.select_action(Event::Other), Ok(Action::None));
}

#[test]
fn test_select_action_redraw() {
    let mut ctrl = create_controller();
    assert_same_action(ctrl.select_action(Event::Redraw), Ok(Action::Redraw));
}

#[test]
fn test_select_action_exit_no_modifiers() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Escape)),
        Ok(Action::Exit),
    );
}

#[test]
fn test_select_action_exit_shift() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Escape)),
        Ok(Action::Exit),
    );
}

#[test]
fn test_select_action_exit_alt_meta() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Meta)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.meta_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Escape)),
        Ok(Action::Exit),
    );
}

#[test]
fn test_select_action_move_selection_left_arrow() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::LeftArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_left_arrow_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::LeftArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_left_arrow_alt_meta() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Meta)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.meta_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::LeftArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_right_arrow() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::RightArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_right_arrow_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::RightArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_right_arrow_alt_meta() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Meta)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.meta_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::RightArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_up_arrow() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::UpArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_up_arrow_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::UpArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_up_arrow_alt_meta() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Meta)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.meta_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::UpArrow)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_down_arrow() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::DownArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_down_arrow_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::DownArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_down_arrow_alt_meta() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Meta)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.meta_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::DownArrow)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_tab() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Tab)),
        Ok(Action::MoveSelRight),
    );
}

#[test]
fn test_select_action_move_selection_tab_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );

    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Tab)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_move_selection_tab_alt_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Tab)),
        Ok(Action::MoveSelLeft),
    );
}

#[test]
fn test_select_action_choose_selection_on_return() {
    let mut ctrl = create_controller();
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Return)),
        Ok(Action::ChooseSelection),
    );
}

#[test]
fn test_select_action_choose_selection_on_return_shift() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Shift)),
        Ok(Action::None),
    );
    assert!(ctrl.shift_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Return)),
        Ok(Action::ChooseSelection),
    );
}

#[test]
fn test_select_action_choose_selection_on_return_alt_ctrl() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Ctrl)),
        Ok(Action::None),
    );

    assert!(ctrl.alt_key);
    assert!(ctrl.ctrl_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Return)),
        Ok(Action::ChooseSelection),
    );
}

#[test]
fn test_select_action_set_layer_0() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Ctrl)),
        Ok(Action::None),
    );
    assert!(ctrl.ctrl_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Character('1'))),
        Ok(Action::SetLayer(0)),
    );
}

#[test]
fn test_select_action_add_char_on_ctrl_alt_1() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Ctrl)),
        Ok(Action::None),
    );
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Alt)),
        Ok(Action::None),
    );

    assert!(ctrl.ctrl_key);
    assert!(ctrl.alt_key);

    // note: During normal use, the tested behaviour will not happen.
    // Instead, a different key might be reported based on the active keymap.
    // We just test that keys pass the hotkey filter.
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Character('1'))),
        Ok(Action::AddChar('1')),
    );
}

#[test]
fn test_select_action_set_layer_2() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Ctrl)),
        Ok(Action::None),
    );
    assert!(ctrl.ctrl_key);

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Character('3'))),
        Ok(Action::SetLayer(2)),
    );
}

#[test]
fn test_select_action_add_char_on_ctrl_4() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Ctrl)),
        Ok(Action::None),
    );
    assert!(ctrl.ctrl_key);

    // note: During normal use, the tested behaviour will not happen.
    // Instead, a different key might be reported based on the active keymap.
    // We just test that keys pass the hotkey filter.
    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Character('4'))),
        Ok(Action::AddChar('4')),
    );
}

#[test]
fn test_select_action_add_char_on_f() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Character('f'))),
        Ok(Action::AddChar('f')),
    );
}

#[test]
fn test_select_action_remove_char() {
    let mut ctrl = create_controller();

    assert_same_action(
        ctrl.select_action(Event::KeyPressed(Key::Backspace)),
        Ok(Action::RemoveChar),
    );
}

// note: create window is not tested.
// writing a test is difficult and a simple start of the application confirms correct behaviour
