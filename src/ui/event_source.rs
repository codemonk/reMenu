/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::{
    core::config::MenuLayout,
    ui::{window::Window, Event, Key, Length},
};
use anyhow::Result;
use log::{debug, error};
use std::rc::Rc;
use winit::{
    application::ApplicationHandler,
    event::{DeviceEvent, ElementState, WindowEvent},
    event_loop::{ActiveEventLoop, ControlFlow, EventLoop},
    platform::x11::{WindowAttributesExtX11, WindowType},
    window::{Window as WinitWnd, WindowId},
};

/// Source for input (and other) events
///
/// This is the driving force of the systems.
/// Encapsules winit event loop and calls an event handler when a relevant event occurs.
pub struct EventSource {
    /// height of the window
    /// Used by event loop to create the window.
    height: Length,

    /// layout to use when creating a window
    layout: MenuLayout,
}

impl EventSource {
    /// create a new event loop
    ///
    /// # Arguments:
    ///
    /// * `height` - height of the window (used in create window events)
    pub fn new(height: Length, layout: MenuLayout) -> Result<EventSource> {
        Ok(EventSource { height, layout })
    }

    /// run the main event loop
    ///
    /// Wait for events in winit event loop and call callback on events.
    ///
    /// # Arguments
    ///
    /// * `callback` - function to call on events
    pub fn run(&mut self, callback: impl FnMut(Event) -> Result<bool>) -> Result<()> {
        let event_loop = EventLoop::new()?;
        event_loop.set_control_flow(ControlFlow::Wait);
        let mut app = App {
            height: self.height,
            callback,
            layout: self.layout,
        };
        event_loop.run_app(&mut app)?;

        Ok(())
    }
}

/// internall structure create by [`EventSource`] to implement winit event loop
struct App<F: FnMut(Event) -> Result<bool>> {
    /// height of the window (used when creating a window)
    height: Length,

    /// function to call when an event occurs
    callback: F,

    /// layout to use when creating a window
    layout: MenuLayout,
}

impl<F: FnMut(Event) -> Result<bool>> ApplicationHandler for App<F> {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        let screen = event_loop
            .primary_monitor()
            .expect("Could not get primary screen");
        let window_type = match self.layout {
            MenuLayout::Dock => WindowType::Dock,
            MenuLayout::Splash => WindowType::Splash,
        };
        let attrs = WinitWnd::default_attributes()
            .with_decorations(false)
            .with_window_level(winit::window::WindowLevel::AlwaysOnTop)
            .with_resizable(false)
            .with_transparent(false)
            .with_x11_window_type(vec![window_type]); // @todo do this only on X11, find solution for wayland (probably layer shell)
        let wnd = Rc::new(
            event_loop
                .create_window(attrs)
                .expect("Could not create winit window"),
        );
        wnd.focus_window();

        // create window
        let screen_size = screen.size();
        let screen_width_px = screen_size.width;
        let screen_height_px = screen_size.height;
        (self.callback)(Event::WindowCreated(
            Window::new(wnd, screen_width_px, screen_height_px, self.height)
                .expect("Creating a window failed"),
        ))
        .expect("Registering a new window failed.");
    }

    fn window_event(&mut self, event_loop: &ActiveEventLoop, _id: WindowId, event: WindowEvent) {
        match event {
            WindowEvent::CloseRequested => {
                event_loop.exit();
            }
            WindowEvent::RedrawRequested => {
                debug!("Redraw request");
                match (self.callback)(Event::Redraw) {
                    Ok(cont) => {
                        if !cont {
                            debug!("Exiting due to user request after redraw");
                            event_loop.exit()
                        }
                    }
                    Err(e) => {
                        error!("Exiting due to an error: '{}'", e);
                        event_loop.exit()
                    }
                }
            }
            _ => (),
        }
    }

    fn device_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        _device_id: winit::event::DeviceId,
        event: winit::event::DeviceEvent,
    ) {
        if let DeviceEvent::Key(kev) = event {
            if let winit::keyboard::PhysicalKey::Code(code) = kev.physical_key {
                debug!("Handling input of physical key with code '{:?}'", code);
                let ev = match code {
                    winit::keyboard::KeyCode::Backquote => {
                        key_event(kev.state, Key::Character('`'))
                    }
                    winit::keyboard::KeyCode::Backslash => {
                        key_event(kev.state, Key::Character('\\'))
                    }
                    winit::keyboard::KeyCode::BracketLeft => {
                        key_event(kev.state, Key::Character('['))
                    }
                    winit::keyboard::KeyCode::BracketRight => {
                        key_event(kev.state, Key::Character(']'))
                    }
                    winit::keyboard::KeyCode::Comma => key_event(kev.state, Key::Character(',')),
                    winit::keyboard::KeyCode::Digit0 => key_event(kev.state, Key::Character('0')),
                    winit::keyboard::KeyCode::Digit1 => key_event(kev.state, Key::Character('1')),
                    winit::keyboard::KeyCode::Digit2 => key_event(kev.state, Key::Character('2')),
                    winit::keyboard::KeyCode::Digit3 => key_event(kev.state, Key::Character('3')),
                    winit::keyboard::KeyCode::Digit4 => key_event(kev.state, Key::Character('4')),
                    winit::keyboard::KeyCode::Digit5 => key_event(kev.state, Key::Character('5')),
                    winit::keyboard::KeyCode::Digit6 => key_event(kev.state, Key::Character('6')),
                    winit::keyboard::KeyCode::Digit7 => key_event(kev.state, Key::Character('7')),
                    winit::keyboard::KeyCode::Digit8 => key_event(kev.state, Key::Character('8')),
                    winit::keyboard::KeyCode::Digit9 => key_event(kev.state, Key::Character('9')),
                    winit::keyboard::KeyCode::Equal => key_event(kev.state, Key::Character('=')),
                    winit::keyboard::KeyCode::KeyA => key_event(kev.state, Key::Character('a')),
                    winit::keyboard::KeyCode::KeyB => key_event(kev.state, Key::Character('b')),
                    winit::keyboard::KeyCode::KeyC => key_event(kev.state, Key::Character('c')),
                    winit::keyboard::KeyCode::KeyD => key_event(kev.state, Key::Character('d')),
                    winit::keyboard::KeyCode::KeyE => key_event(kev.state, Key::Character('e')),
                    winit::keyboard::KeyCode::KeyF => key_event(kev.state, Key::Character('f')),
                    winit::keyboard::KeyCode::KeyG => key_event(kev.state, Key::Character('g')),
                    winit::keyboard::KeyCode::KeyH => key_event(kev.state, Key::Character('h')),
                    winit::keyboard::KeyCode::KeyI => key_event(kev.state, Key::Character('i')),
                    winit::keyboard::KeyCode::KeyJ => key_event(kev.state, Key::Character('j')),
                    winit::keyboard::KeyCode::KeyK => key_event(kev.state, Key::Character('k')),
                    winit::keyboard::KeyCode::KeyL => key_event(kev.state, Key::Character('l')),
                    winit::keyboard::KeyCode::KeyM => key_event(kev.state, Key::Character('m')),
                    winit::keyboard::KeyCode::KeyN => key_event(kev.state, Key::Character('n')),
                    winit::keyboard::KeyCode::KeyO => key_event(kev.state, Key::Character('o')),
                    winit::keyboard::KeyCode::KeyP => key_event(kev.state, Key::Character('p')),
                    winit::keyboard::KeyCode::KeyQ => key_event(kev.state, Key::Character('q')),
                    winit::keyboard::KeyCode::KeyR => key_event(kev.state, Key::Character('r')),
                    winit::keyboard::KeyCode::KeyS => key_event(kev.state, Key::Character('s')),
                    winit::keyboard::KeyCode::KeyT => key_event(kev.state, Key::Character('t')),
                    winit::keyboard::KeyCode::KeyU => key_event(kev.state, Key::Character('u')),
                    winit::keyboard::KeyCode::KeyV => key_event(kev.state, Key::Character('v')),
                    winit::keyboard::KeyCode::KeyW => key_event(kev.state, Key::Character('w')),
                    winit::keyboard::KeyCode::KeyX => key_event(kev.state, Key::Character('x')),
                    winit::keyboard::KeyCode::KeyY => key_event(kev.state, Key::Character('y')),
                    winit::keyboard::KeyCode::KeyZ => key_event(kev.state, Key::Character('z')),
                    winit::keyboard::KeyCode::Minus => key_event(kev.state, Key::Character('-')),
                    winit::keyboard::KeyCode::Period => key_event(kev.state, Key::Character('.')),
                    winit::keyboard::KeyCode::Quote => key_event(kev.state, Key::Character('"')),
                    winit::keyboard::KeyCode::Semicolon => {
                        key_event(kev.state, Key::Character(';'))
                    }
                    winit::keyboard::KeyCode::Slash => key_event(kev.state, Key::Character('/')),
                    winit::keyboard::KeyCode::AltLeft => key_event(kev.state, Key::Alt),
                    winit::keyboard::KeyCode::AltRight => key_event(kev.state, Key::AltGr),
                    winit::keyboard::KeyCode::Backspace => key_event(kev.state, Key::Backspace),
                    winit::keyboard::KeyCode::CapsLock => key_event(kev.state, Key::CapsLock),
                    winit::keyboard::KeyCode::ControlLeft => key_event(kev.state, Key::Ctrl),
                    winit::keyboard::KeyCode::ControlRight => key_event(kev.state, Key::Ctrl),
                    winit::keyboard::KeyCode::Enter => key_event(kev.state, Key::Return),
                    winit::keyboard::KeyCode::SuperLeft => key_event(kev.state, Key::Meta),
                    winit::keyboard::KeyCode::SuperRight => key_event(kev.state, Key::Meta),
                    winit::keyboard::KeyCode::ShiftLeft => key_event(kev.state, Key::Shift),
                    winit::keyboard::KeyCode::ShiftRight => key_event(kev.state, Key::Shift),
                    winit::keyboard::KeyCode::Space => key_event(kev.state, Key::Character(' ')),
                    winit::keyboard::KeyCode::Tab => key_event(kev.state, Key::Tab),
                    winit::keyboard::KeyCode::ArrowDown => key_event(kev.state, Key::DownArrow),
                    winit::keyboard::KeyCode::ArrowLeft => key_event(kev.state, Key::LeftArrow),
                    winit::keyboard::KeyCode::ArrowRight => key_event(kev.state, Key::RightArrow),
                    winit::keyboard::KeyCode::ArrowUp => key_event(kev.state, Key::UpArrow),
                    winit::keyboard::KeyCode::Numpad0 => key_event(kev.state, Key::Character('0')),
                    winit::keyboard::KeyCode::Numpad1 => key_event(kev.state, Key::Character('1')),
                    winit::keyboard::KeyCode::Numpad2 => key_event(kev.state, Key::Character('2')),
                    winit::keyboard::KeyCode::Numpad3 => key_event(kev.state, Key::Character('3')),
                    winit::keyboard::KeyCode::Numpad4 => key_event(kev.state, Key::Character('4')),
                    winit::keyboard::KeyCode::Numpad5 => key_event(kev.state, Key::Character('5')),
                    winit::keyboard::KeyCode::Numpad6 => key_event(kev.state, Key::Character('6')),
                    winit::keyboard::KeyCode::Numpad7 => key_event(kev.state, Key::Character('7')),
                    winit::keyboard::KeyCode::Numpad8 => key_event(kev.state, Key::Character('8')),
                    winit::keyboard::KeyCode::Numpad9 => key_event(kev.state, Key::Character('9')),
                    winit::keyboard::KeyCode::NumpadAdd => {
                        key_event(kev.state, Key::Character('+'))
                    }
                    winit::keyboard::KeyCode::NumpadBackspace => {
                        key_event(kev.state, Key::Backspace)
                    }
                    winit::keyboard::KeyCode::NumpadComma => {
                        key_event(kev.state, Key::Character(','))
                    }
                    winit::keyboard::KeyCode::NumpadEnter => key_event(kev.state, Key::Return),
                    winit::keyboard::KeyCode::NumpadEqual => {
                        key_event(kev.state, Key::Character('='))
                    }
                    winit::keyboard::KeyCode::Escape => key_event(kev.state, Key::Escape),
                    winit::keyboard::KeyCode::Meta => key_event(kev.state, Key::Meta),
                    _ => Event::Other,
                };

                match (self.callback)(ev) {
                    Ok(cont) => {
                        if !cont {
                            debug!("Exiting due to user request after key input");
                            event_loop.exit()
                        }
                    }
                    Err(e) => {
                        error!("Exiting due to an error: '{}'", e);
                        event_loop.exit()
                    }
                }
            }
        }
    }
}

/// helper function to create key events
fn key_event(state: ElementState, key: Key) -> Event {
    match state {
        ElementState::Pressed => Event::KeyPressed(key),
        ElementState::Released => Event::KeyReleased(key),
    }
}
