// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::core::actions::{Action, ActionHandler, Keybind};
use crate::core::config::{Config, Modifier};
use crate::ui::event_source::EventSource;
use crate::ui::{view::View, Event, Key};
use anyhow::{Ok, Result};
use log::debug;

/// Encapsule input handling and thereby the driving logic
///
/// Translates events to actions.
/// Store the current state of the modifier keys.
/// Associated function [select_action()](Controller::select_action) turns input events into [`Action`] variants.
/// The current state of the modifier keys is considered when doing so.
pub struct Controller {
    ah: ActionHandler<View>,
    cfg: Config,
    layer_keybinds: Vec<(Keybind, usize)>,
    shift_key: bool,
    ctrl_key: bool,
    alt_key: bool,
    altgr_key: bool,
    meta_key: bool,
}

impl Controller {
    /// create a new controller
    ///
    /// # Arguments:
    ///
    /// * `cfg` - configuration used for extracting keybinds
    /// * `ah` - action handler to call when a new action occurs
    pub fn new(cfg: Config, ah: ActionHandler<View>) -> Self {
        let layer_keybinds = cfg
            .feeds
            .iter()
            .enumerate()
            .filter_map(|(idx, feed)| {
                feed.keybind.as_ref().map(|kb| {
                    (
                        crate::core::actions::Keybind {
                            shift: kb.modifier.contains(&Modifier::Shift),
                            ctrl: kb.modifier.contains(&Modifier::Ctrl),
                            alt: kb.modifier.contains(&Modifier::Alt),
                            altgr: kb.modifier.contains(&Modifier::AltGr),
                            meta: kb.modifier.contains(&Modifier::Meta),
                            key: kb.key,
                        },
                        idx,
                    )
                })
            })
            .collect();

        Controller {
            ah,
            cfg,
            layer_keybinds,
            shift_key: false,
            ctrl_key: false,
            alt_key: false,
            altgr_key: false,
            meta_key: false,
        }
    }

    /// main event loop
    ///
    /// Creates a event source (see [`EventSource`] and waits for events.
    /// On receiving events, convert them to actions and call the action handler.
    pub fn run(&mut self) -> Result<()> {
        let mut event_source = EventSource::new(self.cfg.general.height, self.cfg.general.layout)?;
        self.ah.refresh()?;

        let event_callback = |event: Event| -> Result<bool> {
            let action = self.select_action(event)?;
            self.ah.evaluate(action)
        };

        event_source.run(event_callback)?;

        Ok(())
    }

    /// select an action matching the event `ev`
    ///
    /// # Arguments:
    ///
    /// * `ev` - event to be handled
    fn select_action(&mut self, ev: Event) -> Result<Action<View>> {
        match ev {
            Event::KeyPressed(Key::Escape) => Ok(Action::Exit),
            Event::KeyReleased(Key::Escape) => Ok(Action::None),
            Event::KeyPressed(Key::Shift) => {
                debug!("Set shift state to true.");
                self.shift_key = true;
                Ok(Action::None)
            }
            Event::KeyReleased(Key::Shift) => {
                debug!("Set shift state to false.");
                self.shift_key = false;
                Ok(Action::None)
            }
            Event::KeyPressed(Key::CapsLock) => Ok(Action::None),
            Event::KeyReleased(Key::CapsLock) => Ok(Action::None),
            Event::KeyPressed(Key::Ctrl) => {
                debug!("Set ctrl state to true.");
                self.ctrl_key = true;
                Ok(Action::None)
            }
            Event::KeyReleased(Key::Ctrl) => {
                debug!("Set ctrl state to false.");
                self.ctrl_key = false;
                Ok(Action::None)
            }
            Event::KeyPressed(Key::Alt) => {
                debug!("Set alt state to true.");
                self.alt_key = true;
                Ok(Action::None)
            }
            Event::KeyReleased(Key::Alt) => {
                debug!("Set alt state to false.");
                self.alt_key = false;
                Ok(Action::None)
            }
            Event::KeyPressed(Key::AltGr) => {
                debug!("Set altgr state to true.");
                self.altgr_key = true;
                Ok(Action::None)
            }
            Event::KeyReleased(Key::AltGr) => {
                debug!("Set altgr state to false.");
                self.altgr_key = false;
                Ok(Action::None)
            }
            Event::KeyPressed(Key::Meta) => {
                debug!("Set meta state to true.");
                self.meta_key = true;
                Ok(Action::None)
            }
            Event::KeyReleased(Key::Meta) => {
                debug!("Set meta state to false.");
                self.meta_key = false;
                Ok(Action::None)
            }
            Event::KeyPressed(Key::UpArrow) => Ok(Action::MoveSelLeft),
            Event::KeyReleased(Key::UpArrow) => Ok(Action::None),
            Event::KeyPressed(Key::LeftArrow) => Ok(Action::MoveSelLeft),
            Event::KeyReleased(Key::LeftArrow) => Ok(Action::None),
            Event::KeyPressed(Key::RightArrow) => Ok(Action::MoveSelRight),
            Event::KeyReleased(Key::RightArrow) => Ok(Action::None),
            Event::KeyPressed(Key::DownArrow) => Ok(Action::MoveSelRight),
            Event::KeyReleased(Key::DownArrow) => Ok(Action::None),
            Event::KeyPressed(Key::Tab) => match self.shift_key {
                true => Ok(Action::MoveSelLeft),
                false => Ok(Action::MoveSelRight),
            },
            Event::KeyReleased(Key::Tab) => Ok(Action::None),
            Event::KeyPressed(Key::Return) => Ok(Action::ChooseSelection),
            Event::KeyReleased(Key::Return) => Ok(Action::None),
            Event::KeyPressed(Key::Backspace) => Ok(Action::RemoveChar),
            Event::KeyReleased(Key::Backspace) => Ok(Action::None),
            Event::KeyPressed(Key::Character(c)) => {
                Ok(self.switch_layer(c).unwrap_or_else(|| self.char_action(c)))
            }
            Event::KeyReleased(Key::Character(_)) => Ok(Action::None),
            Event::WindowCreated(wnd) => Ok(Action::SetOutputHandler(View::new(&self.cfg, wnd)?)),
            Event::Redraw => Ok(Action::Redraw),
            Event::Other => Ok(Action::None),
        }
    }

    /// convert character into a `AddChar` action based on shift key state
    ///
    /// # Arguments:
    ///
    /// * `ch` - character to be handled
    fn char_action(&self, ch: char) -> Action<View> {
        match self.shift_key {
            true => Action::AddChar(
                // @todo: correctly handle case when uppercase results in more than 1 char
                // e.g. 'ß' -> 'SS'
                ch.to_uppercase()
                    .next()
                    .expect("Casting to uppercase should result in a character"),
            ),
            false => Action::AddChar(ch),
        }
    }

    /// select current layer based on key pressed
    ///
    /// Compare modifier keys and the input character with configured key bindings.
    /// Select the matching layer (if any).
    ///
    /// # Arguments:
    ///
    /// * `ch` - entered character
    fn switch_layer(&self, ch: char) -> Option<Action<View>> {
        self.layer_keybinds
            .iter()
            .find(|(kb, _)| {
                kb.alt == self.alt_key
                    && kb.altgr == self.altgr_key
                    && kb.ctrl == self.ctrl_key
                    && kb.meta == self.meta_key
                    && kb.shift == self.shift_key
                    && kb.key == ch
            })
            .map(|(_, layer)| Action::SetLayer(layer.to_owned()))
    }
}

#[cfg(test)]
#[path = "test_controller.rs"]
mod tests;
