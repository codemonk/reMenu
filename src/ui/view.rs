/// This Source Code Form is subject to the terms of the Mozilla Public
/// License, v. 2.0. If a copy of the MPL was not distributed with this
/// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use crate::{
    core::{actions::UiOutputHandler, config::Config, Color, Length, StatefulEntry},
    io::{font::Font, icon_theme::IconTheme, GraphicsStore},
    ui::{
        resultview::{ResultView, ResultViewConfig},
        textfield::TextFieldConfig,
        RenderWidth, RenderingBackend, ScreenInfo, TextField,
    },
};
use anyhow::Result;
use log::{error, info};

use super::window::{self, Window};

/// configuration options for a [`View`]
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ViewConfig {
    /// height of the window
    pub height: Length,

    /// a fontconfig compatible font name
    pub font: String,

    /// a fontconfig compatible font name
    pub font_size: u32,

    /// a freedesktop compatible icon theme name
    pub icon_theme: String,

    /// icon size
    pub icon_size: Length,

    /// color of text
    pub text_color: Color,

    /// background color
    pub bg_color: Color,

    /// color of text when highlighted
    pub text_color_hl: Color,

    /// background color when highlighted
    pub bg_color_hl: Color,

    /// padding used for results
    pub padding: Length,

    /// separator distance between results
    pub separator: Length,
}

/// Representation of the main user interface (output part).
/// This is pure UI output, i.e. rendering.
/// All logic is placed outside as is input handling (encapsuled in [`Controller]).
/// `View` just draws the state provided to it.
pub struct View {
    /// store to be used for all graphics (font glyphs & icons)
    graphics_store: GraphicsStore,

    /// configuration options for the view
    cfg: ViewConfig,

    /// rendering backend that actually draws the view
    wnd: Window,

    /// input text field
    input: TextField<window::Canvas>,

    /// area that displays the results
    resultview: ResultView<window::Canvas>,
}

impl View {
    /// create a new view
    ///
    /// # Arguments
    /// * `cfg` - configuration options for the UI
    /// * `wnd` - rendering backend (i.e. window) doing window handling and rendering
    pub fn new(cfg: &Config, wnd: Window) -> Result<View> {
        let view_config = ViewConfig {
            height: cfg.general.height,
            font: cfg.general.font.clone(),
            font_size: cfg.general.font_size,
            icon_theme: cfg.general.icon_theme.clone(),
            icon_size: cfg.general.icon_size,
            padding: Length::Pixel(4),
            separator: Length::Pixel(20),
            text_color: cfg.general.text_color,
            bg_color: cfg.general.bg_color,
            text_color_hl: cfg.general.text_color_hl,
            bg_color_hl: cfg.general.bg_color_hl,
        };
        let input_tf_config = TextFieldConfig {
            width: cfg.general.input_width.unwrap_or(Length::Pixel(600)),
            height: cfg.general.height, // @todo border
            render_width: RenderWidth::Fixed,
            caret_width: Length::Pixel(2),
            ..TextFieldConfig::default()
        };

        let input_width = wnd.to_pixels(input_tf_config.width, crate::ui::Direction::Horizontal);

        let font = Font::new(&view_config.font, view_config.font_size as isize, wnd.dpi())
            .map_err(|err| {
                error!("Failed to load font with error: {}", err);
                err
            })?;
        info!("Loaded font with {} dpi.", wnd.dpi());

        let theme = IconTheme::open(
            &view_config.icon_theme,
            wnd.to_pixels(view_config.icon_size, crate::ui::Direction::Vertical),
        )?;
        info!("Loaded icon theme.");

        let graphics_store = GraphicsStore {
            font,
            theme: Some(theme),
        };

        let input = TextField::new(&wnd, input_tf_config, &wnd).map_err(|err| {
            error!("Failed to create input field with error: {}", err);
            err
        })?;

        let resultview = ResultView::new(
            &wnd,
            ResultViewConfig {
                padding: view_config.padding,
                separator: view_config.separator,
                text_color: view_config.text_color,
                bg_color: view_config.bg_color,
                text_color_hl: view_config.text_color_hl,
                bg_color_hl: view_config.bg_color_hl,
                width: Length::Pixel(wnd.width() - input_width),
                height: view_config.height,
                max_results: 20,
                max_result_width: Length::Pixel(240),
            },
            &wnd,
        )
        .map_err(|err| {
            error!("Failed to create result view with error: {}", err);
            err
        })?;

        Ok(View {
            graphics_store,
            cfg: view_config,
            wnd,
            input,
            resultview,
        })
    }
}

impl UiOutputHandler for View {
    fn update_input(&mut self, txt: &str) -> Result<()> {
        self.input.set_text(txt)
    }

    fn update_results(&mut self, results: &[StatefulEntry]) -> Result<()> {
        self.resultview.set_entries(results.to_owned())
    }

    fn decrease_selection_position(&mut self) -> Result<()> {
        self.resultview.decrease_selection_positon();
        Ok(())
    }

    fn increase_selection_position(&mut self) -> Result<()> {
        self.resultview.increase_selection_positon()
    }

    fn refresh(&mut self) -> Result<()> {
        self.wnd.fill(self.cfg.bg_color).map_err(|err| {
            error!("Failed to fill window");
            err
        })?;
        self.input
            .render(0, 0, &self.graphics_store, &self.wnd)
            .map_err(|err| {
                error!("Failed to render input.");
                err
            })?;

        self.resultview
            .render(600, 0, &self.graphics_store, &self.wnd)
            .map_err(|err| {
                error!("Failed to render result view.");
                err
            })?;
        self.wnd.show().map_err(|err| {
            error!("Failed to show window");
            err
        })?;

        Ok(())
    }
}
