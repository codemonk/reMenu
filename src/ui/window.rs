/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
use crate::ui::{
    Color, Direction, Length, Pixels, RenderingBackend, ScreenInfo, Surface, SurfaceProducer,
};
use anyhow::{Error, Result};
use std::rc::Rc;
use std::{cell::RefCell, num::NonZeroU32};
use tiny_skia::{IntSize, Pixmap};
use winit::{dpi::PhysicalSize, window::Window as WinitWnd};

/// Window displaying the menu
///
/// Encapsules a winit window handle and the rendering surface.
pub struct Window {
    /// window handle
    pub(crate) hdl: Rc<WinitWnd>,

    /// surface to render to window
    pub(crate) surface: softbuffer::Surface<Rc<WinitWnd>, Rc<WinitWnd>>,

    /// width of the screen showing the window (in pixels)
    pub(crate) screen_width_px: u32,

    /// height of the screen showing the window (in pixels)
    pub(crate) screen_height_px: u32,

    /// height of the window (in pixels)
    pub(crate) height_px: u32,

    /// canvas used for rendering all parts of the UI.
    /// This is rendered to the surface when all rendering is complete.
    pub(crate) offscreen_canvas: Rc<RefCell<Pixmap>>,
}

impl Window {
    /// create a new window
    ///
    /// # Arguments
    ///
    /// * `hdl` - winit window
    /// * `screen_width_px` - width of the screen in pixels
    /// * `screen_height_px` - height of the screen in pixels
    /// * `height` - height of the window
    pub fn new(
        hdl: Rc<WinitWnd>,
        screen_width_px: u32,
        screen_height_px: u32,
        height: Length,
    ) -> Result<Self> {
        let height_px = length_to_pixels(
            hdl.scale_factor(),
            screen_width_px,
            screen_height_px,
            height,
            Direction::Horizontal,
        ) as u32;
        hdl.set_min_inner_size(Some(PhysicalSize::new(screen_width_px, screen_height_px)));
        hdl.set_max_inner_size(Some(PhysicalSize::new(screen_width_px, screen_height_px)));

        // create offscreen buffer
        // @note: SoftBufferError is not send. We therefore transform all errors
        let context = softbuffer::Context::new(hdl.clone()).map_err(|e| {
            Error::msg(format!(
                "Error while creating a new softbuffer context: '{}'",
                e
            ))
        })?;
        let mut surface = softbuffer::Surface::new(&context, hdl.clone()).map_err(|e| {
            Error::msg(format!(
                "Failed to create a rendering surface with error: '{}'",
                e
            ))
        })?;
        surface
            .resize(
                NonZeroU32::new(screen_width_px)
                    .ok_or(Error::msg("Cannot convert screen width to NonZeroU32"))?,
                NonZeroU32::new(height_px)
                    .ok_or(Error::msg("Cannot convert height to NonZeroU32"))?,
            )
            .map_err(|e| {
                Error::msg(format!(
                    "Could not resize rendering surface with error: '{}'",
                    e
                ))
            })?;
        let offscreen_canvas = Rc::new(RefCell::new(
            Pixmap::new(screen_width_px, height_px)
                .ok_or(Error::msg("Failed to create pixmap for offscreen canvas"))?,
        ));

        Ok(Window {
            hdl,
            surface,
            screen_width_px,
            screen_height_px,
            height_px,
            offscreen_canvas,
        })
    }
}

impl RenderingBackend for Window {
    fn fill(&mut self, color: Color) -> Result<()> {
        self.offscreen_canvas
            .borrow_mut()
            .fill(tiny_skia::Color::from_rgba8(
                color.red,
                color.green,
                color.blue,
                color.alpha,
            ));
        Ok(())
    }

    fn show(&mut self) -> Result<()> {
        // @note: SoftBufferError is not send. We therefore transform all errors
        let mut buffer = self
            .surface
            .buffer_mut()
            .map_err(|e| Error::msg(format!("Error while accessing offscreen surface: '{}'", e)))?;
        let target = self.offscreen_canvas.borrow_mut();
        for index in 0..(self.screen_width_px * self.height_px).try_into()? {
            buffer[index] = Into::<u32>::into(target.data()[index * 4 + 2])
                | Into::<u32>::into(target.data()[index * 4 + 1]) << 8
                | Into::<u32>::into(target.data()[index * 4]) << 16;
        }
        buffer
            .present()
            .map_err(|e| Error::msg(format!("Error while presenting offscreen buffer: '{}'", e)))?;

        Ok(())
    }
}

impl SurfaceProducer for Window {
    type Output = Canvas;

    fn create_surface(&self, width: Pixels, height: Pixels) -> Result<Self::Output> {
        Ok(Canvas {
            pixmap: Pixmap::new(width.try_into()?, height.try_into()?)
                .ok_or(Error::msg("Error creating a pixmap"))?,
            target: self.offscreen_canvas.clone(),
        })
    }
}

impl ScreenInfo for Window {
    fn width(&self) -> i32 {
        self.screen_width_px.try_into().unwrap()
    }

    fn dpi(&self) -> u32 {
        // we need dpi for font loading, unfortunately
        // best approximation we can get from winit (to my knowledge)
        // is assuming 96 dpi (standard on windows and linux) and apply the scale factor.
        // Apple uses 72 dpi to my knowledge, but nobody cares for Apple ;)
        length_to_pixels(
            self.hdl.scale_factor(),
            self.screen_width_px,
            self.screen_height_px,
            Length::Point(96),
            Direction::Vertical,
        ) as u32
    }

    fn to_pixels(&self, len: Length, dir: Direction) -> i32 {
        length_to_pixels(
            self.hdl.scale_factor(),
            self.screen_width_px,
            self.screen_height_px,
            len,
            dir,
        )
    }
}

#[derive(Debug, PartialEq)]
pub struct Canvas {
    pixmap: tiny_skia::Pixmap,
    target: Rc<RefCell<tiny_skia::Pixmap>>,
}

impl Surface for Canvas {
    fn fill(&mut self, clr: Color) -> Result<()> {
        self.pixmap.fill(tiny_skia::Color::from_rgba8(
            clr.red, clr.green, clr.blue, clr.alpha,
        ));

        Ok(())
    }

    fn put_colorized(
        &mut self,
        buffer: &[u8],
        x: i32,
        y: i32,
        w: i32,
        h: i32,
        clr: Color,
    ) -> Result<()> {
        // Note: Creating a pixmap fails if at least one dimension is 0
        if w <= 0 || h <= 0 {
            return Ok(());
        }
        debug_assert!(buffer.len() == (w * h) as usize);

        let img: Vec<u8> = buffer
            .iter()
            .flat_map(|alpha| {
                // premultiply alpha
                let red = (Into::<u32>::into(clr.red) * Into::<u32>::into(*alpha) / 255) as u8;
                let green = (Into::<u32>::into(clr.green) * Into::<u32>::into(*alpha) / 255) as u8;
                let blue = (Into::<u32>::into(clr.blue) * Into::<u32>::into(*alpha) / 255) as u8;
                vec![red, green, blue, *alpha]
            })
            .collect();
        let width: u32 = w.try_into().unwrap();
        let height: u32 = h.try_into().unwrap();
        let src = Pixmap::from_vec(img, IntSize::from_wh(width, height).unwrap()).unwrap();

        self.pixmap.draw_pixmap(
            x,
            y,
            src.as_ref(),
            &tiny_skia::PixmapPaint::default(),
            tiny_skia::Transform::identity(),
            None,
        );

        Ok(())
    }

    fn put(&mut self, buffer: &[u8], x: i32, y: i32, w: i32, h: i32) -> Result<()> {
        // create pixmap from buffer
        let src = Pixmap::from_vec(
            buffer.to_vec(),
            tiny_skia::IntSize::from_wh(w.try_into().unwrap(), h.try_into().unwrap()).unwrap(),
        )
        .unwrap();
        self.pixmap.draw_pixmap(
            x,
            y,
            src.as_ref(),
            &tiny_skia::PixmapPaint::default(),
            tiny_skia::Transform::identity(),
            None,
        );

        Ok(())
    }

    fn render(&mut self, x: i16, y: i16) -> Result<()> {
        self.target.borrow_mut().draw_pixmap(
            x.into(),
            y.into(),
            self.pixmap.as_ref(),
            &tiny_skia::PixmapPaint::default(),
            tiny_skia::Transform::identity(),
            None,
        );
        Ok(())
    }
}

// convert length to pixels
fn length_to_pixels(
    scale_factor: f64,
    screen_width: u32,
    screen_height: u32,
    len: Length,
    dir: Direction,
) -> i32 {
    match len {
        Length::Pixel(val) => val,
        Length::Point(val) => (Into::<f64>::into(val) * scale_factor) as i32,
        Length::Percentage(val) => match dir {
            Direction::Horizontal => {
                (screen_width * TryInto::<u32>::try_into(val).unwrap() / 100) as i32
            }
            Direction::Vertical => {
                (screen_height * TryInto::<u32>::try_into(val).unwrap() / 100) as i32
            }
        },
    }
}
