// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use anyhow::{Error, Result};
use simplelog::{
    format_description, ColorChoice, CombinedLogger, ConfigBuilder, LevelFilter, TermLogger,
    TerminalMode, WriteLogger,
};
use std::fs::File;

/// initialize logging
///
/// Open the log file and initialize both file logging and terminal logging.
/// Return path of log file on success.
pub fn init() -> Result<String> {
    let log_level = log_level();
    let xdg_dirs =
        xdg::BaseDirectories::with_prefix("remenu").expect("Could not initialize XDG directories.");
    let log_path = xdg_dirs
        .place_state_file("remenu.log")
        .expect("Could not get log file path.");
    let log_config = ConfigBuilder::new()
        .set_time_format_custom(format_description!("[hour]:[minute]:[second].[subsecond]"))
        .build();
    let log_file = log_path
        .to_str()
        .ok_or(Error::msg("Failed to get path of log file"))?;
    CombinedLogger::init(vec![
        TermLogger::new(
            log_level,
            log_config.clone(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(log_level, log_config, File::create(log_file)?),
    ])?;

    Ok(log_file.to_string())
}

/// Read log level to use from environment ('LOG_LEVEL').
fn log_level() -> LevelFilter {
    if let Ok(log_level_str) = std::env::var("LOG_LEVEL") {
        match log_level_str.as_str() {
            "off" => LevelFilter::Off,
            "error" => LevelFilter::Error,
            "warn" => LevelFilter::Warn,
            "info" => LevelFilter::Info,
            "debug" => LevelFilter::Debug,
            "trace" => LevelFilter::Trace,
            _ => LevelFilter::Off,
        }
    } else {
        LevelFilter::Info
    }
}
