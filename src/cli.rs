/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use crate::io::config_file::IncludedTheme;
use anyhow::Result;
use clap::{Parser, Subcommand};

/// command line configuration for clap
#[derive(Parser, Debug)]
#[command(
    about = "reMenu: a reconfigurable menu",
    author = "Oliver Gerlach",
    version
)]
struct Args {
    #[command(subcommand)]
    command: Option<Commands>,
}

/// available commands for command line
#[derive(Subcommand, Debug)]
enum Commands {
    /// print location of logfile and exit
    LogLocation,

    /// select layer to open (list of layers if none is provided)
    /// optionally provide the name of a config
    Open {
        #[arg(short, long)]
        layer: Option<usize>,
        #[arg(short, long)]
        config: Option<String>,
    },

    /// create configuration and exit
    Touch {
        #[arg(short, long)]
        theme: IncludedTheme,
    },

    /// validate configuration, print result and exit
    /// optionally provide the name of a config
    Validate { config: Option<String> },
}

/// action to be executed as result of given command line arguments
pub enum CliAction {
    // open the menu with the provided layer and the given config (if any)
    Open(usize, Option<String>),

    // print log location and exit
    PrintLogLocation,

    // create config from provided theme and exit
    TouchConfig(IncludedTheme),

    // validate provided or default config file and exit
    Validate(Option<String>),
}

/// handle command line arguments
///
/// Parse command line arguments and select an action to perform
pub fn handle_cli_args() -> Result<CliAction> {
    let args = Args::parse();
    match args.command {
        Some(cmd) => match cmd {
            Commands::LogLocation => Ok(CliAction::PrintLogLocation),
            Commands::Open { layer, config } => Ok(CliAction::Open(layer.unwrap_or(0), config)),
            Commands::Touch { theme } => Ok(CliAction::TouchConfig(theme)),
            Commands::Validate { config } => Ok(CliAction::Validate(config)),
        },
        None => Ok(CliAction::Open(0, None)),
    }
}
