/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use anyhow::Result;

pub mod controller;
pub mod event_source;
pub mod resultview;
pub mod textfield;
pub mod view;
pub mod window;

use crate::core::{Color, Length, Pixels};
use crate::ui::window::Window;
pub use textfield::{RenderWidth, TextField};

/// represent a screen direction
pub enum Direction {
    /// horizontal direction
    Horizontal,

    /// vertical direction
    Vertical,
}

// commented keys are for future use
/// representation for keyboard keys
#[derive(Clone, Copy, Debug)]
pub enum Key {
    Character(char),
    Return,
    Tab,
    Escape,
    // Delete,
    Backspace,
    LeftArrow,
    RightArrow,
    UpArrow,
    DownArrow,
    // PgnDown,
    // PgnUp,
    // Home,
    // End,
    // F1,
    // F2,
    // F3,
    // F4,
    // F5,
    // F6,
    // F7,
    // F8,
    // F9,
    // F10,
    // F11,
    // F12,
    Ctrl,
    Alt,
    AltGr,
    Meta,
    Shift,
    CapsLock,
}

/// an event to be handled
pub enum Event {
    /// key pressed event
    KeyPressed(Key),

    /// key released event
    KeyReleased(Key),

    /// window created
    WindowCreated(Window),

    /// redraw window
    Redraw,

    /// any other event that is not supported
    Other,
}

/// Surface is anything that can be drawn to
pub trait Surface {
    /// fill the whole surface with the given color
    ///
    /// # Arguments
    ///
    /// * `clr` - color used for filling
    fn fill(&mut self, clr: Color) -> Result<()>;

    /// render a alpha map onto the surface
    ///
    /// The given buffer is interpreted as a alpha map and rendered to the surface
    /// with the given color
    ///
    /// # Arguments
    ///
    /// * `buffer` - buffer with alpha map data (8 bit per pixel)
    /// * `x` - left start position for rendering
    /// * `y` - top start position for rendering
    /// * `w` - width of the buffer to render
    /// * `h` - height of the buffer to render
    /// * `clr` - color used for rendering
    fn put_colorized(
        &mut self,
        buffer: &[u8],
        x: i32,
        y: i32,
        w: i32,
        h: i32,
        clr: Color,
    ) -> Result<()>;

    /// render a RGBA image onto the surface
    ///
    /// The given buffer is interpreted as a RGBA image and rendered to the surface
    ///
    /// # Arguments
    ///
    /// * `buffer` - buffer with RGBA data (8 bit per channel)
    /// * `x` - left start position for rendering
    /// * `y` - top start position for rendering
    /// * `w` - width of the buffer to render
    /// * `h` - height of the the buffer to render
    fn put(&mut self, buffer: &[u8], x: i32, y: i32, w: i32, h: i32) -> Result<()>;

    /// render the surface onto the backend
    ///
    /// Causes the surface to be rendered.
    /// It will automatically be rendered to the backend that created the surface.
    ///
    /// # Arguments
    ///
    /// * `x` - left start position on target for rendering
    /// * `y` - top start position on target for rendering
    fn render(&mut self, x: i16, y: i16) -> Result<()>;
}

/// A SurfaceProducer creates surfaces on demand.
pub trait SurfaceProducer {
    type Output: Surface;

    /// create a new surface
    ///
    /// # Arguments
    ///
    /// * `width` - width of the surface
    /// * `height` - height of the surface
    fn create_surface(&self, width: Pixels, height: Pixels) -> Result<Self::Output>;
}

/// A RenderingBackend abstracts the output medium
pub trait RenderingBackend: SurfaceProducer {
    /// fill the main surface of the backend with the given color
    ///
    /// # Arguments
    ///
    /// * `clr` - color for filling
    fn fill(&mut self, color: Color) -> Result<()>;

    /// show the updated screen
    fn show(&mut self) -> Result<()>;
}

pub trait ScreenInfo {
    /// width of the screen in pixels
    fn width(&self) -> i32;

    /// dots per inch
    fn dpi(&self) -> u32;

    /// convert a length value to pixels
    fn to_pixels(&self, len: Length, dir: Direction) -> i32;
}
