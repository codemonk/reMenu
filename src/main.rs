// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! reMenu: a reconfigurable menu
//!
//! reMenu aims to be a launcher menu plus some goodies.
//! While the intended use case is similar to tools like dmenu, the approach is different.
//! reMenu is batteries included.
//! It contains different code to find and extract menu entries without the use of external scripts.
//! The sourcing of entries can be controlled by a config file.
//!
//! This documentation is intended to help navigate and understand the code base.
//! It is not a user level documentation.

use core::config::Config;

use crate::io::config_file;
use crate::ui::controller::Controller;
use crate::{cli::handle_cli_args, core::actions::ActionHandler};
use anyhow::Result;
use log::{error, info};

/// Core logic and common types
mod core;

/// File input output (entries, icons, fonts)
mod io;

/// User input handling and user interface rendering
mod ui;

/// command-line argument handling
mod cli;

/// logging facilities
mod logging;

/// Print a hint on how to open a layer
///
/// Print a hint explaining how to open a specific layer.
/// Print a list of all available layers in a config.
///
/// # Arguments
///
/// * `cfg` - configuration to extract available layers from
fn show_layer_hint(cfg: &Config) {
    println!(
        "To open a layer directly, select a layer available in your config. Available layers:"
    );
    println!();
    cfg.feeds
        .iter()
        .enumerate()
        // @todo properly display feed
        .for_each(|layer| println!("{}: {:?})", layer.0, layer.1.options));
}

/// Open the menu with a given layer
///
/// This starts the UI loop if a valid layer was selected.
///
/// # Arguments
///
/// * `layer` - the layer to show
/// * `config` - name of a config (searched in default directory)
fn open_menu(layer: usize, config: Option<String>) -> Result<()> {
    let cfg = config_file::load(config)?;
    info!("Loaded configuration.");
    if layer >= cfg.feeds.len() {
        show_layer_hint(&cfg);
        error!(
            "Selected layer {} is out of bounds. Select a layer between 0 and {}.",
            layer,
            cfg.feeds.len() - 1
        );
    } else {
        let mut feeds = config_file::build_feed(&cfg);
        info!("Feeds loaded.");
        feeds.set_layer(layer)?;

        let ah = ActionHandler::new(feeds, &cfg.general.terminal);
        let mut ctrl = Controller::new(cfg, ah);
        info!("Initialization complete.");

        // We enter the main event loop
        ctrl.run()?;

        // let mut text = "Yesu ai wo = 耶稣爱我".to_string();
    }
    Ok(())
}

/// main function (who would have guessed?)
fn main() -> Result<()> {
    let log_file = logging::init().expect("Fatal: Could not initialize logger."); // @todo provide path or in general more concrete error
    info!("Started remenu");
    info!("Logger initialized. Logfile is located at {}.", log_file);

    match handle_cli_args() {
        Ok(action) => match action {
            cli::CliAction::Open(layer, config) => open_menu(layer, config),
            cli::CliAction::PrintLogLocation => {
                println!("Logfile is located at: {}", log_file);
                Ok(())
            }
            cli::CliAction::TouchConfig(theme) => config_file::touch(theme),
            cli::CliAction::Validate(config) => config_file::validate(config).map(|res| {
                println!("{}", res);
            }),
        },
        Err(e) => {
            error!("Error whiling handling CLI arguments: {}", e);
            Err(e)
        }
    }
}
