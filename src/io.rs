/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use self::{font::Font, icon_theme::IconTheme};

pub mod config_file;
pub mod entries;
pub mod fd_org_ini_parser;
pub mod font;

/**
Loader for icon themes

Loads icon themes according to Freedesktop icon theme specification
(`<https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html>`).
Currently supports a limited subset of all features.
Especially only loads theme 'hicolor' and does not resolve parent themes.
**/
pub mod icon_theme;

#[derive(Debug)]
pub struct GraphicsStore {
    pub font: Font,
    pub theme: Option<IconTheme>,
}
