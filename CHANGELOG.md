# Changelog

## version 0.0.7

- fix: switching input from uppercase to lowercase not working
- fix: scanline pad is now observed
- feature: layout options are available and observed
- feature: config is now set from main
- feature: input now has a caret

## version 0.0.8

- feature: support icons according to freedesktop spec

## version 0.0.9

- feature: add support for path entries
- feature: add support for direct execution
- feature: add multiple feeds and keybinds for them
- feature: add configuration file support

## version 0.1.0

- feature: add CLI parameters
- feature: terminal emulator can be specified for terminal applications
- fix: command parameters are now supported when needed to open an app

## version 0.2.0

- feature: partial support for wayland by changing rendering backend
- feature: remove dpi parameter from config and use scaling factor instead

## version 0.3.0

- feature: cache loading entries for faster startup when used in multiple feeds
- feature: allow lazy loading of feeds to improve startup time
- feature: support feed-specific options

