# How to release

Just as a quick reminder, these are the release steps:

1. Ensure that all necessary tests exists
2. Ensure that all tests are green
3. Ensure that build has no errors and warnings
4. Ensure that clippy issues no warnings
5. Check that the app starts and works without any apparent issues
6. Update changelog
7. Check that readme is up to date
9. Tag work with `git tag -a v<major>.<minor>.<patch> -m <msg>`
10. Push tag to remote with `git push origin v<major>.<minor>.<patch>`
11. Release with `cargo publish`
12. Update version in `Cargo.toml`
