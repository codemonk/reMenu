# reMenu: A rust-enabled reconfigurable menu

reMenu is a reconfigurabe app launcher.
When invoked, it collects available applications and presents the user with a simple menu
that allow filtering and browsing through the available applications.
The selected application will than be executed.

## Current status

**NOTE:** Development is on hold as I switched back to KDE.

This is an early version.
All the basic features are there.
It worked on my machines as a daily driver.
The main missing features are UI polishing, internationalization and Wayland support.
Feel free to try it out but expect bugs.

## How to use it?

### Installation

As of now, the only option is to build it yourself via

    cargo install remenu

Other options are planned.

### Usage

Until there is a reasonable user documentation, the best way to configure reMenu
to your needs is touching a configuration via

    remenu touch default

This will generate a default configuration (i.e. my current config) at

    ~/.config/remenu/config.toml

This config has comments to get you started.

#### Feeds

A core concept of reMenu are feeds.
A feed is a list of entries to select from based on the input text.
Most feeds apply a filter to the input text but not all.
It is possible to have multiple feeds in one config.
Feeds are either accessable via cli, a configurable hotkey or a fallthrough strategy.
This makes it possible to switch between feeds while the menu is open (via shortcut) or
automatically use another feed when the current feed does not yield any entries with the given
input and filter.

### Troubleshooting

The best way to fix problems is by looking at the logs.
reMenu observed the `LOG_LEVEL` environment variable.
One can therefore simply select a log level via

    LOG_LEVEL=level remenu

where `level` is either `error`, `warn`, `info`, `debug` or `off`.
`trace` is supported but not used.

## Q&A

These are some questions I asked myself, while working on the project.

### Does the world need yet another alternative to dmenu?

Maybe, maybe not.
What I was missing was an alternative with a few features:

1. Desktop entries should be displayed with their icon.
2. Switching between different types of entries (e.g. desktop entries, path)
   shall be possible at runtime.
3. Different filters (e.g. prefix, fuzzy) shall be available.
4. Changing the behaviour shall be possible via a config file.

When I started the project, I did not find a solution to that.
Besides, it is a funny little side project.

### Can I use it?

You can try.
But I do not guarantee that it works.

### I tried it and it renders my native language wrong.

Not really a question, but it renders my native language wrong as well.
As of now, only English works.
I can live with that.
This will change the future but it is no priority.
I assume, most users of tiling window managers speak English as a first language or
(like me) use English for interacting with tech.
This leaves a small sub-group of an already small group of people out.
In the presence of mature alternatives, this is okay.

### What about Wayland support?

Wayland support is planned.
As of now, my personal devices are too old for Wayland support.
Besides, leftWM is stuck on X for now.
Nevertheless, the UI does support wayland.
There is no support for overlay right now.
wlroots has a protocol (layer shell).
It is not an 'official' wayland protocol and not supported by the crate I use
for creating windows.
Thus, remenu will currently be layouted as a normal window.
Besides that, wayland should work.

## Limitations

- Nested folders are not supported when loading desktop entries.
  Normally, desktop entries are stored directly in the searched folders.
  I know only one case involving screensavers where this is not the case.
  Introducing nested folders for this use case only, does not make sense.
  Note that this might change, if more cases of nested folders are found in the wild.

## Open issues

- scroll through results
- check for vertical fonts
- Add missing keys: F1-F12, delete, tab, pos1, end, pgn_up, pgn_down
- casts and shortcuts in font rendering

